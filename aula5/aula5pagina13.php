<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Guia de Boas Práticas em Contratação de Soluções de TI da SLTI', 'exibir', '5','13', '19', 'aula5pagina12.php', 'aula5pagina14.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h4 class="subTitulo">3.2.  GUIA DE BOAS PRÁTICAS EM CONTRATAÇÃO DE SOLUÇÕES DE TECNOLOGIA DA INFORMAÇÃO DA SLTI</h4>
                <p>O <a href="http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/guia-de-boas-praticas-em-contratacao-de-solucoes-de-ti" target="_blank" title="acesse o guia de boas práticas em contratação de SLTI">Guia de boas práticas em contratação de soluções de Tecnologia da Informação</a>, elaborado pela Secretaria de Logística e Tecnologia da Informação – SLTI, do Ministério do Planejamento, Orçamento e Gestão - MPOG, nasceu como um produto do processo de revisão da Instrução Normativa N° 04, de 19 de maio de 2008 – IN SLTI/MP n° 04/2008, publicada pela SLTI.</p>
                <p>Essa Norma disciplinava o processo de contratação de Serviços de Tecnologia da Informação pela Administração Pública Federal (APF) direta, autárquica e fundacional. Sua revisão deu origem à IN SLTI/MP n° 04/2010 e, em seguida, à IN SLTI/MP n° 04/2014, que consolida um conjunto de boas práticas para contratação de Soluções de TI pela APF, denominado Modelo de contratação de soluções de TI – MCTI.</p>
                <p>Conforme destacado anteriormente, as instruções normativas do MPOG não são de aplicação obrigatória em todos os casos e em todas as instituições do poder público. Contudo, são referenciais de boas práticas e estão sendo utilizadas por diversos órgãos que acreditam no efeito positivo de ações dessa natureza na gestão pública, nos termos de diversos acórdãos do TCU (CAVALCANTI, 2013).</p>
                <p>O Guia da SLTI descreve os processos, atividades e artefatos do Modelo, com o objetivo de apoiar os profissionais na realização de contratações de Soluções de TI. Nos mesmos moldes do Guia do TCU, são detalhados, em 206 páginas, pontos importantes do processo de planejamento da contratação, fluxos, processos e atividades envolvidas em cada fase, elaboração de artefatos, estratégias e riscos a serem considerados. Trata-se de excelente fonte de consulta para gestores. Abaixo, apresentamos um exemplo de fluxo constante do Guia da SLTI (p. 34).</p>
                <p class="textAlignCenter"><a href="../include/img/aulas/aula5Figura2.png" title="clique para ampliar" target="_blank"><img src="../include/img/aulas/aula5Figura2.png" style="max-width:500px;" alt="Fluxo de contratação de TI conforme Guia, passando pela análise de viabilidade, plano de sustentação, estratégia da contratação e análise de riscos" /></a></p>
                <p class="textAlignCenter fonteMenor">Figura 2 – Modelo de fluxo. Planejamento da Contratação de Soluções de TI.</p>
                <p>Observe que o Documento de Oficialização da demanda é o instrumento  responsável por detalhar a necessidade da área requisitante a ser atendida. O processo avança pelas etapas previstas na IN 4/2014 – MPOG, conforme demostrado na figura. Durante a tramitação dos autos, vários outros documentos são produzidos, de modo a dar maior robustez aos autos.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina12.php', 'aula5pagina14.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


