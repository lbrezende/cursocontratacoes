<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Avaliação de Resultados', 'exibir', '5','5', '19', 'aula5pagina4.php', 'aula5pagina6.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			       <p>Para Gabriel Lara <i>et al.</i>:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">(...) essa modernização deu-se na alteração da maneira como se avalia os objetos contratados e não nos procedimentos, que continuam respeitando as limitações legais. </p>
                  <p class="fonteMenor">Na prática, a evolução alcançada foi passar a contratar resultados (a eficácia em si) em detrimento à forma de contratação de serviços usualmente utilizada pela Administração Pública, atrelando o pagamento financeiro do fornecedor à qualidade do serviço prestado.</p>
                </div>
                <div class="clear"></div>
                <p>Medidas dessa natureza objetivam parametrizar a dinâmica operacional na execução dos serviços pelos fornecedores. Isso é possível por meio da quantificação do grau de qualidade desejado, vinculando o pagamento da parcela contratual ao índice de desempenho alcançado (LARA, et al. 2013). Vale lembrar que esse recurso sempre deverá constar, com detalhes, no termo de referência/projeto básico/edital.</p>
                <p>A contratação mensurada por resultados não possui previsão normativa recente. De acordo com Cavalcanti (2013), ela encontra fundamento jurídico no §1º do art. 3º do <a href="http://www.planalto.gov.br/ccivil_03/decreto/d2271.htm" target="_blank" title="link para o decreto 2271/97">Decreto nº 2.271/1997</a>, <i>in verbis</i>: </p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Sempre que a prestação do serviço objeto da contratação puder ser avaliada por determinada unidade quantitativa de serviço prestado, esta deverá estar prevista no edital e no respectivo contrato, e será utilizada como um dos parâmetros de aferição de resultados.</p>
                </div>
                <div class="clear"></div>
                <p>Também se fundamenta juridicamente no Princípio da Eficiência, conforme abaixo:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">O pagamento pelo resultado incentiva a eficiência do contratado, que tentará organizar o trabalho no sentido de atingir o resultado estabelecido a partir do melhor rendimento que estiver ao seu alcance. Como corolário da eficiência, o pagamento por resultado dirige a atenção da administração para o controle da eficácia da contratação. Também incentiva que sejam atingidos padrões desejados de qualidade do produto ou serviço oferecido.</p>
                </div>
                <div class="clear"></div>
                <p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
                <div class="bordaPalavraAutor">
                  <p>A previsão de mensuração dos resultados por meio de níveis de serviço previamente especificados, com possibilidade de aplicação de penalidades em função de desempenhos desconformes com as exigências técnicas, constitui uma boa prática a ser adotada, não só em contratações afetas à TI, mas em outros tipos de contratações, sempre que cabível. Essa opção deve ser analisada no planejamento da demanda. Vale lembrar que as regras de avaliação deverão ser previamente estabelecidas no termo de referência/projeto básico/edital, para conhecimento exato de todas as condições de execução pelos contratantes/licitantes.</p>
                </div>
                <p>Para as contratações de TI, o TCU editou a Súmula - TCU 269 - que remete ao ajuste da remuneração da contratada em função da adequada prestação dos resultados, <i>in verbis</i>:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Nas contratações para a prestação de serviços de tecnologia da informação, a <span class="semi-bold">remuneração deve estar vinculada a resultados ou ao atendimento de níveis de serviço</span>, (...) (grifamos)</p>
                </div>
                <div class="clear"></div>
                <p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
                <div class="bordaPalavraAutor">
                  <p>Desde que especificados no edital, uma contratada que, por exemplo, não atender a 70% dos chamados técnicos de primeiro nível, no prazo de 30 minutos, terá sua remuneração glosada por esse fato. Se atender ao ANS, ainda que o superando (atendendo a todos os chamados em um prazo menor), receberá a totalidade da remuneração. </p>
                  <p>Na iniciativa privada, pode haver premiação adicional na remuneração pela excelência na prestação dos serviços (CAVALCANTI, 2013).</p>
                </div>
                



              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina4.php', 'aula5pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


