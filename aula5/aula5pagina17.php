<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('A Importância do Comprometimento com o Resultado', 'exibir', '5','17', '19', 'aula5pagina16.php', 'aula5pagina18.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p>Desde 2009, a Secretaria de Gestão de Pessoas do STF vem desenvolvendo um conjunto de ações relacionadas ao <a href="http://intranet/hotsite/Site_cidadania_corporativa_2015/site/index.html " target="_blank" title="acesse o programa cidadania corporativa do stf">Programa Cidadania Corporativa</a><a href="javascript:void(0);" rel="popover" data-content="<p>Fonte: intranet STF. Seção de Programas Institucionais/CDPE/SGP.</p>" data-toggle="popover" data-size="popover-small"><sup>13</sup></a>, com o objetivo de sensibilizar servidores e demais colaboradores que atuam no Tribunal, visando à prática dos valores institucionais para o cumprimento da  <a href="http://goo.gl/Hsxnh7" target="_blank" title="">missão</a> e o alcance da visão<a href="javascript:void(0);" rel="popover" data-content="<p>Planejamento Estratégico - Supremo Tribunal Federal - ciclo 2012-2014 - Visão STF - Assegurar a guarda da Constituição por meio de prestação jurisdicional célere, acessível, módica e efetiva.</p>" data-toggle="popover" data-size="popover-small"><sup>14</sup></a>  de futuro do STF.</p>
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <h4 class="subTitulo textAlignCenter">A MISSÃO DO STF</h4>
                    <p>Planejamento Estratégico - Supremo Tribunal Federal - ciclo 2012-2014 - Missão do STF - Incumbe, ao Supremo Tribunal Federal, no desempenho de suas altas funções institucionais e como garantidor da intangibilidade da ordem constitucional, o grave compromisso – que lhe foi soberanamente delegado pela Assembleia Nacional Constituinte – de velar pela integridade dos direitos fundamentais, de repelir condutas governamentais abusivas, de conferir prevalência à essencial dignidade da pessoa humana, de fazer cumprir os pactos internacionais que protegem os grupos vulneráveis expostos a injustas perseguições e a práticas discriminatórias, de neutralizar qualquer ensaio de opressão estatal e de nulificar os excessos do Poder e os comportamentos desviantes de seus agentes e autoridades, que tanto deformam o significado democrático da própria Lei Fundamental da República.</p>
                  </div>
                </div>
                <div class="clear"></div>
                <p>O Programa estimula os envolvidos a assumirem o papel de “servidor-cidadão”, consciente não só dos direitos, mas também dos deveres que estão implícitos nessa designação.</p>
                <p>Para tanto, sugere que a melhor forma de assumir esse papel é internalizando os valores institucionais e externando-os na prática cotidiana. Nossos valores são:</p>
                <p class="textAlignCenter"><a href="../include/img/aulas/ValoresDoSTF.jpg" title="clique para ampliar" target="_blank"><img src="../include/img/aulas/ValoresDoSTF.jpg" style="max-width:500px;" alt="ética credibilidade responsabilidade social e ambiental inovação transparência valorização do capital humano celeridade acessibilidade respeito" /></a></p>
                <p class="textAlignCenter fonteMenor">Figura 3 – Banner Programa Cidadania Corporativa STF.</p>
                <p>Em 2015, o valor em destaque é “Inovação”, que se traduz na busca por soluções inovadoras para melhoria da prestação jurisdicional. Incentiva-se a criatividade, a modernidade e a quebra de paradigmas.  Para tanto, algumas dicas são destacadas. Abaixo listamos, com adaptações para nosso curso, essas dicas.. Clique em cada uma delas para conhecer os detalhes:</p>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Questione com mais frequência
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <p>Incorpore as perguntas “Por que...?” e “E se...?”. Isso o estimulará a enxergar as coisas de forma diferente. Lembrando: a criatividade requer disposição para investigar, aprender, questionar e propor inovações.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Revise os processos de trabalho periodicamente
                    </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                    <p>Busque sempre a melhoria. Para isso, ouça todos os envolvidos e construa um ambiente participativo. Esse procedimento costuma dar trabalho, mas os resultados são surpreendentes.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Invista no seu desenvolvimento 
                    </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                    <p>Procure conhecer realidades diferentes. Isso lhe proporcionará reflexões que contribuirão para o seu crescimento profissional e para a melhoria do seu trabalho.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading4">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                      Faça pausas
                    </a>
                    </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                    <p>Distanciar-se um pouco do problema, procurando fazer outra coisa, oxigena o cérebro e muda o foco. Posteriormente, a questão pode ser reavaliada com mais tranquilidade e maior probabilidade de êxito.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading5">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                      Estabeleça parcerias
                    </a>
                    </h4>
                  </div>
                  <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <div class="panel-body">
                    <p>Compartilhe seus anseios e busque soluções que favoreçam todos os envolvidos. Agindo assim, você fortalece a integração, divide as responsabilidades e multiplica os resultados.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading6">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                      Seja resiliente
                    </a>
                    </h4>
                  </div>
                  <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <div class="panel-body">
                    <p>Não deixe a dificuldade abatê-lo. Tenha uma certeza: a implementação de ideias inovadoras requer paciência, determinação e persistência. Não se intimide, vá em frente!</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading7">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                      Descubra seu talento
                    </a>
                    </h4>
                  </div>
                  <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                    <div class="panel-body">
                    <p>Indiscutivelmente, fazemos melhor o que mais gostamos. Em sua rotina de trabalho, identifique aquilo que mais lhe agrada fazer e invista no desenvolvimento dessa habilidade.</p>
                    </div>
                  </div>
                  </div>                                                                        
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading8">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                      Busque desafios. Tenha metas desafiadoras e reais
                    </a>
                    </h4>
                  </div>
                  <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                    <div class="panel-body">
                    <p>Ser criativo é ver oportunidade enquanto outros veem problema. Procure se inspirar naquilo que é difícil e subestimado por muitos e surpreenda-se com o resultado.</p>
                    </div>
                  </div>
                  </div>                  
                </div>  
                  <p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
                  <div class="bordaPalavraAutor">
                    <h4 class="subTitulo">Para refletir!</h4>
                    <p>Busque a assertividade nas contratações - para mitigar o medo de errar, que tal pensar positivamente, levantar o máximo de informações sobre o assunto, conversar com pessoas que entendam a realidade em estudo, analisar as possibilidades e planejar muito?</p>
                    <p>Enriqueça seu trabalho e possibilite que a sua experiência seja útil na resolução de problemas de outro órgão. </p>
                  </div>

                  <p>Se você chegou até aqui, parabéns. A Administração Pública precisa do seu talento. Utilize os conhecimentos adquiridos neste treinamento e faça do seu trabalho uma referência em gestão. Deixe a sua marca. Vejamos o vídeo a seguir.</p>
                  <p><a href="../include/file/sua_marca.pps" target="_blank">Vídeo Sua Marca</a></p>


                
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina16.php', 'aula5pagina18.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


