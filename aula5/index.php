<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

  $acessibilidadeTxt = null;
  if ($acessibilidade == "sim") { 
    $acessibilidadeTxt = "?ac=sim";
  }; 
configHeader('Bem-vindo', 'exibir', '5','1', '19', 'index.php', 'aula5pagina2.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

    <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p><span class="semi-bold">Caros colegas,</span></p>
                <p>Bom dia, boa tarde, boa noite!</p>
                <p>Sejam bem-vindos.</p>
                <p>Na aula de hoje, abordaremos dois temas de grande relevância para a Administração Pública: os resultados e as boas práticas. Veremos também sugestões de referenciais que os gestores podem utilizar para melhorar seu desempenho no trabalho.</p>
                <p>Vamos iniciar?</p>
                <h3 class="titulo">Objetivos desta aula</h3>
                <div class="col-lg-6 col-md-6 col-sm-6 ">
                  <p>Prezado participante,</p>
                  <p>Nesta aula, você conhecerá os chamados “resultados”, tão buscados pela Administração Pública. Saberá para que servem e por que são importantes. Terá noção também as denominadas boas práticas nas contratações, alvo de interesse de um bom gestor. Além disso, entenderá a importância do papel do servidor público na excelência da gestão. </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
                  <img src="../include/img/aulas/mapaMentalAula5Tela1.jpg" alt="" class="imgAulas" />
                </div>
                <div class="clear espacamentoLista"></div>              
              </div>
            </div>   
           </div>
        </div>
    </article>    

    <footer>  
      <nav role="navigation" class="">
        <div class="textAlignCenter ">
          <a href=<?php echo '"aula5pagina2.php'.$acessibilidadeTxt.'"';?> class="btn btn-lg btn-success" title="Ir para próxima página">Avan&ccedil;ar <i class="fa fa-arrow-circle-o-right">  </i></a>
        </div>
      </nav>
    </footer>  

<!-- <?php  //configNavegacaoRodape('exibir', 'index.php', 'aula5pagina3.php'); //Rodapé automático aqui  ?>-->         
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


