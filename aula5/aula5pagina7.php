<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Boas Práticas nas Contratações Públicas', 'exibir', '5','7', '19', 'aula5pagina6.php', 'aula5pagina8.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">2.   BOAS PRÁTICAS NAS CONTRATAÇÕES PÚBLICAS</h3>
                <p>É comum ouvirmos falar que o gestor público deve buscar boas práticas nas contratações. </p>
                <p class="abreCaixa" id="0">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
                <div id="caixa-0">
                  <div class="col-lg-8 col-md-8 col-sm-8" >
                    <p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Mas o que são boas práticas?</p>
                    <hr class="linhaInterrogacao" />
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
                  <div class="clear"></div>                  
                  <h4 class="subTitulo">2.1. O QUE SÃO? </h4>
                  <p>O Centro de Estudos e Pesquisas de Administração Municipal – Cepam (2013) - registra que boas práticas de gestão são aquelas que apresentam respostas a problemas. Tendem a ser eficazes e criativas para novos problemas ou para problemas antigos. Produzem resultados positivos e constituem solução aberta, que pode ser complementada, melhorada, transformada pelos gestores que a adotam (CRUZ e SALGADO, 2013).</p>
                  <div class="espacamentoBottom">
                    <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="cerebro" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-1">
                      <p class="textAlignCenter subTitulo">CEPAM</p>
                      <p>O Centro de Estudos e Pesquisas de Administração Municipal (Cepam) é uma fundação do governo do estado de São Paulo, vinculada à Secretaria de Planejamento e Gestão, que apoia os municípios no aprimoramento da gestão e no desenvolvimento de políticas públicas.</p>
                      <p> Fonte: <a href="http://www.cepam.org/institucional/quem-somos.aspx" target="_blank" title="acesse o SIPAM">http://www.cepam.org/institucional/quem-somos.aspx</a>.</p>
                      <p>Acesso em 7/02/2015.</p>
                    </div>
                  </div>
                  <div class="clear"></div>
                  <p>As boas práticas constituem ações, experiências, ideias bem sucedidas, procedimentos e normativos que favoreçam o melhor aproveitamento de recursos públicos, levando em conta aspectos importantes, tais como a agregação de valor ao negócio e a gestão de riscos. Buscam também atender às disposições legais e, ainda, aos princípios da Administração Pública, p. ex., legalidade, economicidade, eficiência, publicidade, impessoalidade, isonomia e moralidade (Guia de boas práticas em contratação de soluções de Tecnologia da Informação, TCU, 2012).</p>
                  <h4 class="subTitulo">2.2.  PARA QUE SERVEM?</h4>
                  <p>De modo geral, as boas práticas servem como possíveis referências para avaliação e, se for o caso, adoção pelos gestores.  Algumas boas práticas constam de normativos que não são diretamente aplicáveis a todas as áreas, porém, conforme destaca o TCU, “é interessante a adoção voluntária desses normativos, tendo em vista que representam boas práticas em contratação”. </p>
                  <p>Como exemplos desses casos, temos o <a href="http://www.planalto.gov.br/ccivil_03/decreto/d2271.htm" target="_blank" title="acesso ao decreto 2271">Decreto 2.271/1997</a>, que não se aplica a entidades como as estatais. Outro exemplo são as instruções normativas da Secretaria de Logística e Tecnologia da Informação – SLTI (p.ex., IN - SLTI 4/2014), que não são de cumprimento obrigatório para órgãos não pertencentes ao Sistema de Administração dos Recursos de Informação e Informática (SISP). Apesar da não obrigatoriedade, convém analisar a adoção das ações constantes desses normativos, naquilo que for possível, pois representam boas práticas que foram assim definidas em decorrência de estudos técnicos, inspeções, auditorias, etc.</p>
                </div>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina6.php', 'aula5pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


