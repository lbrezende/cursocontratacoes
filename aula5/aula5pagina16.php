<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('A Importância do Comprometimento com o Resultado', 'exibir', '5','16', '19', 'aula5pagina15.php', 'aula5pagina17.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">4.  A IMPORTÂNCIA DO COMPROMETIMENTO COM O RESULTADO</h3>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <p>Ao longo deste treinamento, foi possível constatar que o gestor público será exigido cada vez mais em relação à qualidade de seu trabalho. A Administração Pública e a sociedade necessitam desse labor.</p>
                  <p>A aplicação dos conceitos relacionados ao planejamento, à governança, à gestão de riscos, direcionada para as contratações públicas, é uma realidade que veio para ficar. </p>
                  <p> Porém, nada pode ser executado sem a participação efetiva dos servidores públicos, em especial dos gestores. Muitas vezes, o gestor público não possui a exata noção da relevância e da importância de sua atuação. Mas, acredite, você e o seu trabalho são indispensáveis para a excelência da gestão pública.</p>
                  <p>Necessitamos de uma ação urgente e efetiva dos gestores públicos brasileiros para mudar para melhor o nosso país. O vídeo ao lado mostra como podemos mudar.</p>    
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <p class="textAlignCenter subTitulo">Seja a mudança que você quer ver no mundo!</p>
                  <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/V0qlfK6XTDc" frameborder="0" allowfullscreen></iframe>
                  </div><br><br>
                </div>
                <div class="clear"></div>
                <p>O Manual para gestores do STF destaca a necessidade de “mobilização permanente dos gestores, nos diversos níveis da estrutura orgânica de cada instituição”. Esse movimento é extremamente relevante, tendo em vista que essa mobilização impulsiona as mudanças necessárias para aprimorar a qualidade do processo administrativo de contratação.</p>
                <p>Ainda de acordo com o Manual:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Esse comprometimento é essencial para o fiel cumprimento dos objetivos estratégicos estabelecidos e para a obtenção de resultados que possam ser acolhidos, compreendidos e aprovados não só pelos órgãos de controle, mas sobretudo pelo cidadão brasileiro, que exerce controle social soberano na avaliação dos atos públicos.</p>
                </div>
                <div class="clear"></div>
                <h4 class="subTitulo">4.1.  FAZENDO MAIS E MELHOR</h4>
                <p>Várias são as possibilidades de melhorar ainda mais a qualidade das contratações públicas. Isso depende, em boa parte, mas não somente, do empenho individual de cada um de nós.</p>
                <p class="textAlignCenter subTitulo">Faça pequenas mudanças e grandes resultados aparecerão<a href="javascript:void(0);" rel="popover" data-content="<p>Artigo da autora americana <a href='http://kathycaprino.com/' target='_blank' title=''>Kathy Caprino</a>.</p>" data-toggle="popover" data-size="popover-small"><sup>12</sup></a>.</p>
                <p>Uma pesquisa realizada pela startup <a href="https://www.blinkist.com/" target="_blank" title="acesse o Blinkist">Blinkist</a>, situada em Berlim, em livros de desenvolvimento pessoal e autoajuda detectou que os conceitos relacionados ao desenvolvimento humano são muito similares, mesmo que em contextos distintos. O resultado da pesquisa trouxe à tona alguns tópicos, entre os quais destacamos (ESPAÑA, D; GALVÃO, L, 2014):</p>
                <div class="paddingBottom300">
                  <p class="abreCaixa" id="0"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-0">
                    <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /><span class="semi-bold"> Descubra o que move você</span> – façamos uma reflexão para descobrir nossos pontos fortes e fracos. Procure relacionar os fatores motivadores de sua atuação.</p>
                    <p class="abreCaixa" id="1"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                    <div id="caixa-1">
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /><span class="semi-bold"> Para ter êxito/sucesso, pratique ao máximo seu ofício e aprenda com os outros</span> – utilize o benchmarking para saber as dificuldades e experiências que deram certo no órgão vizinho ao seu. Partilhe você também seus casos de sucesso.</p>
                      <p class="abreCaixa" id="2"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                      <div id="caixa-2">
                        <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /><span class="semi-bold"> Seja efetivo, não apenas eficiente</span> - exercite as boas práticas de gestão e acredite nos resultados positivos advindos delas. Procure não repetir erros ocorridos em processos anteriores.</p>
                        <p class="abreCaixa" id="3"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                        <div id="caixa-3">
                          <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /><span class="semi-bold"> Preocupe-se em criar relações onde todos ganham</span> – faça/Implemente parcerias com outras áreas de seu órgão. Facilite a comunicação. Conheça seus colegas. Entenda as dificuldades deles e procure ajudá-los no que for necessário para dar celeridade ao processo, evitando interrupções desnecessárias no fluxo de tramitação processual da demanda. Mantenha relações pautadas no “ganha-ganha”, com constante busca pelo desenvolvimento pessoal.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>                 
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <p>Uma startup é uma empresa nova, até mesmo embrionária ou ainda em fase de constituição, que conta com projetos promissores, ligados à pesquisa, investigação e desenvolvimento de ideias inovadoras.</p>
                    <p>Fonte: <a href="https://goo.gl/hYLhEf" target="_blank" title="">https://goo.gl/hYLhEf</a></p>
                    <p>Acesso em 18/02/2015.</p>

                  </div>
              </div>
              <div class="clear"></div>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina15.php', 'aula5pagina17.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


