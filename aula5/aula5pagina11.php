<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referenciais em Boas Práticas nas Contratações', 'exibir', '5','11', '19', 'aula5pagina10.php', 'aula5pagina12.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				<p><span class="semi-bold">Utilizar listas de verificação (check-list) na elaboração dos artefatos</span> – a utilização de listas desse tipo gera previsibilidade ao trabalho, além de possibilitar que servidores substitutos compreendam com maior facilidade os pontos a serem observados em cada situação.  O Guia de boas práticas em contratação de soluções de Tecnologia da Informação do TCU (2012, p. 228) traz nos apêndices A, B e C modelos dos artefatos citados, que também podem ser usados como listas de verificação do conteúdo desses. Sugere-se que essas listas sejam anexadas aos processos de contratação e de gestão do contrato.</p>
				<p><span class="semi-bold">Documentar todas as interações com agentes internos e externos</span> – o Guia sugere como boa prática o registro de todas as interações ocorridas entre o órgão e as empresas durante o planejamento, execução da licitação e a gestão contratual. Nesse sentido, consultas a outros órgãos, sítios de internet, empresas, pesquisas de mercado, questionamentos ao edital e respectivas respostas devem ser documentados no processo licitatório pelas áreas envolvidas em cada situação.</p>
				<p>A medida também vale para a fase de execução contratual, assim, reuniões com contratadas, elucidação de dúvidas, ofícios encaminhados, respostas da empresa, aplicação de sanções, demais ocorrências de execução, ou seja, toda interação com a contratada ao longo da gestão contratual deve ser documentada pelo fiscal/gestor do contrato. </p>
				<p>O Guia do TCU (2012, p. 236) traz o seguinte exemplo:</p>
				<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
				<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
					<p class="fonteMenor">(...) no caso de contratação de serviço de service desk, alguns dos documentos que podem constar nos autos do processo de fiscalização são expostos a seguir: relatórios mensais entregues pela contratada com o registro de todas as demandas tratadas, relatórios necessários aos recebimentos provisórios, elaborados pelo fiscal do contrato, bem como relatórios necessários para efetuar os recebimentos definitivos, que devem ser elaborados pela comissão de recebimento.</p>
				</div>
				<div class="clear"></div>
				<p><span class="semi-bold">Desenvolver a cultura de controles internos baseados em riscos</span> – conforme estudado na aula anterior, a gestão e o gerenciamento de riscos são de extrema importância para a Administração Pública. Apesar de essencialmente necessário o envolvimento da alta administração no processo, cabe a cada gestor buscar meios para que os riscos inerentes às contratações sejam identificados, avaliados e as providências para o tratamento desses riscos sejam definidas e executadas. </p>
				<p>Deve haver uma parceria entre a alta administração e seus servidores em prol da melhoria na gestão dos recursos públicos. Vale destacar que formação e treinamento específicos são fundamentais e necessários, dada a complexidade dos conceitos e do trabalho a ser executado na gestão e no gerenciamento de riscos.</p>
				<p><span class="semi-bold">Capacitação de servidores</span> – constante em contratação e em gestão de contratos. Considerando-se o dinamismo das técnicas de gestão e tendo em vista os diversos normativos afetos à área de licitações e contratos, os servidores devem acompanhar essas inovações por meio de aprendizado frequente. </p>
				<p>Nesse sentido, os órgãos devem buscar, por meio de suas áreas de gestão de pessoas, incentivos à participação em treinamentos específicos e ao desenvolvimento de competências necessárias para atuar com segurança na gestão de contratos. Acordos de cooperação entre os órgãos públicos também podem proporcionar alternativas para o aperfeiçoamento dos servidores.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina10.php', 'aula5pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


