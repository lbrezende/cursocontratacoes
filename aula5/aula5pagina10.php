<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referenciais em Boas Práticas nas Contratações', 'exibir', '5','10', '19', 'aula5pagina9.php', 'aula5pagina11.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p><span class="semi-bold">Exata definição da necessidade e dos resultados</span> - é essencial definir a necessidade e os resultados pretendidos com a contratação, independentemente de como a contratação ocorra (<a href="http://www.planalto.gov.br/ccivil_03/leis/l8666cons.htm" target="_blank" title="link para lei 8666">Lei 8.666/1993</a>,   art. 7º, inciso I, § 2º, inciso I, § 6º e § 9º)”(Guia de boas práticas em contratação de soluções de Tecnologia da Informação, TCU, 2012, p. 55). Sem o exato detalhamento da demanda e dos resultados a serem auferidos com a despesa, todo o processo de contratação estará prejudicado, tendo em vista a lacuna que poderá existir por falta de especificação ou imprecisão nos elementos avaliadores do resultado. Por exemplo, o Nível Mínimo de Serviço a ser exigido da contratada deve ser bem especificado para evitar dúvidas e possíveis contestações da empresa.</p>
                <p><span class="semi-bold">Produção de artefatos claramente definidos</span> - do processo de planejamento, devem resultar pelo menos três artefatos, para qualquer contratação:
                <br />
                a) estudos técnicos preliminares (Lei 8.666/1993, art. 6º, inciso IX 22); <br />
                b) plano de trabalho, no caso da contratação de serviços (Decreto 2.271/1997, art. 2º 23); <br />
                c) termo de referência ou projeto básico (Lei 8.666/1993, art. 7º, inciso I, § 2º, inciso I, §§ 6º e 9º).</p> 
                <p>É recomendável a adoção de controles internos administrativos para a elaboração dos artefatos necessários ao planejamento da contratação. O conteúdo dos artefatos é detalhado no Guia, conforme disposição a seguir. Trata-se somente de exemplo de como o Guia do TCU detalha o conteúdo dos artefatos. Cada caso deverá ser tratado individualmente. O importante é o gestor observar que deve planejar a contratação com detalhes.</p>
                <p class="textAlignCenter"><img src="../include/img/aulas/Aula5Tela10Figura1.png" alt="Detalhamento dos 3 artefatos do processo de planejamento: estudos técnicos preliminares, plano de trabalho e termo de referência ou projeto básico" /></p>
                <p class="textAlignCenter fonteMenor">Figura 1 – Artefatos do processo de planejamento de TI e respectivos conteúdos, de acordo com o TCU.</p>
                <p><span class="semi-bold">Documentar os artefatos</span> – <span class="semi-bold">Motivar as decisões</span> - a equipe de planejamento da contratação deve documentar os artefatos, com seus respectivos itens, nos autos do processo administrativo de contratação. O objetivo é registrar todos os documentos utilizados, de modo a possibilitar a fácil identificação e o entendimento por parte de todos os atores do processo, p.ex., áreas administrativas, assessoria jurídica, etc. (Guia de boas práticas em contratação de soluções de Tecnologia da Informação, TCU, 2012, p. 227). </p>
                <p>As informações, detalhes, dificuldades, motivações e decisões tomadas não devem ficar somente na memória dos servidores envolvidos no processo de contratação, devendo ser registrados nos autos do processo da contratação.</p>
                <p>A exigência de registro da motivação dos atos administrativos é obrigação legal. O art. 2º da <a href="http://www.planalto.gov.br/ccivil_03/leis/l9784.htm" target="_blank" title="acesse a lei 9784">Lei 9.784/1999</a><a href="javascript:void(0);" rel="popover" data-content="<p>Lei 9.784/1999, a qual regula o processo administrativo no âmbito da APF, como exposto a seguir: Art. 2º A Administração Pública obedecerá, dentre outros, aos princípios da legalidade, finalidade, motivação, razoabilidade, proporcionalidade, moralidade, ampla defesa, contraditório, segurança jurídica, interesse público e eficiência. Parágrafo único. Nos processos administrativos serão observados, entre outros, os critérios de: VII - indicação dos pressupostos de fato e de direito que determinarem a decisão; Art. 50. Os atos administrativos deverão ser motivados, com indicação dos fatos e dos fundamentos jurídicos, quando: I - neguem, limitem ou afetem direitos ou interesses...</p>" data-toggle="popover" data-size="popover-small"><sup>9</sup></a>, que regula o processo administrativo no âmbito da APF, disciplina que a Administração Pública obedecerá, entre outros, aos princípios da legalidade, finalidade, motivação, razoabilidade, proporcionalidade, moralidade, ampla defesa, contraditório, segurança jurídica, interesse público e eficiência. </p>
                <p>O art. 50 da mesma lei afirma que os atos administrativos deverão ser motivados, com indicação dos fatos e dos fundamentos jurídicos, quando neguem, limitem ou afetem direitos ou interesses.</p>
                <p>Como os atos administrativos que compõem o processo de contratação e de gestão do contrato podem afetar direitos e interesses de empresas, o registro da motivação é essencial. A medida favorece a necessária legalidade, celeridade e transparência.</p>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina9.php', 'aula5pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


