<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referenciais em Boas Práticas nas Contratações', 'exibir', '5','9', '19', 'aula5pagina8.php', 'aula5pagina10.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
                <div class="bordaPalavraAutor">
                  <p>Abaixo, destacamos dois exemplos de adaptação dessa equipe de planejamento, em outra área que não propriamente a TI, citando algumas possíveis atribuições de cada participante:</p>
                  <p>A equipe de planejamento da demanda poderá ser formada por integrantes das áreas a seguir, que auxiliarão nas seguintes atividades.</p>
                  <p><span class="subTitulo">Objeto da demanda 1</span>: contratação de empresa para prestação de serviços de apoio administrativo na área de auditoria médico-hospitalar, odontológica e correlatos, bem como assessoramento técnico do Plano de Saúde e Benefícios Sociais do Tribunal X.</p>
                  <p><span class="subTitulo">Representante da área requisitante, a saber, o Plano de Saúde e Benefícios Sociais do Tribunal X</span> - definição e detalhamento das necessidades do Plano de Saúde (em conformidade com as normas e regulamentos do Plano). Especificação e esclarecimento de quais são os serviços a serem executados, as condições de execução, equipamentos necessários e demais particularidades. Auxilia também na definição e no detalhamento dos critérios de avaliação e dos níveis mínimos de serviço exigidos (por exemplo, número de auditorias a serem realizadas no período, etc.), e normas técnicas específicas aplicáveis à contratação. Auxilia nos demais pontos relacionados ao planejamento, entre outras atividades.</p>
                  <p><span class="subTitulo">Representante da área de saúde do Tribunal X</span> – definição das normas e regulamentos específicos da área médica, aplicáveis à contratação (por exemplo, Conselho Federal de Medicina, Conselho Federal de Enfermagem, Conselho Federal de Odontologia, etc.), das condições de execução dos serviços, dos possíveis riscos envolvidos. Auxilia nos demais pontos relacionados ao planejamento, entre outras atividades.</p>
                  <p><span class="subTitulo">Representante da área administrativa</span> - definição dos documentos exigidos, avaliação da convenção coletiva aplicável, definição dos elementos formadores da Planilha de Formação de Preços (por exemplo, metodologia adotada para o cálculo do auxílio-transporte, valores e quantitativos de auxílio-alimentação, regime tributário e econômico, metodologia adotada para o cálculo dos encargos sociais, apuração da Bonificação e Despesas Indiretas – BDI<a href="javascript:void(0);" rel="popover" data-content="<p>Conceitualmente, denomina-se Benefícios ou Bonificações e Despesas Indiretas (BDI) a taxa correspondente às despesas indiretas e ao lucro que, aplicada ao custo direto de um empreendimento (materiais, mão-de-obra, equipamentos), eleva o a seu valor final. Fonte: <a href='http://portal2.tcu.gov.br/portal/pls/portal/docs/2055358.PDF' target='_blank' title='link para revista do tcu'>Revista do TCU</a>.</p>" data-toggle="popover" data-size="popover-small"><sup>8</sup></a>). Auxilia, ainda, nos aspectos relacionados à liquidação e ao pagamento dos serviços faturados pela futura contratada, sanções cabíveis, pagamento dos serviços prestados, repactuações e reajustes de materiais, especificação de uniformes, se necessário, providências para confecção de crachás para uso dos profissionais alocados, detalhes atinentes ao início e à vigência contratual. Auxilia em demais pontos relacionados ao planejamento, entre outras atividades.</p>
                  <p><span class="subTitulo">Objeto da demanda 2</span>: contratação de empresa especializada para prestação de serviços de Operação, Manutenção Preventiva e Corretiva de equipamentos do sistema de automação predial instalados no Órgão A, com posto de trabalho residente. </p>
                  <p>Nesse caso, o objeto, apesar de ser diretamente ligado à área de engenharia, envolve reflexamente a TI.</p>
                  <p><span class="subTitulo">Representante da área de engenharia, requisitante da solução</span> – definição e detalhamento das necessidades do Órgão, documentos exigidos, rotinas de manutenção, definição e detalhamento dos critérios de avaliação e dos níveis mínimos de serviço exigíveis, manuais e normas técnicas específicas aplicáveis à demanda. Previsão de cronograma de execução, de substituição de peças, componentes ou acessórios, dias e horários para realização dos serviços, pesquisas de preços de mercado do serviço a ser contratado, necessidade de relatórios mensais por meio de engenheiro da contratada. Demais pontos relacionados ao planejamento, entre outras atividades.</p>
                  <p><span class="subTitulo">Representante da área de TI</span> – avaliação de adequação e compatibilidade dos sistemas relacionados ao serviço em relação à plataforma, políticas de segurança, compatibilidade e aderência ao plano de TI do Órgão. Demais pontos relacionados ao planejamento, entre outras atividades.</p>
                  <p><span class="subTitulo">Representante da área administrativa</span> - definição dos documentos exigidos, avaliação de convenção coletiva aplicável, definição de elementos formadores da Planilha de Formação de Preços (por exemplo, metodologia adotada para o cálculo do auxílio-transporte de valores e quantitativos de auxílio-alimentação, regime tributário e econômico, metodologia adotada para o cálculo dos encargos sociais, apuração da Bonificação e Despesas Indiretas – BDI), aspectos relacionados à liquidação e ao pagamento dos serviços faturados pela adjudicatária, sanções cabíveis, pagamento dos serviços prestados, repactuações e reajustes de materiais, especificação de uniformes, providências para confecção de crachás para uso dos profissionais alocados, detalhes atinentes ao início e à vigência contratual. Demais pontos relacionados ao planejamento, entre outras atividades.</p>
                  <p>Contratações dessa natureza são complexas, assim, a composição da equipe objetiva melhorar o processo de planejamento, favorecendo o detalhamento, a especificação e a posterior execução dos serviços. Um planejamento bem feito facilita a elaboração de documentos instrutores da demanda, por exemplo, o projeto básico/termo de referência subsequentes.</p>
                  <p>Contratações mais simples podem não necessitar da formação da equipe de planejamento.</p>

                </div>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina8.php', 'aula5pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


