<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Avaliação de Resultados', 'exibir', '5','4', '19', 'aula5pagina3.php', 'aula5pagina5.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p>Acerca dos resultados, a área de TI vem consolidando, há alguns anos, mecanismos para medi-los nos dispositivos e serviços de informática. Um eficiente meio de medição de serviços contratados é a utilização de métodos modernos de gestão, entre eles os níveis de serviço. Como exemplo dessa prática, temos o SLA (<i>Service Level Agreement</i>), ou Acordo de nível de serviço (ANS), que é um acordo entre o provedor de serviços de TI e um cliente. Por meio do acordo de SLA, o serviço de TI é descrito, as metas de nível de serviço são documentadas e as responsabilidades do provedor de serviços de TI e do cliente são especificadas<a href="javascript:void(0);" rel="popover" data-content="<p>Termos do Information Technology Infrastructure Library – ITIL. Biblioteca de Infraestrutura de TI. É um conjunto de melhores práticas para gestão de serviços em Tecnologia da Informação. Estas práticas estão contidas em sete, entre esses livros os dois primeiros se destacam: Service Support (Suporte a Serviços) e Service Delivery (Entrega de Serviços). Fonte: <a href='https://consultorti.wordpress.com/2008/07/08/o-que-e-itil-cobit-e-bsc/' target='_blank' title=''>https://consultorti.wordpress.com/2008/07/08/o-que-e-itil-cobit-e-bsc/</a>. Acesso em 2/02/2015</p>" data-toggle="popover" data-size="popover-small"><sup>5</sup></a>.</p>
                <p>Por oportuno, registramos que, na esfera pública, o TCU orienta que a expressão a ser utilizada em editais de licitação e em termos de referência é “Nível Mínimo de Serviço”, que é apropriada para caracterizar o requisito mínimo de qualidade de serviço a ser prestado pelo fornecedor contratado pela APF (Nota Técnica 6/2010 -Sefti/TCU, 2011).</p>
                <p>A adoção de níveis de serviço em contratos da Administração Pública fora da área de TI ainda é muito recente (LARA, et al. 2013, p.5). A complexidade na definição de indicadores e o nível de detalhamento que o planejamento requer acabam por retardar a adoção desse método. Em que pese essa dificuldade, a utilização de ANS possibilita que sejam alcançados resultados positivos para o órgão, com a melhoria na qualidade do serviço prestado, tendo em vista que a contratada busca melhorar seu desempenho para atender aos níveis especificados. </p>
                <p>Um exemplo de aplicação de ANS fora da área de TI é a Intendência da Cidade Administrativa, órgão criado em 2011 pelo governo de Minas Gerais, para “gerir bens e serviços e planejar, coordenar, normatizar e executar atividades necessárias à operação da <a href="https://goo.gl/aOZIRX" target="_blank" title="acessar site da cidade administrativa">Cidade Administrativa Presidente Tancredo de Almeida Neves (CA)</a><a href="javascript:void(0);" rel="popover" data-content="<p>A Cidade Administrativa Presidente Tancredo Neves foi criada para centralizar e abrigar a administração direta do Estado de Minas Gerais. O moderno complexo de prédios foi projetado pelo mestre da arquitetura Oscar Niemeyer. Projetado para 17 mil servidores trabalharem reunidos.</p>" data-toggle="popover" data-size="popover-small"><sup>6</sup></a>, visando à qualidade do gasto”(Lei Delegada n° 180/2011, art. 16). </p>
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <p class="textAlignCenter subTitulo">INTENDÊNCIA DA CIDADE ADMINISTRATIVA</p>
                    <p>É um órgão criado para gerir o complexo em que se encontra o núcleo central do governo mineiro, nasceu com a proposta de utilizar-se de métodos modernos de gestão, entre eles os ANS. Esta medida visa uma melhor dinâmica operacional na execução dos serviços pelos fornecedores a partir da quantificação do grau de qualidade desejado, vinculando o pagamento da parcela contratual ao índice de desempenho alcançado. </p>
                    <p>Fonte: Acordo de Nível de Serviço e Eficiência na Gestão Contratual: O Caso Da Cidade Administrativa. Rodrigues, Gabriel Lara. Kênnya Kreppel Dias Duarte. Maria Virgínia Sena Tomich. Renata Resende Coelho. VI Consad de Gestão Pública. Brasília 16,17 e 18 abril 2013.</p>
                  </div>
                </div>
                <p>Entre outras competências, cabe à Intendência da CA suprir as necessidades de aproximadamente 17 mil usuários, “bem como gerir os contratos, considerando os níveis de serviços a eles associados, com vistas à otimização logístico-operacional e do gasto público”; inerentes à operação do empreendimento (Lei Delegada n° 180/2011, art. 16).
Possuem verificação mensal de indicadores de ANS, por exemplo, os contratos de manutenção de elevadores, de vigilância patrimonial, e de reprografia e impressão. Abaixo, listamos alguns indicadores associados a esses contratos (LARA, et al. 2013, p.5).
                </p>
                <div class="clear"></div>

                <div class="col-lg-6 col-lg-offset-3"> 
                    <div class="bloco-titulo-red">Indicadores do contrato de:</div>
                    <div class="bloco bloco-dark-blue"> 
                        <img class="img-dark-blue hidden-xs" src="../include/img/aulas/bloco-dark-blue.png">
                        <div class="titulo-bloco">
                             MANUTENÇÃO PREDIAL
                        </div>
                        <div class="botao-bloco-dark-blue" style="text-align:center">
                              <a href="javascript:void(0);"class="btn btn-white botao-bloco-dark-blue">Clique para ver os indicadores</a>
                        </div>
                        <div class="conteudo-bloco conteudo-bloco-dark-blue">  
                            <ul>  
                                <li>  Ordens de Serviço - OSs corretivas fechadas no prazo especificado.</li>
                                <li>  OSs preventivas realizadas.</li>
                                <li>  OSs corretivas X OSs preventivas.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="bloco bloco-blue"> 
                        <img class="img-blue hidden-xs" src="../include/img/aulas/bloco-blue.png">
                        <div class="titulo-bloco">
                             VIGILÂNCIA
                        </div>
                        <div class="botao-bloco-blue" style="text-align:center">
                              <a  href="javascript:void(0);"class="btn btn-white botao-bloco-blue">Clique para ver os indicadores</a>
                        </div>                        
                        <div class="conteudo-bloco conteudo-bloco-blue">  
                            <ul>  
                                <li>  Ocupação dos Postos de Trabalho Exigidos.</li>
                                <li>  Tempo de Execução das Rondas Noturnas.</li>
                                <li>  Qualidade dos Serviços de Segurança, Vigilância.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="bloco bloco-green"> 
                        <img class="img-green hidden-xs" src="../include/img/aulas/bloco-green.png">
                        <div class="titulo-bloco">
                             REPROGRAFIA
                        </div>
                        <div class="botao-bloco-green" style="text-align:center">
                              <a  href="javascript:void(0);"class="btn btn-white botao-bloco-green">Clique para ver os indicadores</a>
                        </div>                          
                        <div class="conteudo-bloco conteudo-bloco-green">  
                            <ul>  
                                <li>  Tempo desobstrução de papel.</li>
                                <li>  Tempo de troca de itens de consumo.</li>
                                <li>  Consertos e/ou substituição de equipamentos.</li>
                                <li>  Indisponibilidade dos sistemas.</li>
                                <li>  Atendimento às ordens de serviço pelas centrais de reprografia.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina3.php', 'aula5pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


<script>
  $(document).ready(function(){
    $('.conteudo-bloco').hide();
    $('.botao-bloco-dark-blue').click(function(){
          $('.botao-bloco-dark-blue').hide();
          $('.conteudo-bloco-dark-blue').show('slideUp');
    });
    $('.botao-bloco-blue').click(function(){
          $('.botao-bloco-blue').hide();
          $('.conteudo-bloco-blue').show('slideUp');
    });
    $('.botao-bloco-green').click(function(){
          $('.botao-bloco-green').hide();
          $('.conteudo-bloco-green').show('slideUp');
    });        
  })

</script>
