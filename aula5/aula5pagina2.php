<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Avaliação de Resultados', 'exibir', '5','2', '19', 'index.php', 'aula5pagina3.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">1.  AVALIAÇÃO DE RESULTADOS</h3>
                <p>É muito comum ouvirmos falar em avaliação de resultados, benefícios a serem auferidos com os resultados, resultado que se pretende alcançar, resultados compatíveis, gestão por resultados ou somente demonstração dos resultados a serem alcançados com a contratação.</p>
                <p>Trata-se de palavra recorrente em diversas publicações relacionadas ao setor público. A título de exemplo, a palavra “resultados” aparece 165 vezes no Guia de boas práticas em contratação de soluções de TI do Tribunal de Contas da União - TCU (2012), 41 vezes no livro Licitações e contratos do TCU (2010) e esteve presente em 1125 acórdãos desse Órgão<a href="javascript:void(0);" rel="popover" data-content="<p>Pesquisa efetuada no sitio do TCU. Disponível <a href='https://contas.tcu.gov.br/juris/Web/Juris/ConsultarTextual2/Jurisprudencia.faces' target='_blank' title=''>clicando aqui</a>. Acesso em 3/02/2015.</p>" data-toggle="popover" data-size="popover-small"><sup>1</sup></a> em 2014.</p>
                <p class="abreCaixa" id="0">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
                <div id="caixa-0">
                  <div class="col-lg-8 col-md-8 col-sm-8" >
                    <p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Mas, afinal, o que são resultados?</p>
                    <hr class="linhaInterrogacao" />
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
                  <div class="clear"></div>
                    <h4 class="subTitulo">1.1.  O QUE SÃO RESULTADOS?</h4>
                    <p>O Guia de boas práticas em contratação de soluções de TI do TCU (2012) traz uma boa definição da expressão “resultados”:</p>
                    <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                    <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                      <p class="fonteMenor">Os resultados pretendidos são os <span class="semi-bold">benefícios</span> diretos que o órgão <span class="semi-bold">almeja</span> com a contratação da solução, em termos de <span class="semi-bold">economicidade, eficácia, eficiência</span>, de melhor <span class="semi-bold">aproveitamento dos recursos humanos, materiais e financeiros disponíveis</span>, inclusive com respeito a impactos ambientais positivos (e.g. diminuição do consumo de papel ou de energia elétrica), bem como, se for caso, de melhoria da qualidade de produtos ou serviços, de forma a atender à necessidade da contratação. Deve-se ter em mente que os resultados pretendidos devem ser formulados em <span class="semi-bold">termos de negócio</span>, não de TI. (Grifamos).</p>
                    </div>
                    <div class="clear"></div>
                    <p>Apesar de direcionado para a área de TI, o conceito acima pode ser ampliado para as demais áreas das contratações públicas, tendo em vista que aborda pontos essenciais para nosso estudo, conforme será exposto a seguir.</p>
                    <p>Os resultados são expressos em relação aos benefícios que geram. Esses podem ser qualitativos e/ou quantitativos, p. ex., a troca de um sistema informatizado por outro mais amigável e moderno é um benefício qualitativo. Já o simples aumento de equipamentos padronizados do parque tecnológico de um tribunal pode ser um exemplo de benefício quantitativo. </p>
                    <p>Os resultados são almejados pela Administração. Para serem atingidos devem ter sido anteriormente especificados (desejados) por meio de um eficiente planejamento da demanda. Para que ocorra uma adequação entre o que se desejou, aquilo que foi efetivamente contratado e o que está sendo executado/entregue na situação real, devemos detalhar, com precisão, os termos do objeto da demanda com todas as suas peculiaridades.</p>
                    <h4 class="subTitulo">1.2.  PARA QUE SERVEM OS RESULTADOS? </h4>
                    <p>A questão de se receber o que foi contratado parece lógica, contudo, não é tão simples assim. </p>
                    <p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
                    <div class="bordaPalavraAutor">
                      <p>Certa vez, ouvi de uma colega o seguinte caso:</p>
                      <p>Uma prefeitura do interior desejava combater um surto de escorpiões que estava infestando a cidade. Para tanto, publicaram edital para aquisição de galinhas, com o objetivo de soltá-las pela cidade. Esses animais são inimigos naturais dos escorpiões e ajudam a controlá-los porque os comem. A licitação transcorreu sem maiores problemas. Chegado o dia da entrega das galinhas, encosta na frente da prefeitura um caminhão frigorífico para entregar o objeto licitado. Dentro do caminhão, todas as galinhas estavam perfeitamente congeladas e prontas para a entrega. Sim, esqueceram de especificar no objeto que as galinhas deveriam estar vivas. Nesse caso, fica fácil constatar que o resultado real não foi o pretendido.</p>
                    </div>
                    <p>Nesse contexto, os resultados servem para que a Administração verifique se o que foi contratado/entregue está em consonância com os termos do objeto demandado. Para o caso de entregas parceladas, essa verificação deverá ser constante, durante o período de vigência da contratação.</p>
                    <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 "><img src="../include/img/icons/BoaPratica.png" style="float:left;" /><p class="textAlignCenter bordaConceito">Boa Prática: Os olhos do gestor/fiscal do contrato devem estar sempre atentos em relação às características e aos termos de entrega/execução do objeto adquirido.</p></div>
                    <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                    <div class="clear"></div>
                    <p>O que dificulta esse procedimento, em muitos casos, é a falta de detalhamento dos termos a serem exigidos da futura contratada. Em regra, é essencial a exata mensuração das características e peculiaridades do produto/serviço que o poder público deseja adquirir. Isso é possível por meio de um planejamento eficiente, que produza projetos básicos, termos de referências, editais e contratos que não deixem pairar dúvidas quanto ao que se quer contratar ou adquirir. Também devem ser detalhados os termos de execução a serem exigidos posteriormente da futura contratada.</p>







                </div>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'index.php', 'aula5pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


