<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Avaliação de Resultados', 'exibir', '5','6', '19', 'aula5pagina5.php', 'aula5pagina7.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p>Recentemente, o TCU abordou a gestão por resultados no Acórdão 8105/2014, conforme excerto abaixo:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">159. No entanto, foram verificadas falhas relevantes nas áreas de controles internos, governança e <span class="semi-bold">gestão por resultados</span>, seja na própria Sexec/MCTI, ou nas unidades vinculadas ou contratadas (OS e UPs). Considerando que a maioria das atividades do Ministério é descentralizada ou desconcentrada para essas unidades especializadas, a fragilidade estrutural nos sistemas de controle e <span class="semi-bold">acompanhamento de resultados é grave</span>, e deve suscitar ressalva às contas dos gestores, conforme abaixo listado. (Grifamos).</p>
                </div>
                <div class="clear"></div>
                <p>Um exemplo normativo acerca dos parâmetros para aferição de resultados nos serviços de limpeza e conservação é a <a href="http://goo.gl/yTxEJl" target="_blank" title="link para Instrução Normativa Nº 02, de 30 de abril de 2008 do MPOG">Instrução Normativa Nº 02, de 30 de abril de 2008</a>, do MPOG, que dispõe sobre regras e diretrizes para a contratação de serviços, continuados ou não, apresentando, por exemplo, as orientações seguintes:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Art. 42. Deverão constar do <span class="semi-bold">Projeto Básico</span> na contratação de serviços de limpeza e conservação, além dos demais requisitos dispostos nesta IN:</p>
                  <p class="fonteMenor">I - áreas internas, áreas externas, esquadrias externas e fachadas envidraçadas, classificadas segundo as características dos serviços a serem executados, periodicidade, turnos e jornada de trabalho necessários etc;</p>
                  <p class="fonteMenor">II - <span class="semi-bold">produtividade mínima</span> a ser considerada para cada categoria profissional envolvida, <span class="semi-bold">expressa em termos de área física por jornada de trabalho</span> ou relação serventes por encarregado; e</p>
                  <p class="fonteMenor">III – exigências de sustentabilidade ambiental na execução do serviço, conforme o disposto no anexo V desta Instrução Normativa.</p>
                  <p class="fonteMenor">Art. 43. Os serviços <span class="semi-bold">serão contratados com base na Área Física a ser limpa</span>, estabelecendo-se uma estimativa do <span class="semi-bold">custo por metro quadrado</span>, observadas a peculiaridade, a produtividade, a periodicidade e a frequência de cada tipo de serviço e das condições do local objeto da contratação.</p>
                  <p class="fonteMenor">Parágrafo único. Os órgãos deverão <span class="semi-bold">utilizar as experiências e parâmetros aferidos e resultantes de seus contratos anteriores</span> para definir as produtividades da mão-de-obra, em face das características das áreas a serem limpas, buscando sempre fatores econômicos favoráveis à administração pública. (Grifamos).</p>
                </div>
                <div class="clear"></div>
                <p>Conforme exposto, a avaliação de resultados deve estar presente na pauta dos gestores públicos, que deverão incluí-la no planejamento das contratações. Para cada caso concreto, o gestor deverá buscar meios que possibilitem a medição da qualidade e/ou quantidade do objeto contratado.</p>
                <p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
                <div class="bordaPalavraAutor">
                  <p>Atualmente, não basta somente entregar o objeto contratado, exige-se mais. </p>
                  <p>A entrega deve atender aos critérios preestabelecidos de qualidade, prazo, entre outros, buscando incentivar a excelência técnica e operacional da contratada. Os indicadores de desempenho vinculam a remuneração à qualidade efetiva do serviço prestado. A inadequação do serviço poderá implicar glosa rápida no pagamento. </p>
                  <p>Sem os indicadores de desempenho, o procedimento é mais demorado, pois, em regra, problemas como atrasos injustificados e baixa qualidade do serviço normalmente resultam em multas à contratada. Esse procedimento é efetivado após demorado processo de apuração, assegurada ampla defesa.</p>
                  <p>Importante ressaltar que a implantação de indicadores não impede o regular procedimento de apuração de falhas e punição da contratada.</p>
                </div>
                <p>Em que pesem os benefícios da avaliação de resultados como técnica moderna de gestão, a questão encontra-se em fase de estudos e avaliação de implantação por parte dos órgãos públicos, tendo em vista as dificuldades em se medir exatamente os serviços demandados por meio de indicadores eficientes de desempenho. Nesse sentido: “A importação de modelos do setor privado é prática que demanda adaptações, mas que pode trazer resultados benéficos”(LARA, et al. 2013).</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina5.php', 'aula5pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


