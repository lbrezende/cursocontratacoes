<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Considerações Finais', 'exibir', '5','18', '19', 'aula5pagina17.php', 'aula5pagina19.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <div class="col-lg-6 col-md-6 col-sm-6">
					<h3 class="titulo">RESUMO DA AULA</h3>
					<p>Nesta aula, verificamos o que são os resultados para a Administração Pública, bem como as denominadas boas práticas de gestão.</p>
					<p>Fomos apresentados a três instrumentos, o Guia de boas práticas em contratação de soluções de TI, elaborado pelo TCU, o Guia de boas práticas em contratação de soluções de TI, da SLTI/ MPOG e o Manual para gestores de contratos do STF, que difundem boas práticas de gestão. Verificamos que, em que pese toda a informação teórica, o sucesso das contratações públicas depende particularmente da ação de cada servidor público e de seu ativo envolvimento.</p>
					<p>Obrigado pela participação!</p>
				</div>
                <div class="col-lg-6 col-md-6 col-sm-6">
					<h3 class="textAlignCenter titulo">MENSAGEM</h3>
					<p class="textAlignCenter"><img src="../include/img/aulas/mensagemAula5.jpg" alt="sucesso é quando você se sente bem consigo mesmo, com suas decisões" /></p>
                </div>
                <div class="clear"></div>


              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina17.php', 'aula5pagina19.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


