<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referências Bibliográficas', 'exibir', '5','19', '19', 'aula5pagina18.php', 'aula5pagina19.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">SUGESTÕES DE LEITURA COMPLEMENTAR</h3>
				<p>Livro: JONES, Lyndon; LOFTUS, Paul. <i>Organize o seu tempo</i>. Tradução: Lisete Sucupira. São Paulo: Clio, 2010.</p>
                <h3 class="titulo">REFERÊNCIAS BIBLIOGRÁFICAS</h3>
				<p>BRASIL. Ministério do Planejamento, Orçamento e Gestão. Secretaria de Logística e Tecnologia da Informação. <i>Guia de boas práticas em contratação de soluções de Tecnologia da Informação</i> V 2.0. Brasília – 2014. 206 p.</p>
				<p>BRASIL. Supremo Tribunal Federal. <i>Manual para Gestores de Contratos</i> – Brasília. 58 p.</p>
				<p>BRASIL. Tribunal de Contas da União. <i>Guia de boas práticas em contratação de soluções de Tecnologia da Informação: riscos e controles para o planejamento da contratação</i> / Tribunal de Contas da União. – Versão 1.0. – Brasília: TCU, 2012.</p>
				<p>BRASIL. Tribunal de Contas da União. <i>Licitações e contratos: orientações e jurisprudência do TCU</i> / Tribunal de Contas da União. – 4. ed. rev., atual. e ampl. – Brasília: TCU, Secretaria Geral da Presidência: Senado Federal, Secretaria Especial de Editoração e Publicações, 2010. 910 p.</p>
				<p>CAVALCANTI, Augusto Sherman. <i>O novo modelo de contratação de soluções de TI pela Administração Pública</i>. Belo Horizonte: Fórum, 2013.</p>
				<p>Constituição Federal Brasileira. </p>
				<p>CRUZ, Maria do Carmo Toledo; SALGADO, Silvia Regina da Costa. <i>Boas práticas de gestão – educação fiscal para a cidadania</i>. São Paulo: CEPAM, 2013.</p>
				<p>Decreto nº 2.271, de 7 de julho de 1997. </p>
				<p>ESPAÑA, D.; GALVÃO, L. <i>Desenvolvimento pessoal em 7 tópicos</i>. Disponível em: <a href="http://exame.abril.com.br/rede-de-blogs/o-que-te-motiva/2014/04/02/desenvolvimento-pessoal-em-7-topicos/" target="_blank" title="conheça os 7 tópicos para o desenvolvimento pessoal">http://exame.abril.com.br/rede-de-blogs/o-que-te-motiva/2014/04/02/desenvolvimento-pessoal-em-7-topicos/</a>. Abril, 2014. Com adaptações. Acesso em: 12/02/2015. </p>
				<p>Instituto Brasileiro de Governança Corporativa. <i>Código das melhores práticas de Governança Corporativa</i>. 4.ed. São Paulo, SP : IBGC, 2009. 73 p.</p>
				<p>Intranet do STF - Seção de Programas Institucionais/CDPE/SGP</p>
				<p>Lei 9.784/1999</p>
				<p>MINAS GERAIS. Lei Delegada 179, de 1º de janeiro de 2011. Belo Horizonte, 2011.</p>
				<p>MINAS GERAIS. Lei Delegada 180, de 20 de janeiro de 2011. Belo Horizonte, 2011.</p>
				<p>Planejamento Estratégico do STF ciclo 2012 - 2014</p>
				<p>RODRIGUES, Gabriel Lara. Et al. <i>Acordo de Nível de Serviço e Eficiência na Gestão Contratual: O caso da Cidade Administrativa</i>. VI Consad de Gestão Pública. Brasília 16,17 e 18 abril 2013. </p>
				<p>SLTI IN 4/2014 – MPOG</p>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina18.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


