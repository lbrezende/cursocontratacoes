<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Avaliação de Resultados', 'exibir', '5','3', '19', 'aula5pagina2.php', 'aula5pagina4.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h4 class="subTitulo">1.3.  COMO MEDIR RESULTADOS?</h4>
                <p>Conforme verificamos, os resultados são aferidos em termos de economicidade, eficácia, eficiência e melhor aproveitamento dos recursos humanos, materiais e financeiros disponíveis (Guia de boas práticas em contratações de soluções de Tecnologia da Informação: riscos e controles para o planejamento da contratação, TCU, 2012).</p>
                <p>Nesse sentido, podemos pensar na contratação de um novo sistema de informação para automatizar um processo de trabalho. Um dos resultados almejados pode ser a diminuição do tempo médio de execução de um determinado serviço, p.ex., emissão de uma certidão. O resultado aqui se refere à eficácia e à eficiência, tendo em vista que o serviço a ser oferecido pela solução deverá ser mais rápido<a href="javascript:void(0);" rel="popover" data-content="<p>Será mais rápido tendo em vista que a informatização gera, em regra, celeridade ao processo de trabalho.</p>" data-toggle="popover" data-size="popover-small"><sup>2</sup></a> do que o serviço oferecido manualmente, com menores custos<a href="javascript:void(0);" rel="popover" data-content="<p>Em regra haverá redução de custos tendo em vista que diminuirá a mão de obra envolvida diretamente com o processo de emissão da certidão. Devemos lembrar também que os custos com a aquisição da ferramenta serão diluídos ao longo do tempo de utilização da tecnologia entre as áreas beneficiadas pela solução de TI.</p>" data-toggle="popover" data-size="popover-small"><sup>3</sup></a>.</p>
                <p>Os aspectos relacionados aos resultados (economicidade, eficácia, eficiência, melhor aproveitamento de recursos humanos, materiais, etc.) podem ser verificados por meio da análise dos impactos da contratação no caso concreto e, ainda, dos impactos em outros processos/áreas do órgão. </p>
                <p>Explico: o benefício decorrente de uma contratação pode ser direto em função da aquisição do objeto da demanda, ou em virtude de impactos que essa aquisição poderá acarretar em outras atividades, setores, etc. Por exemplo, a aquisição de catracas eletrônicas por um tribunal pode gerar uma economia nos gastos com pessoal, possibilitando a melhor distribuição de seguranças em outros pontos de maior necessidade, evitando-se a contratação de novos postos de trabalho. Pode, também, gerar benefícios no fluxo de pessoas, processos, etc.</p>
                <p>Cabe à equipe de planejamento e ao futuro gestor especificar os meios e as ferramentas cabíveis para aferir esses parâmetros em cada caso. Essa questão fica clara na adaptação da frase de <a id="modal" title="clique aqui para saber mais sobre W. Edwards Deming" href="javascript:void(0);" style="padding-left:0px;" class="btn btn-primary btn-small" data-toggle="modal" data-target="#myModal">W. Edwards Deming <sup><img src="../include/img/icons/plus.jpg" width="10px" height="10px" alt="clique aqui para saber mais sobre W. Edwards Deming" style="margin-left:5px;" /></sup></a> “Não se gerencia o que não se mede, não se mede o que não se define, não se define o que não se entende, ou seja, não há sucesso no que não se gerencia”.</p>
                <p>Várias são as possibilidades de aferir resultados. A atenta verificação física do objeto entregue ao órgão, os testes padronizados, a observância de fatos relevantes nas contratações anteriores de mesmo objeto, a construção, implantação e monitoramento de indicadores de desempenho<a href="javascript:void(0);" rel="popover" data-content="<p>A construção e implantação de indicadores de desempenho tem por objetivo medir, da forma mais próxima ao real, o resultado almejado. Fonte: Acordo de nível de serviço e eficiência na gestão contratual - o caso da cidade administrativa. Disponível na biblioteca complementar do curso.</p>" data-toggle="popover" data-size="popover-small"><sup>4</sup></a> são alguns exemplos. É importante que a Administração Pública acumule e armazene experiências nas contratações para que possa aprimorar a avaliação de resultados. </p>
                <p>Por mais específico que seja o objeto a ser adquirido/licitado, dificilmente um planejamento de contratação partirá do ponto zero. Sempre haverá informações disponíveis para consulta, que deverão ser buscadas e agregadas ao planejamento. </p>
                <p>Como fontes de consulta, temos os autos de processos de contratações anteriores de natureza similar, contratações de outros órgãos, benchmarking, etc. Nesse ponto, a integração entre o poder público, de todas as esferas governamentais, é fundamental para o crescimento da expertise nas contratações.</p>
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <p class="textAlignCenter subTitulo">BENCHMARKING</p>
                    <p>“É uma ferramenta de gestão que consiste na mensuração da performance de uma organização, permitindo que ela compare sua eficiência com a de outras organizações, frequentemente com a empresa líder do segmento ou outro concorrente muito relevante. Esta prática não significa copiar o que a concorrência está fazendo, mas aprender com ela através da observação e comparação das melhores práticas.”</p>
                    <p>Fonte: <a href="http://goo.gl/pa1Evv" target="_blank" title="">http://goo.gl/pa1Evv</a></p>
                    <p>Acesso em 5/03/2015.</p>

                  </div>
                </div>
                <div class="clear"></div>


              </div>
            </div>   
           </div>
        </div>
    </article>    
<!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <br>
              <i class="icon-credit-card icon-7x"></i>
              <h4 id="myModalLabel" class="semi-bold">W. Edwards Deming</h4>
             
            </div>
            <div class="modal-body">
              <div class="row form-row" style="text-align:center;">
                <div class="row">

                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 "><img src="../include/img/aulas/wEdwards.jpg" alt="foto de W. Edwards Deming" style="width:133px"><br><br></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <p class="no-margin">Autor dos 14 pontos da gestão, foi considerado o “pai do renascimento japonês após a segunda guerra mundial e um mestre do gerenciamento de qualidade no mundo todo”. Fonte: <a href="http://goo.gl/rEwSl7" target="_blank" title="">http://goo.gl/rEwSl7</a></p>
              <br></div>
                </div>
                  
              </div>
            </div>
            <div class="modal-footer">
              <a class="btn btn-default" data-dismiss="modal">Voltar para a aula</a> 
              <a href="https://pt.wikipedia.org/wiki/William_Edwards_Deming"  target="_blank" class="btn btn-primary">Ver currículo de W. Edwards Deming</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->          
<?php  configNavegacaoRodape('exibir', 'aula5pagina2.php', 'aula5pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


