<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Manual do Gestor de Contratos do STF', 'exibir', '5','14', '19', 'aula5pagina13.php', 'aula5pagina15.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h4 class="subTitulo">3.3.  MANUAL DO GESTOR DE CONTRATOS DO STF</h4>
                <p>O STF editou, há alguns anos, o <a href="http://www.stf.jus.br/arquivo/cms/intranetNavegacao/anexo/Manuais_documentos/Manual_para_gestores_de_Contratos__Versao_1.0__17092011.pdf" target="_blank" title="acesse o manual para gestores de contratos">Manual para gestores de contratos</a><a href="javascript:void(0);" rel="popover" data-content="<p>O manual pode ser acessado em: Intranet – Manuais e Documentos – Manual para Gestores de Contratos.</p>" data-toggle="popover" data-size="popover-small"><sup>11</sup></a>. O Documento reitera o compromisso da Corte com o aprimoramento constante dos processos de trabalho, com a transparência e com a eficiência administrativa.</p>
                <p>O Manual tem por objetivo orientar, de modo simples e direto, os gestores e fiscais designados para acompanhar a execução dos contratos administrativos firmados pelo STF, quanto às melhores práticas e procedimentos aplicáveis ao gerenciamento desses ajustes. </p>
                <p>Objetiva, ainda, aperfeiçoar o monitoramento e o controle de contratos administrativos, acordos, convênios e demais instrumentos firmados pelo Tribunal e regulados pela Lei 8.666/93, e legislação correlata, por meio do detalhamento de responsabilidades, obrigações e prerrogativas do gestor (Manual para Gestores de Contratos, STF, p. 58).</p>
                <div class="espacamentoBottom">
                    <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                      <h4 class="subTitulo textAlignCenter">CONTRATO ADMINSTRATIVO</h4>
                      <p class="subTitulo">O que é?</p>
                      <p><span class="semi-bold">Contrato Administrativo</span> é o acordo de vontades, celebrado entre a Administração Pública e um terceiro, com o propósito de criar um vínculo capaz de gerar direitos e obrigações recíprocas, disciplinado por regras de Direito Público. Tem como princípio imediato a realização de um interesse público e distingue-se do contrato privado em face da supremacia da Administração (contratante) sobre o particular (contratado). Decorre de processo licitatório ou de contratação direta sem licitação para a execução de um objeto.</p>
                      <p class="subTitulo">Como formalizar?</p>
                      <p>Todo contrato nasce de uma necessidade da Administração pautada pelo interesse público justificado e circunstanciado. Tal necessidade deve ser formalizada por meio de memorando ou documento de encaminhamento, autuado sob a forma de processo administrativo. </p>
                      <p class="subTitulo">Quem é o responsável?</p>
                      <p>O gestor. Ele é o servidor da Administração designado pelo ordenador de despesa para acompanhar, controlar e fiscalizar os contratos administrativos, de modo a promover as medidas necessárias à correta execução do objeto contratado, de acordo com as condições previstas no ato convocatório, no instrumento de contrato e na legislação aplicada. P. 25.</p>
                    </div>
                </div>
                <div class="clear"></div>
                <p>Esse Manual é dividido em cinco partes, destacadas nos parágrafos seguintes:</p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"><!-- inicio de tabela de conceitos com accordion -->
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/contrato.png" alt="" border="0" class="" />&nbsp;Clique aqui para informações
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Contrato Administrativo</h4>
                     <p>Aborda pontos importantes acerca dos contratos administrativos, tais como, características essenciais, obrigatoriedade, formalidade e outros aspectos relacionados a eles. </p>
                    </div>
                  </div>
                  </div>
                  </div><!-- divisao em cols-->
                  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><!-- divisao em cols--></div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                      <img src="../include/img/icons/processo.png" alt="" border="0" class="" />&nbsp;Clique aqui para informações
                    </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Processo Administrativo</h4>
                    <p>Apresenta informações sobre o processo administrativo e a diferença entre processo principal e processo de pagamento.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><!-- divisao em cols--></div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                      <img src="../include/img/icons/gestor.png" alt="" border="0" class="" />&nbsp;Clique aqui para informações
                    </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Gestor</h4>
                    <p>Traz considerações sobre o gestor de contratos, atribuições, competências, designação, limites de atuação, etc. Esse tópico destaca o perfil desejado do gestor, conforme abaixo.</p>
                    </div>
                  </div>
                  </div>
                  </div>

                  <div class="clear"></div>                  
                </div>
                <p>Espera-se que o gestor possua, entre outras qualidades, as seguintes características:</p>
                <div class="paddingBottom300">
                  <p class="abreCaixa" id="0"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-0">
                    <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> conhecimento da legislação aplicada;</p>
                    <p class="abreCaixa" id="1"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                    <div id="caixa-1">
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> boa reputação ético-profissional;</p>
                      <p class="abreCaixa" id="2"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                      <div id="caixa-2">
                        <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> conhecimentos específicos do objeto a ser fiscalizado;</p>
                        <p class="abreCaixa" id="3"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                        <div id="caixa-3">
                          <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> organização e proatividade;</p>
                          <p class="abreCaixa" id="4"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                          <div id="caixa-4">
                            <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> atuação de maneira preventiva;</p>
                            <p class="abreCaixa" id="5"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                            <div id="caixa-5">
                              <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> acuidade: observar o cumprimento, pela contratada, das regras e dos prazos previstos no ato convocatório e no instrumento contratual;</p>
                              <p class="abreCaixa" id="6"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                              <div id="caixa-6">
                                <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> dinamismo: buscar os resultados esperados pela Administração e trazer benefícios e economia para o STF;</p>
                                <p class="abreCaixa" id="7"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                                <div id="caixa-7">
                                  <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> zelo e cuidado na manutenção do bem público.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>                


              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina13.php', 'aula5pagina15.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


