<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Manual do Gestor de Contratos do STF', 'exibir', '5','15', '19', 'aula5pagina14.php', 'aula5pagina16.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <div class="espacamentoBottom">
                    <div class="col-lg-1 col-md-1 col-sm-1 textAlignRight" ><img alt="" src="../include/img/icons/interrogacao_cinza.png" /></div>
                    <div class="col-lg-11 col-md-11 col-sm-11"><p><br /></p><p><span class="semi-bold">Dúvidas e Respostas</span> - nesse ponto, o Manual apresenta um mix de perguntas e respostas de interesse do gestor.  Abaixo, destacamos algumas (clique nas perguntas para abrir as respostas):</p></div>
                </div>
                <div class="clear"></div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Como monitorar o contrato para evitar a incidência de atos irregulares por parte da contratada?
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <p>O gestor deve analisar, minuciosamente, os termos contratuais, em especial aqueles relativos às obrigações da contratada, e verificar, com detalhes, o Projeto Básico/Termo de Referência, a proposta da contratada, a legislação pertinente e os princípios da Administração Pública, acompanhando com atenção a execução do contrato. O gestor deve elaborar um cronograma que contenha cada obrigação 
da contratada e a respectiva data de execução. O cronograma é uma ferramenta útil para a verificação do cumprimento das obrigações mensais da contratada.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Qual o prazo que o gestor tem para informar alguma irregularidade verificada na execução do contrato?
                    </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                    <p>O gestor deve formalizar imediatamente, no processo principal, qualquer irregularidade verificada na execução do contrato. </p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Como obter qualificação adequada para o exercício da função? 
                    </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                    <p>Solicitar à Secretaria de Recursos Humanos – SRH, caso necessário, treinamento específico. Outra forma de qualificação, que é bastante enriquecedora, advém da experiência de outros gestores da Administração, por meio da troca de informações. </p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading4">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                      Como evitar a pessoalidade e a subordinação direta na execução dos contratos de prestação de serviços?
                    </a>
                    </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                    <p>Exigindo da contratada a presença diária de seu representante, na pessoa do preposto, no local da prestação do serviço, conforme previsto no art. 68 da Lei 8.666/1993. É importante esclarecer que assuntos atinentes à execução das atividades definidas no contrato devem ser tratados entre o gestor e o preposto da contratada. Já os assuntos de cunho trabalhista (salário, férias, hora extra, frequência) devem ser discutidos entre a empresa (preposto) e seus empregados contratados, observando-se as comprovações de entrega dos documentos exigidos no contrato. Porém, o gestor também deve acompanhar e exigir da contratada o cumprimento das obrigações trabalhistas firmadas com os empregados dela.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading5">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                      Quais são os tipos de contratos que exigem gestão especial?
                    </a>
                    </h4>
                  </div>
                  <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <div class="panel-body">
                    <p>Todos os ajustes firmados pela Administração devem ser acompanhados e fiscalizados pelo gestor com zelo e acuidade. Contudo, são merecedores de atenção especial os contratos relativos a: serviços de conservação e limpeza, porque necessitam de acompanhamento diário; fornecimento de gêneros alimentícios, pelo fato de demandarem qualidade, higiene e segurança alimentar, entre outros; obras, tendo em vista as peculiaridades envolvidas.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading6">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                      Um servidor pode ser gestor de mais de um contrato?
                    </a>
                    </h4>
                  </div>
                  <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <div class="panel-body">
                    <p>Sim. Um servidor pode ser gestor de mais de uma contratação, na medida em que a gestão de contratos, sempre que possível, estará relacionada com a atividade da unidade de lotação do gestor. </p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="heading7">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                      Quais são as sanções para o mau gestor?
                    </a>
                    </h4>
                  </div>
                  <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                    <div class="panel-body">
                    <p>Se confirmada a postura inadequada do gestor, além da possibilidade de perda da função, caso exerça, o servidor é passível de sofrer as sanções previstas na Lei 8.666/1993 e na Lei 8.112/1990, observando-se as condições estabelecidas na Lei 9.784/1999, que regulamenta o processo administrativo no âmbito da Administração Pública Federal.</p>
                    </div>
                  </div>
                  </div>                                                                        
                </div>
                <div class="espacamentoBottom">
                   <div class="col-lg-1 col-md-1 col-sm-1 textAlignRight" ><img alt="" src="../include/img/icons/fluxo.png" /></div>
                    <div class="col-lg-11 col-md-11 col-sm-11"><p><span class="semi-bold">Fluxos de Contratações</span> – apresenta alguns fluxos importantes no processo de contratações, tais como Fluxo do Processo de Aquisição por Projeto Básico (PB) ou Termo de Referência (TR), Fluxo do Procedimento Licitatório – Projeto Básico (PB) ou Termo de Referência (TR), Fluxo do Processo de Dispensa de Licitação – Aquisição por Projeto Básico (PB) ou Termo de Referência (TR), Fluxo do Processo de Inexigibilidade de Licitação – Aquisição por Projeto Básico (PB) ou Termo de Referência (TR), entre outros.</p></div>
                </div>
                <div class="clear"></div>
                <p>Por fim, em um tópico destacado está o Glossário de Termos, que apresenta uma série de conceitos relacionados à seara de licitações e contratos. Nesse item, vale a pena destacar os seguintes conceitos:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor"><span class="semi-bold">Produtividade</span> é o índice que mede a capacidade de realização de determinado volume de tarefas, em um contexto específico de rotinas para a execução de serviços, considerados os recursos humanos, materiais e tecnológicos disponibilizados, o padrão de qualidade exigido e as condições do local de prestação do serviço. </p>
                  <p class="fonteMenor"><span class="semi-bold">Produtos ou Resultados</span> são os bens materiais e/ou imateriais, quantitativamente delimitados, a serem produzidos na execução do serviço contratado.</p>
                </div>
                <div class="clear"></div>
                <p>Assim, os manuais apresentados se sobressaem, entre outros, como instrumentos oportunos e necessários para orientar e nortear os gestores acerca de boas práticas atinentes à gestão dos processos administrativos contribuindo para aperfeiçoá-los, com foco direcionado para o interesse público. </p>
                <p>São, sem dúvida, instrumentos auxiliares aos gestores como relevantes ferramentas para auxiliá-los no cumprimento dos relevantes ofícios que lhe são conferidos e no desempenho de suas competências, sempre com qualidade e eficiência (Manual do STF, com adaptações).</p>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina14.php', 'aula5pagina16.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


