<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referenciais em Boas Práticas nas Contratações', 'exibir', '5','8', '19', 'aula5pagina7.php', 'aula5pagina9.php', '<h4 style="font-weight:bold">Boas Práticas</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">3.  REFERENCIAIS EM BOAS PRÁTICAS NAS CONTRATAÇÕES</h3>
                <p>Quando falamos em boas práticas a serem adotadas nas contratações públicas, vários referenciais podem ser destacados. Como exemplo, temos os guias do <a href="http://portal3.tcu.gov.br/portal/page/portal/TCU/publicacoes_institucionais" target="_blank" title="link para o TCU">TCU</a> e da <a href="http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/guia-de-boas-praticas-em-contratacao-de-solucoes-de-ti" target="_blank" title="link para SLTI do MPOG">Secretaria de Logística e Tecnologia da Informação - SLTI, do Ministério do Planejamento, Orçamento e Gestão</a>, além do <a href="http://www.stf.jus.br/arquivo/cms/intranetNavegacao/anexo/Manuais_documentos/Manual_para_gestores_de_Contratos__Versao_1.0__17092011.pdf" target="_blank" title="link para o manual do gestor do STF">Manual do gestor de contratos do STF</a>.  </p>
                <h4 class="subTitulo">3.1.  GUIA DE BOAS PRÁTICAS EM CONTRATAÇÃO DE SOLUÇÕES DE TECNOLOGIA DA INFORMAÇÃO DO TCU</h4>
                <p>O Guia de boas práticas em contratação de soluções de Tecnologia da Informação: riscos e controles para o planejamento da contratação, elaborado pelo TCU (2012, p. 7), objetiva contribuir para que órgãos e entidades da Administração Pública Federal planejem as contratações de bens e serviços de TI, evitando problemas já conhecidos, utilizando as práticas constantes do manual “para alavancar suas operações e entregar os resultados almejados pela sociedade, que cobra cada vez mais efetividade, eficácia, eficiência, transparência e lisura dos entes públicos”.</p>
                <p>O Guia foi elaborado sob o ponto de vista do controle externo da Administração Pública Federal - APF, com base na legislação, na jurisprudência e nas melhores práticas do mercado. Destaque para as sugestões de controles internos no tratamento dos riscos relativos ao processo de contratação de soluções de TI. Conforme podemos verificar, esse Guia foi citado amplamente ao longo deste treinamento, dada a sua importância para as contratações em geral, especialmente as ligadas à área de informática.</p>
                <p>Trata-se de excelente apoio referencial com 527 páginas, que abordam diversos pontos relevantes às contratações de TI, mas que podem ser utilizados em outras áreas, com as adaptações necessárias. Nesta ocasião, não objetivamos esgotar o tema, e sim apresentar os pontos que consideramos merecedores de destaque.</p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Clique aqui para saber alguns pontos que fazem parte desse Guia.
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Importância do <span class="semi-bold">planejamento</span> das contratações de soluções de TI.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Contexto do planejamento</span> das contratações de soluções de TI.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Processo de planejamento</span> da contratação de soluções de TI.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Artefatos</span> gerados no processo de planejamento da contratação de soluções de TI.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Riscos e sugestões de controles internos</span> relativos ao processo de planejamento das contratações como um todo.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Controles internos</span> de caráter estruturante.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Principais <span class="semi-bold">falhas</span> encontradas pelo TCU.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Aplicação do guia a diferentes <span class="semi-bold">níveis de complexidade</span> de contratação.</p>
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Gestão do conhecimento</span> sobre o processo de contratação de soluções de TI.</p>
                    </div>
                  </div>
                  </div>
                </div>
                <p>Conforme podemos observar, os tópicos 1, 2 e 3 do Guia destacam a relevância do planejamento, dada a importância e o impacto dessa questão no resultados das contratações públicas. </p>
                <p>O Guia aponta o que a legislação, a jurisprudência e as melhores práticas sinalizam sobre o planejamento das contratações de TI. Indica também diversos riscos relativos ao processo de planejamento de contratações dessa natureza, além de sugerir providências, por meio de controles internos, para mitigá-los.</p>
                <p>Tendo em vista sua abrangência e nível de detalhamento, apesar de ser um material inicialmente voltado para a área de TI, o Guia de boas práticas em contratação de soluções de Tecnologia da Informação do TCU constitui excelente ferramenta a ser utilizada e consultada por gestores, independentemente da área de atuação.</p>
                <p>Entre as boas práticas sugeridas pelo Guia, podemos destacar como referenciais para as contratações:</p>
                <p><span class="semi-bold">Formalização do processo da contratação</span> - o planejamento da contratação deve seguir essencialmente os mesmos passos, de maneira a demonstrar previsibilidade com relação à execução e garantir a aderência à legislação e à jurisprudência. Trata-se de padronizar o processo da contratação de modo a formalizar, divulgar e capacitar os servidores envolvidos. A alta administração deve aprovar e publicar esse processo de trabalho para conhecimento de todos (Guia de boas práticas em contratação de soluções de Tecnologia da Informação, TCU, 2012, p. 51).</p>
                <p><span class="semi-bold">Equipe de planejamento</span> – as áreas envolvidas devem designar servidores para compor uma equipe encarregada de elaborar o planejamento de uma contratação. De acordo com a <a href="http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/instrucao-normativa-mp-slti-no04" target="_blank" title="link para IN SLTI 04">IN - SLTI 4/2014</a>, essa equipe deve ser formada por um servidor da área requisitante da solução, um servidor da área de TI e um servidor da área administrativa<a href="javascript:void(0);" rel="popover" data-content="<p>IN 4/2010. Art. 2º Para fins desta Instrução Normativa, considera-se: (...) III - Equipe de Planejamento da Contratação: equipe envolvida no planejamento da contratação, composta por: a) Integrante Técnico: servidor representante da Área de Tecnologia da Informação, indicado pela autoridade competente dessa área; b) Integrante Administrativo: servidor representante da Área Administrativa, indicado pela autoridade competente dessa área; c) Integrante Requisitante: servidor representante da Área Requisitante da Solução, indicado pela autoridade competente dessa área.</p>" data-toggle="popover" data-size="popover-small"><sup>7</sup></a>. </p>
                <p>Essa equipe multidisciplinar ajudará, p. ex., no planejamento de questões específicas atinentes às especificidades da demanda (requisitante), à compatibilidade com os sistemas do órgão (TI) e às particularidades contratuais e de execução (administrativa).</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula5pagina7.php', 'aula5pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


