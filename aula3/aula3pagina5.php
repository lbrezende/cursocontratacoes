<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Conceito', 'exibir', '3','5', '13', 'aula3pagina4.php', 'aula3pagina6.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			<div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
	    			<p>No que se refere aos objetivos do nosso curso, trataremos da governança, de modo geral (englobando a governança corporativa e a governança global), e particularmente da governança pública.</p>
					<p>Avançando um pouco nos conceitos, de acordo com Kaufmann, Kraay e Zoido-Lobatón (1999), governança pública refere-se às:</p>
					<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
					<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
						<p class="fonteMenor">Tradições e instituições nas quais a autoridade de um país é exercida, o que inclui o processo pelo qual os governos são selecionados, monitorados e substituídos, a capacidade efetiva do governo em formular e implementar políticas sólidas e o respeito dos cidadãos e do Estado para com as instituições que governam as interações sociais e econômicas entre eles.</p>
					</div>
					<div class="clear"></div>
						<p>Para conhecer um pouco mais sobre o conceito de Governança Corporativa, acesse o vídeo a seguir da entrevista do advogado Marcelo Gasparino da Silva ao programa da Rede Record, Economia News, exibida em 31/5/2010, em que ele trata da importância de se planejar o futuro das empresas.</p>						
					<p>Governança é, portanto, a adoção de ações que ajudam a organização a alcançar seus objetivos de modo mais eficaz. Ela auxilia no gerenciamento de riscos para alcançar resultados desejados (JEZINI, 2014).</p>
					<p>Na governança, dois tipos elementares de atores estão envolvidos: principal e agente, conforme será visto a seguir.</p>
					<p>O primeiro ator, denominado <span class="semi-bold">principal</span>, é o “dono do negócio”, maior interessado na longevidade, na prosperidade da organização e nos melhores resultados. A ele devem ser assegurados o comando e o controle. Como exemplo desse grupo, temos os donos, acionistas, sociedade, etc.</p>
					<p>O segundo ator é o <span class="semi-bold">agente</span>, que é aquele que recebe a delegação de autoridade para administrar os ativos e os recursos, ou seja, dirigentes, gerentes, colaboradores, entre outros. Cabe a eles executar com dedicação e lealdade as metas e rumos apontados pelo principal.</p>
					<p>Na área da governança, é comum ouvirmos falar em conflito de agência, que é, resumidamente, o conflito de interesses que pode ocorrer entre principal e agente, em decorrência de uma quebra de confiança. Esse tipo de conflito pode acontecer em qualquer instituição. </p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 textAlignRight">
				<div class="video-container">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/08Vda2ncvIo" frameborder="0" allowfullscreen></iframe>
				</div><br><br>
				</div>
				<div class="clear"></div>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina4.php', 'aula3pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


