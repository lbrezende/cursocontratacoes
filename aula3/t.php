<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Governança', 'exibir', '3','2', '13', 'index.php', 'aula3pagina3.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 
<link rel="stylesheet" type="text/css" href="../include/css/stylesheet2.css" />
 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			       <p>Governança</p>

              <div class="responsive-timeline-color">

              <div class="titulotimeline">Evolução do termo Governança</div>

                    <div class="responsive-timeline">

                        <div class="container">
                            <div class="timeline_container">
                                <div class="timeline">
                                    <div class="plus"></div>
                                </div>
                            </div>
                            
                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                    </div>
                                </div>
                                <img class="cover" src="images/cover2.jpg" />
                                <div class="content">
                                  <h1>1932</h1>
                                  <p>Berle e Means (1932 apud TCU), desenvolvem um dos primeiros estudos acadêmicos tratando de assuntos correlatos à governança. É papel do Estado regular as organizações privadas.</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">1932 </span>
                                </footer>                                                                 
                            </div>

                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                    </div>
                                </div>
                                <img class="cover" src="images/cover3.jpg" />
                                <div class="content">
                                    <h1>1934</h1>
                                    <p>Em 1934, foi criada, nos Estados Unidos, a US Securities and Exchange Comission, organização americana responsável por proteger investidores; garantir a justiça, a ordem e a eficiência dos mercados e facilitar a formação de capital até os dias atuais.</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">1934 </span>
                                </footer>                                                                  
                            </div>

                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                    </div>
                                </div>
                                <img class="cover" src="images/cover4.jpg" />
                                <div class="content">
                                  <h1>1990</h1>
                                  <p>No começo da década de 90, diante das crises financeiras, o Banco da Inglaterra criou o Comitê de Governança Corporativa , que elaborou o Código das melhores práticas de Governança Corporativa, trabalho que resultou no Cadbury Report  (Referencial básico de Governança do TCU, 2014, p.15).</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">1990 </span>
                                </footer>                                  
                            </div>

                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                    </div>
                                </div>
                                <img class="cover" src="images/cover5.jpg" />
                                <div class="content">
                                  <h1>1992</h1>
                                  <p>Em 1992, o Committee of Sponsoring Organizations of the Treadway Commission – COSO  (Comitê das Organizações Patrocinadoras da Comissão Treadway) publicou o Internal Control - Integrated Framework (Controle Interno – Estrutura Integrada).</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">1992 </span>
                                </footer>                                  
                            </div>
                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                 </div>
                                </div>
                                <img class="cover" src="images/cover6.jpg" />
                                <div class="content">
                                  <h1>2002</h1>
                                  <p>Em 2002, em decorrência de escândalos envolvendo demonstrações contábeis fraudulentas ratificadas por empresas de auditorias, publicou-se, nos Estados Unidos, a Lei Sarbanes-Oxley . O objetivo era melhorar os controles para garantir a fidedignidade das informações constantes dos relatórios financeiros.</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">2002 </span>
                                </footer>                                  
                            </div>
                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                    </div>
                                </div>
                                <img class="cover" src="images/cover7.jpg" />
                                <div class="content">
                                  <h1>2004</h1>
                                  <p>Em 2004, o COSO publicou o Enterprise Risk Management - Integrated Framework (Gerenciamento de riscos corporativos – Estrutura integrada). Ainda hoje é tido como referência no tema gestão de riscos.</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">2004 </span>
                                </footer>                                  
                            </div>

                            <div class="item">
                                <div class="tags">
                                    <span class="icon">C</span>
                                    <div class="tags-container">
                                      <span class="tag">Battle</span>
                                      <span class="tag">Emperor</span>
                                    </div>
                                </div>
                                <img class="cover" src="images/cover8.jpg" />
                                <div class="content">
                                  <h1>2015</h1>
                                  <p>Contemporaneamente, o G8 (grupo dos oito países mais desenvolvidos do mundo) e organizações como o Banco Mundial, o Fundo Monetário Internacional – FMI – e a Organização para Cooperação e Desenvolvimento Econômico – OCDE – trabalham para difundir a governança.</p>
                                </div>
                                <footer>
                                    <span class='corner'></span>
                                    <span class="date">2015 </span>
                                </footer>                                  
                            </div>                            
                        </div>
                    </div>
                </div>


        
              </div>
            </div>   
           </div>
        </div>
    </article>    




<?php  configNavegacaoRodape('exibir', 'index.php', 'aula3pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


