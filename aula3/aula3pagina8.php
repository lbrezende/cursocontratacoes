<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Princípios básicos', 'exibir', '3','8', '13', 'aula3pagina7.php', 'aula3pagina9.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
      			<h3 class="titulo">2.2. PRINCÍPIOS BÁSICOS</h3>
				<p>Na busca por metodologias que agregassem elementos para tornar o Estado mais eficiente, várias instituições promoveram estudos sobre a boa governança. Os diversos estudos destacaram uma variedade de princípios a serem observados na esfera pública.</p>
				<p>Entre as entidades internacionais que avaliaram as condições necessárias à melhoria da governança nas organizações públicas estão, por exemplo, o <a href="http://www.ifac.org/" target="_blank" title="link para o International Federation of Accountants – IFAC">International Federation of Accountants – IFAC</a>, o <a href="http://www.anao.gov.au/ " target="_blank" title="link para o Australian National Audit Office – ANAO">Australian National Audit Office – ANAO</a>, o <a href="http://www.cipfa.org/" target="_blank" title="link para o Chartered Institute of Public Finance and Accountancy – CIPFA">Chartered Institute of Public Finance and Accountancy – CIPFA</a>, o <a href="http://www.opm.co.uk/" target="_blank" title="link para Office for Public Management Ltd – OPM">Office for Public Management Ltd – OPM</a>, além do <a href="http://www.coe.int/t/dghl/standardsetting/media/doc/Good_Gov_StandardPS_en.pdf" target="_blank" title="link para Independent Commission for Good Governance in Public Services – ICGGPS">Independent Commission for Good Governance in Public Services – ICGGPS</a>; do <a href="http://www.worldbank.org/pt/country/brazil" target="_blank" title="link para o Banco Mundial">Banco Mundial</a> e do <a href="https://www.iia.org.uk/" target="_blank" title="link para o Institute of Internal Auditors – IIA">Institute of Internal Auditors – IIA</a> (Referencial básico de Governança do TCU, com adaptações).</p>
				<p>O Referencial relaciona a <span class="semi-bold">legitimidade</span>, a <span class="semi-bold">equidade</span>, a <span class="semi-bold">responsabilidade</span>, a <span class="semi-bold">eficiência</span>, a <span class="semi-bold">probidade</span>, a <span class="semi-bold">transparência</span> e a <span class="semi-bold">accountability</span>, sugeridos pelo Banco Mundial, como os princípios da boa governança, conforme a seguir. Clique em cada um dos princípios da boa governança para visualizar os seus conceitos:</p>
				<p class="textAlignCenter "><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>Refere-se ao zelo pela sustentabilidade das organizações, visando à sua longevidade, incorporando considerações de ordem social e ambiental na definição dos negócios e operações (GesPública, 2010).</p>" data-toggle="popover" data-size="popover-small">Responsabilidade</a></p>
				<p class="textAlignCenter "><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>Busca garantir as condições para que todos tenham acesso ao exercício de seus direitos civis (liberdade de expressão, de acesso à informação, de associação, de voto, igualdade entre gêneros, etc.), políticos e sociais (saúde, educação, moradia, segurança, etc.) – Manual de auditoria operacional do TCU, 2010.</p>" data-toggle="popover" data-size="popover-small">Equidade</a><span style="padding:40px;">&nbsp;</span><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>Consiste no dever dos servidores públicos de demonstrarem probidade, zelo, economia e observância às regras e aos procedimentos do órgão ao utilizar, arrecadar, gerenciar e administrar bens e valores públicos. Os servidores devem demonstrar que são dignos de confiança (IFAC, 2001).</p>" data-toggle="popover" data-size="popover-small">Probidade</a></p>
				<p class="textAlignCenter "><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>Fazer o que é preciso ser feito com qualidade adequada ao menor custo possível. Não é a redução de custo de qualquer maneira, mas a busca da melhor relação entre qualidade do serviço e qualidade do gasto Manual de auditoria operacional do TCU, 2010.</p>" data-toggle="popover" data-size="popover-small">Eficiência</a><span style="padding:60px;">&nbsp;</span><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>Princípio jurídico fundamental do Estado Democrático de Direito. Amplia o controle externo da Administração Pública para além da aplicação isolada do critério da legalidade, buscando verificar não somente se a lei foi cumprida, mas também se o interesse público, o bem comum, foi alcançado. Observa que nem sempre o que é legal poderá ser legítimo (Glossário de termos de controle do TCU, 2012).</p>" data-toggle="popover" data-size="popover-small">Legitimidade</a></p>
				<p class="textAlignCenter "><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>É a possibilidade de acesso a todas as informações relativas à organização pública, sendo um dos requisitos de controle do Estado pela sociedade civil. A adequada transparência resulta em um clima de confiança, tanto internamente quanto nas relações de órgãos e entidades com terceiros.</p>" data-toggle="popover" data-size="popover-small">Transparência</a><span style="padding:20px;">&nbsp;</span><a href="javascript:void(0);" class="btn btn-conceito btn-big subTitulo" rel="popover" data-content="<p>Os agentes de governança devem prestar contas de sua atuação de forma voluntária, assumindo integralmente as consequências de seus atos e omissões (IBGC, 2009). De acordo com as normas de auditoria da INTOSAI, accountability é a obrigação que têm as pessoas ou entidades às quais se tenham confiado recursos, incluídas as empresas e as organizações públicas, de assumir as responsabilidades de ordem fiscal, gerencial e programática que lhes foram conferidas, e de informar a quem lhes delegou essas responsabilidades (Plano estratégico do TCU 2011 – 2015).</p>" data-toggle="popover" data-size="popover-small">Accountability</a></p>
    			<h4 class="subTitulo">2.3.  DIRETRIZES</h4>
				<p>Trilhando os caminhos de difusão das boas práticas de governança, o Referencial básico do TCU, 2013 apresenta algumas diretrizes que favorecem o alcance da boa governança. As diretrizes foram relacionadas com base em estudo da CIPFA - <i>Chartered Institute of Public Finance and Accountancy, 2004</i>.</p>

				
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
					  <h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
						  Clique aqui para visualizar as diretrizes
						</a>
					  </h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					  <div class="panel-body">
						<p><br />
							a) focar o propósito da organização em resultados para cidadãos e usuários dos serviços;<br />
							b) realizar, efetivamente, as funções e os papéis definidos;<br />
							c) tomar decisões embasadas em informações de qualidade;<br />
							d) gerenciar riscos;<br />
							e) desenvolver a capacidade e a eficácia do corpo diretivo das organizações;<br />
							f) prestar contas e envolver efetivamente as partes interessadas;<br />
							g) ter clareza acerca do propósito da organização, bem como dos resultados esperados para cidadãos e usuários dos serviços;<br />
							h) certificar-se de que os usuários recebam um serviço de alta qualidade;<br />
							i) certificar-se de que os contribuintes recebam algo de valor em troca dos aportes financeiros providos;<br />
							j) definir claramente as funções das organizações e as responsabilidades da alta administração e dos gestores, certificando-se de seu cumprimento;<br />
							l) ser claro sobre as relações entre os membros da alta administração e a sociedade;<br />
							m) ser rigoroso e transparente sobre a forma como as decisões são tomadas;<br />
							n) ter e usar estruturas de aconselhamento, apoio e informação de boa qualidade;<br />
							o) certificar-se de que um sistema eficaz de gestão de risco esteja em operação;<br />
							p) certificar-se de que os agentes (comissionados ou eleitos) tenham as habilidades, o conhecimento e a experiência necessários para um bom desempenho;<br />
							q) desenvolver a capacidade de pessoas com responsabilidades de governo e avaliar o seu desempenho, como indivíduos e como grupo;<br />
							r) equilibrar, na composição do corpo diretivo, continuidade e renovação;<br />
							s) compreender as relações formais e informais de prestação de contas;<br />
							t) tomar ações ativas e planejadas para dialogar com e prestar contas à sociedade, bem como engajar, efetivamente, organizações parceiras e partes interessadas;<br />
							u) tomar ações ativas e planejadas de responsabilização dos agentes;<br />
							v) garantir que a alta administração se comporte de maneira exemplar, promovendo, sustentando e garantindo a efetividade da governança; e<br />
							x) colocar em prática os <a href="http://intranet/hotsite/Site_cidadania_corporativa_2015/site/index.html" target="_blank" title="">valores organizacionais.</a>
						</p>
					  </div>
					</div>
				  </div>
				</div>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina7.php', 'aula3pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


