<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Considerações finais', 'exibir', '3','12', '13', 'aula3pagina11.php', 'aula3pagina13.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			     <h3 class="titulo">RESUMO DA AULA</h3>
               <p>Nesta aula, verificamos que a governança não é modismo.  Trata-se de um sistema/processo que vem evoluindo há muitos anos em todo o mundo e no Brasil. Não existe um conceito único para o termo governança. Em regra, os princípios da boa governança são aplicáveis a qualquer tipo de organização, independente do porte, natureza jurídica ou tipo de controle. De modo particular, conhecemos a governança pública, excelente ferramenta na gestão dos gastos públicos, seu conceito, princípios e diretrizes. </p>
                <p>Tendo em vista a complexidade e a criticidade da Tecnologia da Informação para a Administração Pública Federal, verificamos que o TCU tem dedicado atenção especial às ações de governança aplicáveis a essa área.</p>
                <p>Na próxima aula, serão apresentadas noções sobre os riscos corporativos.</p>
                <p>Até lá... </p>
                <p>Abraços e bom estudo.</p>
                <h3 class="titulo">MENSAGEM</h3>
                <p class="textAlignCenter"><img src="../include/img/aulas/Mensagem_Aula_3.jpg" style="" alt="Palavra do Autor" /></p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina11.php', 'aula3pagina13.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


