<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Conceito', 'exibir', '3','4', '13', 'aula3pagina3.php', 'aula3pagina5.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			<h3 class="titulo">1.2. CONCEITO </h3>
				<p>Não existe um conceito único para o termo governança. De acordo com a área de aplicação, pública ou privada, a definição pode variar um pouco, sem, contudo, perder seu objetivo principal, que é destacar os meios para dar maior eficiência, transparência na prestação de contas, ética e responsabilidade para as corporações, Estados, etc.</p>
				<p>As ações de governança procuram destacar a atuação dos diversos agentes envolvidos, tipicamente os acionistas, a alta administração, o conselho de administração, além de outros participantes, tais como funcionários, fornecedores, clientes, bancos e outros credores, instituições reguladoras (p. ex., a Comissão de Valores Mobiliários - CVM, o Banco Central, etc.) e a comunidade em geral.</p>
				<p>Tendo em vista a amplitude do termo governança e seus diversos significados, que podem variar de acordo com a perspectiva de análise, o Tribunal de Contas da União (Referencial básico de Governança, 2014) procurou conceituar as definições mais conhecidas, a saber, governança corporativa, governança pública e governança global, conforme abaixo:</p>
				<div class="">
					<div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="listaSaibaMais abreMais" id="1" title="clique aqui para saber mais"><img alt="" src="../include/img/aulas/govCorporativa.png" /></a></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-1" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="caixa-1">
						<p>Pode ser entendida como o sistema pelo qual as organizações são dirigidas e controladas (CADBURY, 1992; ABNT NBR ISO/IEC 38500, 2009). Refere-se ao conjunto de mecanismos de convergência de interesses de atores direta e indiretamente impactados pelas atividades das organizações (SHLEIFER; VISHNY, 1997), mecanismos esses que protegem os investidores externos da expropriação pelos internos (gestores e acionistas controladores) (LA PORTA et al., 2000).</p>
					</div>
				</div>
				<div class="clear paddingBottom20"></div>
				<div class="espacamentoBottom">
					<div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="listaSaibaMais abreMais" id="2" title="clique aqui para saber mais"><img alt="" src="../include/img/aulas/govPublica.png" /></a></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-2" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="caixa-2">
						<p>Pode ser entendida como o sistema que determina o equilíbrio de poder entre os envolvidos — cidadãos, representantes eleitos (goverTnantes), alta administração, gestores e colaboradores — com vistas a permitir que o bem comum prevaleça sobre os interesses de pessoas ou grupos (MATIAS-PEREIRA, 2010, adaptado).</p>
					</div>
				</div>
				<div class="clear"></div>
				<div class="espacamentoBottom">
					<div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="listaSaibaMais abreMais" id="3" title="clique aqui para saber mais"><img alt="" src="../include/img/aulas/govGlobal.png" /></a></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-3" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="caixa-3">
						<p>  Que pode ser entendida como o conjunto de instituições, mecanismos, relacionamentos e processos, formais e informais, entre Estado, mercado, cidadãos e organizações, internas ou externas ao setor público, através dos quais os interesses coletivos são articulados, direitos e deveres são estabelecidos e diferenças são mediadas (WEISS; THAKUR, 2010).</p>
					</div>
				</div>
				<div class="clear"></div>

				
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina3.php', 'aula3pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


