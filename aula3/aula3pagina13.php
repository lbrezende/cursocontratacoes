<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referências Bibliográficas', 'exibir', '3','13', '13', 'aula3pagina12.php', 'aula3pagina13.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">SUGESTÕES DE LEITURA COMPLEMENTAR</h3>
                <p>Livro: PEREIRA, José Matias. <i>Governança no setor público</i>. 1. ed. São Paulo:  Atlas, 2010. </p>
                <p><i>Acórdão</i> 3.117/2014-TCU-Plenário.</p>
                <p>Artigo: GASTIM, Ian Chicharo; OLIVEIRA, Malena. <i>Falta de governança no setor público prejudica qualidade do serviço oferecido à população</i> – O Estado de São Paulo, Novembro de 2014. Arquivo disponível em: <a href="http://economia.estadao.com.br/noticias/governanca,falta-de-governanca-no-setor-publico-prejudica-qualidade-do-servico-oferecido-a-populacao,1597473" target="_blank" title="acesso ao artigo">http://economia.estadao.com.br/noticias/governanca,falta-de-governanca-no-setor-publico-prejudica-qualidade-do-servico-oferecido-a-populacao,1597473</a>. Acesso em 28/07/2015.</p>
                <h3 class="titulo">REFERÊNCIAS BIBLIOGRÁFICAS</h3>
                <p>BERLE, A.; MEANS, G. <i>The modern corporation and private property</i>. New York: Macmillan, 1932 apud Tribunal de Contas da União. Governança Pública: referencial básico de governança.</p>
                <p>BRASIL. Ministério do Planejamento, Orçamento e Gestão. Secretaria de Gestão. Programa Nacional de Gestão Pública e Desburocratização – GesPública, 2010. Disponivel em:<a href="http://www.gespublica.gov.br/projetos-acoes/pasta.2010-04-26.8934490474/Instrumento_ciclo_2010_22mar.pdf" target="_blank" title="acesso ao gespublica">http://www.gespublica.gov.br/projetos-acoes/pasta.2010-04-26.8934490474/Instrumento_ciclo_2010_22mar.pdf</a>. </p>
                <p>BRASIL. Ministério do Planejamento, Orçamento e Gestão. Secretaria de Gestão. Prêmio Nacional da Gestão Pública – PQGF. Documento de Referência.</p>
                <p>BRASIL. Ministério do Planejamento, Orçamento e Gestão, Subsecretaria de Planejamento, Orçamento e Gestão. Fórum Nacional 2008/2009. Brasília: MP, SEGES, 2009.</p>
                <p>BRASIL. Tribunal de Contas da União - TCU. Glossário de termos do controle, 2012. Disponivel em:<a href="http://portal.tcu.gov.br/cidadao/cidadao.htm" target="_blank" title="">http://portal2.tcu.gov.br/portal/page/portal/TCU/co-munidades/fiscalizacao_controle/normas_auditoria/Glossario_termos_ce.pdf</a>.</p>
                <p>Brasil. Tribunal de Contas da União. <i>Governança Pública: referencial básico de governança aplicável a órgãos e entidades da administração pública e ações indutoras de melhoria</i>. Brasília: TCU, Secretaria de Planejamento, Governança e Gestão, 2014.</p>
                <p>BRASIL. Tribunal de Contas da União. Plano estratégico TCU 2011-2015, BRASÍLIA, 2011. Disponível em:<a href="http://portal.tcu.gov.br/cidadao/cidadao.htm " target="_blank" title=""> http://portal2.tcu.gov.br/portal/page/por-tal/TCU/planejamento_gestao/planejamento2011/index.html</a>.</p>
                <p>BRASIL. Tribunal de Contas da União. Secretaria de Fiscalização e Avaliação de Programas de Governo. Portaria-Segecex 4, de 26 de fevereiro de 2010: Manual de auditoria operacional, 2010c. Disponível em: <a href="http://portal2.tcu.gov.br/portal/pls/portal/docs/2058980.PDF" target="_blank" title="">http://portal2.tcu.gov.br/portal/pls/portal/docs/2058980.PDF</a>.</p>
                <p>CIPFA. Chartered Institute of Public Finance and Accountancy. The good governance standard for public services, 2004. Disponível em: <a href="http://www.jrf.org.uk/system/files/1898531862.pdf" target="_blank" title="">http://www.jrf.org.uk/system/files/1898531862.pdf</a>.</p>
                <p>Constituição Federal Brasileira. </p>
                <p>DALLARI, D. D. A. <i>Elementos de teoria geral do Estado</i>. São Paulo: Saraiva, 2005.</p>
                <p>Fórum: Governança e Gestão no Setor Público - 26 e 27 de Novembro de 2014. Palestrantes diversos. Conexxões Educação - Brasília DF.</p>
                <p>Instituto Brasileiro de Governança Corporativa. <i>Código das melhores práticas de Governança Corporativa</i>. 4.ed. São Paulo, SP : IBGC, 2009. 73 p. Disponivel <a href="../include/file/Codigo_das_melhores_praticas_de_Governanca_Corporativa.pdf" target="_blank" title="">clicando aqui</a>.</p>
                <p>IFAC. International Federation of Accountants. <i>Governance in the public sector: a governing body perspective. In International public sector study nº 13</i>., 2001. Disponível em: <a href="http://www.ifac.org/sites/default/files/publications/files/study-13-governance-in-th.pdf" target="_blank" title="">http://www.ifac.org/sites/default/files/publications/files/study-13-governance-in-th.pdf</a>.</p>
                <p>KAUFMANN, Daniel; KRAAY, Aart; ZOIDO-LOBATÓN, Pablo. <i>Governance matters. World Bank Policy Research Working Paper 2196, World Bank</i>, 1999.Disponível em: <a href="http://www.worldbank.org/wbi/governance/pdf/govmatrs.pdf" target="_blank" title="">http://www.worldbank.org/wbi/governance/pdf/govmatrs.pdf</a>. Acesso em: 12 Nov. 2014.</p>
                <p>Levantamento de Governança de TI 2014 – Glossário. Disponível em: <a href="http://portal3.tcu.gov.br/portal/page/portal/TCU/comunidades/tecnologia_informacao/pesquisas_governanca/Perfil%20GovTI2014%20-%20Glossário%20v1.pdf" target="_blank" title="">http://portal3.tcu.gov.br/portal/page/portal/TCU/comunidades/tecnologia_informacao/pesquisas_governanca/Perfil%20GovTI2014%20-%20Glossário%20v1.pdf</a>. Acesso em 9/01/2014.</p>
                <p>Supremo Tribunal Federal <a href="http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=280058" target="_blank" title="">http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=280058</a>. Quarta-feira, 19 de novembro de 2014.</p>
                <p>WORLD BANK. <i>The International Bank for Reconstruction and Development. Worldwide Governance Indicators (WGI)</i>, 2013. Disponível em: <a href="http://info.worldbank.org/governance/wgi/index.aspx#faq" target="_blank" title="">http://info.worldbank.org/governance/wgi/index.aspx#faq</a>. Acesso em: 12 Nov. 2014.</p>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina12.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


