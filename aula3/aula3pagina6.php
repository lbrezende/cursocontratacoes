<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Aplicabilidade, governança e gestão', 'exibir', '3','6', '13', 'aula3pagina5.php', 'aula3pagina7.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			<p>Em regra, os princípios e práticas da boa governança corporativa são aplicáveis a qualquer tipo de organização, independente do porte, natureza jurídica ou tipo de controle.</p>
				<p>A governança busca otimizar ações que melhorem os relacionamentos entre proprietários, sociedade, conselho de administração, diretoria, gestores e órgãos de controle, convertendo princípios teóricos em recomendações objetivas e práticas que busquem preservar e agregar valor à instituição, tornando sua operação mais efetiva e transparente.</p>
				<p>Assim, a governança tem por objetivos, entre outros:</p>
				<div>
					<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
					<div id="caixa-0">
						<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> melhor atender aos interesses da sociedade; </p>
						<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
						<div id="caixa-1">
							<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> garantir o comportamento ético, íntegro, responsável, comprometido e transparente da liderança; </p>
							<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-2">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> controlar a corrupção; implementar efetivamente um código de conduta e de valores éticos; </p>
								<p class="abreCaixa" id="3"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
								<div id="caixa-3">
									<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> observar e garantir a aderência das organizações às regulamentações, códigos, normas e padrões; </p>
									<p class="abreCaixa" id="4"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
									<div id="caixa-4">
										<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> garantir a transparência e a efetividade das comunicações; </p>
										<p class="abreCaixa" id="5"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
										<div id="caixa-5">
											<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> balancear interesses e envolver efetivamente os <i>stakeholders</i> (cidadãos, usuários de serviços, acionistas, iniciativa privada).</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="paddingBottom20"></div>
				<h4 class="subTitulo">1.4. GOVERNANÇA E GESTÃO</h4>
				<P>Embora possuam estrita relação, os termos governança e gestão não são sinônimos e nem se confundem.</p>
				<p>A governança é mais ampla e está associada às seguintes funções básicas:</p>
				<p><span class="semi-bold">avaliação</span> de desempenho e resultados; </p>
				<p><span class="semi-bold">direcionamento</span> e coordenação de políticas e planos; </p>
				<p><span class="semi-bold">monitoramento</span> de resultados, desempenhos e cumprimento de políticas. </p>
				<p>À governança interessa verificar a qualidade do processo decisório, sua efetividade avaliando se as decisões foram tomadas de modo a agregar o maior valor possível para o negócio, tendo em vista os resultados definidos (Referencial básico de Governança do TCU, 2014).</p>
				<p>Já a gestão está adstrita aos processos organizacionais. Compreende ações de planejamento, execução, controle e ações diretas para a consecução de objetivos.</p>
				<p class="textAlignCenter"><img src="../include/img/aulas/imagem_aula3_tela6.png" alt="2 ciclos. Governança (avaliar, direcionar e monitorar) x gestão (planejar, executar, controlar e agir), interligados pela estratégia e accountability" /></p>
				<p class="textAlignCenter fonteMenor">Figura 1: Relação entre governança e gestão (TCU, 2014, p.48).</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina5.php', 'aula3pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


