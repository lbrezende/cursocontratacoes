<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('O TCU e a Governança Pública', 'exibir', '3','9', '13', 'aula3pagina8.php', 'aula3pagina10.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				<h3 class="titulo">3. O TRIBUNAL DE CONTAS DA UNIÃO E A GOVERNANÇA PÚBLICA</h3>
				<p>Conforme foi possível verificar até o momento, a governança constitui excelente ferramenta na gestão dos gastos públicos. Contemporaneamente, há, em todo o mundo, uma crescente preocupação em relação à governança pública. </p>
				<p>Em 2004, o TCU realizou quatro levantamentos de auditoria operacional sobre gestão de ética na Administração Pública, que resultaram nos acórdãos 517 (Funasa), 684 (Petrobrás), 1030 (CVM) e 1331 (BNDES), todos exarados pelo Plenário do TCU em 2005 (Referencial básico de Governança do TCU, 2014). </p>
				<p>O objetivo desses levantamentos foi verificar a conveniência e a oportunidade da ampliação de fiscalizações dessa natureza no combate à fraude e à corrupção. O tema governança constou dos quatro relatórios, segundo excerto abaixo:</p>
				<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
				<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
					<p class="fonteMenor">As mudanças estruturais ocorridas na Administração Pública Federal, principalmente a partir da década de 1990, e o cenário econômico instalado desde então, demandam a promoção da ética, da transparência e da boa governança como quesitos fundamentais para a garantia de confiabilidade das instituições públicas e a consequente inclusão do Brasil na nova ordem socioeconômica mundial.</p>
				</div>
				<div class="clear"></div>
				<p>Tendo em vista o resultado de trabalhos, conforme os citados acima, o TCU observou que o tema governança deveria receber atenção especial. Os trabalhos despertaram a consciência da necessidade urgente de aprimoramentos na governança da Administração Pública Federal.</p>
				<p>Nessa linha, o TCU vem desenvolvendo trabalhos com o objetivo de melhor conhecer a situação da governança no setor público federal, bem como difundir as boas práticas de governança para o setor.</p>
				<p>A seguir, apresentamos um gráfico que evidencia o crescimento dos acórdãos produzidos pelo TCU que apresentam o termo “governança” ao longo dos anos:</p>
				<p class="textAlignCenter"><img src="../include/img/aulas/imagem_aula3_tela9.png" alt="" /></p>
				<p class="textAlignCenter fonteMenor">Figura 3: Acórdãos do TCU que usam o termo “governança” (Referencial básico de Governança do TCU, 2014).</p>
				<p>Em encontro recente, o Presidente do STF, ministro Ricardo Lewandowski, e o Presidente do TCU, ministro Augusto Nardes, acordaram que o Supremo Tribunal Federal irá difundir dentro do Poder Judiciário propostas para a melhoria da governança pública, em ação integrada entre os dois órgãos (<a href="http://www.stf.jus.br/portal/cms/verNoticiaDetalhe.asp?idConteudo=280058" target="_blank" title="link para o portal do stf">Portal do STF</a>). De acordo com os ministros, o Poder Judiciário só tem a ganhar com o movimento de integração entre o STF e o TCU visando à difusão e adoção de boas práticas de governança.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina8.php', 'aula3pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


