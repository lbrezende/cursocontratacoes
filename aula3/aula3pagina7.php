<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Governança no setor público', 'exibir', '3','7', '13', 'aula3pagina6.php', 'aula3pagina8.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			<h3 class="titulo">GOVERNANÇA NO SETOR PÚBLICO</h3>
				<p>A aplicação dos princípios de governança no setor público vem se firmando como uma tendência há alguns anos. As crises fiscais que nos últimos anos atormentaram diversos países pelo mundo afora possibilitaram o incremento da discussão acerca da aplicabilidade das boas práticas de governança no setor público.</p>
				<p>O objetivo é tornar o Estado e o setor público mais eficientes por meio do estabelecimento de princípios básicos que norteiam as boas práticas de governança nas organizações públicas (Referencial básico de Governança do TCU, 2014). Os princípios mais relevantes para o nosso estudo serão abordados adiante, em tópico específico desta aula.</p>
				<p>Além de tornar o setor público mais eficiente, a aplicação da governança busca tornar a atuação dos agentes mais responsável, comprometida e transparente, de modo a melhor atender aos interesses da sociedade.</p>
				<h4 class="subTitulo">2.1. CONCEITO</h4>
				<p>A governança no setor público possui múltiplas perspectivas conceituais e de aplicação (TI, pessoal, segurança pública, etc.). Embora existam alguns modelos referenciais (p.ex., Alemanha, Japão, Estados Unidos), cada país ou organização deve buscar e adequar o modelo mais pertinente à sua realidade e a seus valores.</p>
				<p>Não há na literatura contemporânea um conceito único ou mais correto sobre esse tema. O que se percebe é que a governança pública é um sistema/processo, tendo em vista que pode ser melhorado continuamente. <span class="semi-bold">É governar com a sociedade</span>.</p>
				<p>Governança pública é mais que governo, Estado ou Administração Pública. É a atuação desses, em colaboração recíproca, com o foco de gerar valor público, pela satisfação das expectativas da sociedade e da geração de confiança nas instituições.</p>
				<p>O valor público está relacionado com qualidade e capacidade institucional, desempenho, relacionamento e colaboração (Fórum Governança e Gestão no Setor Público, 2014).</p>
				<p>O Referencial básico de Governança do TCU conceitua governança no setor público conforme abaixo:</p>
				<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
				<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
					<p class="fonteMenor">Governança no setor público compreende essencialmente os mecanismos de <span class="semi-bold">liderança</span>, <span class="semi-bold">estratégia</span> e <span class="semi-bold">controle</span> postos em prática para <span class="semi-bold">avaliar</span>, <span class="semi-bold">direcionar</span> e <span class="semi-bold">monitorar</span> a atuação da <span class="semi-bold">gestão</span>, com vistas à condução de políticas públicas e à prestação de serviços de interesse da sociedade.</p>
				</div>
				<div class="clear"></div>
				<p>Quando falamos em governança no setor público, devemos observar os dois tipos básicos de atores envolvidos:</p>
				<ul><li><span class="semi-bold">Principal: sociedade</span> - o povo, por inferência aos termos constitucionais constantes do parágrafo único do art. 1º da Carta Magna<a class="link" href="javascript:void(0);" rel="popover" data-content="<p>Constituição da República Federativa do Brasil. Título I Dos Princípios Fundamentais Art. 1º A República Federativa do Brasil, formada pela união indissolúvel dos Estados e Municípios e do Distrito Federal, constitui-se em Estado Democrático de Direito e tem como fundamentos: I - a soberania; II - a cidadania; III - a dignidade da pessoa humana; IV - os valores sociais do trabalho e da livre iniciativa; V - o pluralismo político. <span class='semi-bold'>Parágrafo único. Todo o poder emana do povo, que o exerce por meio de representantes eleitos ou diretamente, nos termos desta Constituição</span>.</p>" data-toggle="popover" data-size="popover-small"><sup>6</sup></a>.</li>
				<li><span class="semi-bold">Agentes: autoridades, dirigentes, gerentes e colaboradores do setor público. Todos aqueles a quem foi delegada autoridade para administrar os ativos e os recursos públicos</span>. (Grifamos)</li></ul>
				<p>Nesse contexto, o principal, a sociedade, detém o poder, que pode ser exercido de forma conjunta e ordenada por meio de estruturas criadas para representá-la (DALLARI, 2005). Tanto principal quanto agentes podem se relacionar com outras partes interessadas, p.ex., setor privado, terceiro setor, em prol do desenvolvimento social.</p>
				<p class="textAlignCenter"><img src="../include/img/aulas/imagem_aula3_tela7.png" alt="Exemplo ator principal: cidadãos; exemplos atores agentes: representantes eleitos, conselhos, autoridade máxima, dirigentes e gerentes." /></p>
				<p class="textAlignCenter fonteMenor">Figura 2: Interação principal-agente (Referencial básico de Governança do TCU, 2014).</p>
				



              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina6.php', 'aula3pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


