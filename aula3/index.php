<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

  $acessibilidadeTxt = null;
  if ($acessibilidade == "sim") { 
    $acessibilidadeTxt = "?ac=sim";
  }; 
configHeader('Bem-vindo', 'exibir', '3','1', '13', 'index.php', 'aula3pagina2.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

    <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				<p class="paddingTop20">Caros colegas,</p>
				<p>Bom dia, boa tarde, boa noite! </p>
				<p>Sejam bem-vindos.</p>
				<p>Na aula de hoje, trataremos de um assunto contemporâneo muito interessante e que deve ser conhecido por todo bom gestor: a governança.</p>
				<p>Vamos iniciar?</p>			  
				<h3 class="titulo">Objetivos desta aula</h3>
				<div class="col-lg-6 col-md-6 col-sm-6 ">
					<p>Prezados participantes,</p>
					<p>Nesta aula, você conhecerá aspectos importantes relacionados à Governança e especialmente à governança no setor público. A ideia é semear informações para despertar seu interesse por esse tema tão vasto, instigante e inovador. Ao final da aula, você deverá ser capaz de:</p>						
					<div class="paddingBottom300">
					<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
					<div id="caixa-0">
						<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> compreender a origem, o conceito e a aplicabilidade da governança;</p>
						<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
						<div id="caixa-1">
							<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> distinguir governança de gestão; e </p>
							<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-2">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> entender a importância da governança, seus princípios e diretrizes, para o setor público. </p>
								<p>Ao final, verificaremos que o TCU tem dedicado atenção especial às ações de governança em toda a Administração Pública Federal (APF), especialmente na área de TI.</p>
							</div>
						</div>
					</div>
				</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
					<img src="../include/img/aulas/mapaMentalAula3Tela1.png" alt="" class="imgAulas" />
				</div>
				<div class="clear espacamentoLista"></div>
										
				
              </div>
            </div>   
           </div>
        </div>
    </article>    

    <footer>  
      <nav role="navigation" class="">
        <div class="textAlignCenter ">
          <a href=<?php echo '"aula3pagina2.php'.$acessibilidadeTxt.'"';?> class="btn btn-lg btn-success" title="Ir para próxima página">Avan&ccedil;ar <i class="fa fa-arrow-circle-o-right">  </i></a>
        </div>
      </nav>
    </footer>  

<!-- <?php  //configNavegacaoRodape('exibir', 'index.php', 'aula3pagina3.php'); //Rodapé automático aqui  ?>-->         
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


