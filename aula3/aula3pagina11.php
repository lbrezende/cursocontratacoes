<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Exemplo de Governança Aplicada – Governança de TI', 'exibir', '3','11', '13', 'aula3pagina10.php', 'aula3pagina12.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				        <p>O Acórdão 3.117/2014 do Plenário do TCU traz a análise dos dados de 2014, que foram apresentados por meio de três seções do relatório, clique em cada uma delas para ver suas características:</p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      1. Perfil de Governança de TI
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <p>Traz a situação atual da Administração Pública Federal, bem como a evolução em relação a 2012.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      2.  Índice de Governança de TI 2014 (iGovTI2014)
                    </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                    <p>São apresentados a definição e o método de cálculo desse índice, bem como os resultados consolidados da avaliação, com base nos números obtidos pelo iGovTI2014.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      3.  Principais Riscos e Ações de Controle
                    </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                    <p>São apresentados os níveis de risco associados às organizações, com base na relação entre o iGovTI2014 e o orçamento de TI aprovado para 2014, e sugeridas formas de atuação do TCU.</p>
                    </div>
                  </div>
                  </div>
                </div>
                <p>Dos trabalhos e avaliações realizadas nos últimos anos, bem como dos Acórdãos 1603/2008 e 2585/2012 - TCU - Plenário, resultaram em várias recomendações para os principais órgãos governantes superiores da Administração, no sentido de serem implantados controles de governança básicos, tais como:</p>
                <div class="paddingBottom300">
                  <p class="abreCaixa" id="0"><a href="#0" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-0">
                    <a name="0"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> planejamento institucional e de TI;</p>
                    <p class="abreCaixa" id="1"><a href="#1" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                    <div id="caixa-1">
                      <a name="1"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> vinculação do orçamento de TI às ações efetivamente planejadas;</p>
                      <p class="abreCaixa" id="2"><a href="#2" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                      <div id="caixa-2">
                        <a name="2"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> estruturação de quadros próprios de pessoal adequados e suficientes;</p>
                        <p class="abreCaixa" id="3"><a href="#3" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                        <div id="caixa-3">
                          <a name="3"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> gestão de serviços de TI e a realização sistemática de auditoria de TI;</p>
                          <p class="abreCaixa" id="4"><a href="#4" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                          <div id="caixa-4">
                            <a name="4"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> transparência do plano estratégico institucional e de TI;</p>
                            <p class="abreCaixa" id="5"><a href="#5" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                            <div id="caixa-5">
                              <a name="5"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> identificar claramente os processos críticos de negócio e designar os gestores de sistema de informação que dão suporte a esses processos; e </p>
                              <p class="abreCaixa" id="6"><a href="#6" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                              <div id="caixa-6">
                                <a name="6"></a><p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> promover melhorias na gestão de pessoas de TI.</p>
                              </div>

                            </div>

                          </div>

                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <p>Na linha das boas práticas de governança, o TCU também recomendou à Secretaria do Orçamento Federal que definisse critérios práticos de alocação de recursos públicos para TI, considerando métricas de risco, eficácia e efetividade da aplicação desses recursos, bem como os planos de melhoria de governança de Tecnologia da Informação das instituições com maiores riscos (Referencial básico de Governança do TCU, 2014, p. 32).</p>
                <p>A seguir, destacamos alguns pontos importantes do levantamento de governança de TI 2014, todos constantes do Acórdão 3.117/2014 do Plenário do TCU:</p>
                <ul>
                  <li>Nos aspectos de <span class="semi-bold">liderança</span>, destaque para a evolução na adoção das práticas relativas aos mecanismos basilares da estrutura de governança corporativa e de TI. Cita-se, como exemplo, o aumento do número de organizações que dispõem de comitê de TI, indicando o reconhecimento da importância desse colegiado como estrutura de apoio no processo de tomada de decisão.</li>
                  <li>Evolução das práticas de <span class="semi-bold">planejamento de TI</span>, sugerindo a efetividade das ações de indução promovidas pelos órgãos de controle e pelos órgãos governantes superiores.</li>
                  <li>As práticas de <span class="semi-bold">planejamento e de gestão de contratações de serviços de TI</span> mereceram destaque, tendo em vista os resultados expressivos apurados. Essa evolução pode ser atribuída, em especial, aos normativos que estabeleceram o processo e outras orientações sobre as contratações de TI, resultado do alinhamento de esforço entre os órgãos de controle e os órgãos governantes superiores.</li>
                  <li>Houve avanço no número de organizações que <span class="semi-bold">avaliam o seu desempenho na gestão e uso da TI</span> mediante o acompanhamento do alcance das metas associadas aos objetivos de TI.</li>
                  <li>Em geral, a alta administração, apesar dos expressivos valores geridos e do histórico de problemas relacionados a projetos e atividades malsucedidas, ainda não reconhece a importância da <span class="semi-bold">gestão de riscos de TI</span> para o sucesso de suas ações.</li>
                  <li>Ainda existem organizações que não reconhecem a importância da atividade de <span class="semi-bold">planejamento estratégico</span> para o sucesso de suas ações, seja com a não execução de um processo ou, ainda mais grave, com a não elaboração de um plano estratégico institucional. Esse ponto gera preocupação ao TCU.</li>
                  <li>Apesar da evolução identificada para as práticas de <span class="semi-bold">gestão de pessoas</span>, os níveis de adoção apurados ainda são objeto de atenção, haja vista que o sucesso das políticas e planos de TI está intrinsecamente relacionado com a capacidade<a href="javascript:void(0);" rel="popover" data-content="<p>A capacidade aqui referenciada está associada ao desenvolvimento de competências de TI (cursos, treinamentos, certificações etc.) e ao desempenho de pessoal de TI (formação de gestores, chefias etc.).</p>" data-toggle="popover" data-size="popover-small"><sup>9</sup></a> dos gestores e dos técnicos responsáveis por conduzi-los e implementá-los.</li>
                  <li>Merece atenção o elevado percentual de organizações que não executa <span class="semi-bold">processo de gerenciamento de projeto de TI</span>, situação que eleva o risco de insucesso de seus projetos, principalmente pela complexidade que, em geral, envolve o desenvolvimento e implantação de soluções de TI.</li>
                </ul>
                <p>À vista dos dados avaliados pelo TCU, o Acórdão 3.117/2014 – Plenário – indica a necessidade da continuidade das ações da Corte de Contas no sentido de induzir a melhoria da governança de TI na APF. Esses levantamentos e avaliações de governança permitem verificar a evolução da situação ao longo de um período e direcionar as ações posteriores.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina10.php', 'aula3pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


