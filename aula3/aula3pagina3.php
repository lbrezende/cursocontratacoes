<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Governança', 'exibir', '3','3', '13', 'aula3pagina2.php', 'aula3pagina4.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				<p class="paddingTop20">No Brasil, os primeiros ensaios dedicados à governança são recentes. A partir de 2001, foi publicada a <a href="http://www.planalto.gov.br/ccivil_03/leis/leis_2001/l10303.htm" target="_blank" title="link para lei 10303/2001">Lei 10.303/2001</a>, que alterou a <a href="http://www.planalto.gov.br/ccivil_03/leis/l6404consol.htm" target="_blank" title="link para Lei 6.404/1976">Lei 6.404/1976</a>, das sociedades por ações, e buscou reduzir riscos ao investidor minoritário e garantir sua participação no controle da empresa. Em 2002, a Comissão de Valores Mobiliários – CVM – publicou recomendações sobre governança.</p>
				<p>Em 2004 e posteriormente em 2009, o Instituto Brasileiro de Governança Corporativa – IBGC – publicou o Código das melhores práticas de Governança Corporativa. O foco inicial do documento foram as organizações empresariais, porém, na redação de 2009, utilizou-se o termo “organizações” para torná-lo mais abrangente e adaptável a outros tipos de instituições.</p>
				<p>A par dos benefícios que a governança traz, o Tribunal de Contas da União publicou, em 2014, o manual “GOVERNANÇA PÚBLICA: Referencial básico de Governança aplicável a órgãos e entidades da Administração Pública e ações indutoras de melhoria”.</p>
				<p>Esse Referencial teve por objetivos: </p>
				<div class="espacamentoLista">
					<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
					<div id="caixa-0">
						<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />  auxiliar a implementação do objetivo estratégico de promover a melhoria da governança no TCU (período 2011-2015); </p>
						<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
						<div id="caixa-1">
							<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> servir como referencial na execução de ações de controle externo sobre governança no setor público; e </p>
							<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-2">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> ser útil para os interessados na melhoria da governança.</p>
							</div>
						</div>
					</div>
				</div>
				<p class="espacamentoLista">Trata-se de documento que reúne e organiza boas práticas de governança pública que, se bem observadas, podem incrementar o desempenho de órgãos e entidades públicas. Embora o material seja direcionado para o setor público, os principais conceitos também se aplicam ao setor privado. </p>
				<div class="col-lg-6 col-md-6 col-sm-6 textAlignRight">
					<p>Vejamos a seguir um vídeo produzido pelo TCU, no qual são apresentados os principais conceitos sobre o tema governança, de forma simples e dinâmica. </p>
					<p>Além disso, o TCU possui uma página em seu portal dedicada ao tema, com diversas publicações. Vale a pena consultar <a href="http://portal2.tcu.gov.br/portal/page/portal/TCU/comunidades/governanca" target="_blank" title="link para governança no TCU">aqui</a>. </p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
					<div class="video-container">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/kGYdT1mJ-0c" frameborder="0" allowfullscreen></iframe>
					</div><br><br>				
				</div>
				<div class="clear"></div>
				<p>Procurando destacar modelos de gestão que abordem, de forma mais efetiva e eficaz, as novas e crescentes demandas da sociedade brasileira, a Secretaria de Gestão Pública – SEGEP, do Ministério do Planejamento, Orçamento e Gestão, revitalizou, em 2014, o Programa Nacional de Gestão Pública e Desburocratização – <a href="http://www.gespublica.gov.br/" target="_blank" title="link para gespública">GESPÚBLICA</a>.</p>
				<p>A finalidade do Programa é fortalecer a gestão pública a partir do <a href="http://goo.gl/5lDh2S" target="_blank" title="link para governança no TCU">Modelo de Excelência em Gestão Pública - MEGP</a><a href="javascript:void(0);" rel="popover" data-content="<p>   O Modelo de Excelência em Gestão Pública, de padrão internacional, expressa o entendimento vigente sobre o “estado da arte” da gestão contemporânea, é a representação de um sistema de gestão que visa a aumentar a eficiência, a eficácia e a efetividade das ações executadas. É constituído por elementos integrados, que orientam a adoção de práticas de excelência em gestão com a finalidade de levar as organizações públicas brasileiras a padrões elevados de desempenho e de qualidade em gestão. Fonte:<a href='http://www.concursosadm.com.br/index.php/noticias/86-modeloexcelenciagestaopublicagespublica' target='_blank'> clique aqui</a></p>" data-toggle="popover" data-size="popover-small"><sup>5</sup></a>, buscando mobilizar, preparar e motivar Administração e gestores para a atuação em prol da inovação e da melhoria da gestão.</p>
				<p>Os tópicos citados nesta aula objetivam contextualizar o aluno quanto à evolução do termo ao longo dos anos. Vale lembrar que diversas publicações adicionais contribuíram para tornar a governança algo real e aplicável às organizações. </p>
				<p>No que tange especificamente à governança no setor público, que será abordada mais adiante, o volume de trabalhos publicados é enorme. Mais informações e detalhes sobre a evolução histórica fogem ao foco principal deste curso e poderão ser obtidos por meio da leitura complementar e da consulta às referências bibliográficas.</p>

				
				
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina2.php', 'aula3pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


