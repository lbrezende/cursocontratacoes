<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Exemplo de Governança Aplicada – Governança de TI', 'exibir', '3','10', '13', 'aula3pagina9.php', 'aula3pagina11.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				<h3 class="titulo">4. EXEMPLO DE GOVERNANÇA APLICADA - GOVERNANÇA DE TI</h3>
				<p>Tendo em vista a complexidade e a criticidade da Tecnologia da Informação para a Administração Pública Federal e o aumento no volume dos gastos com TI nos últimos anos, a partir de meados de 2006, o TCU buscou especializar um grupo de auditores para tratar mais adequadamente do tema, criando a Secretaria de Fiscalização de Tecnologia da Informação - Sefti, especializada em assuntos envolvendo a TI. </p>
				<p>Considerando-se a importância e a abrangência desse tópico (a TI está em praticamente tudo), abordaremos alguns aspectos sobre como a governança aplicada à área de TI deve ser observada com atenção pelos profissionais e gestores da área, bem como pelos demais agentes de todo o setor público.</p>
				<p>De acordo com o TCU, Governança de TI ou Governança corporativa de TI é:</p>
				<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
				<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
					<p class="fonteMenor">o sistema pelo qual o uso atual e futuro da TI é dirigido e controlado. Significa avaliar e direcionar o uso da TI para dar suporte à organização e monitorar seu uso para realizar os planos. Inclui a estratégia e as políticas de uso da TI dentro da organização. A governança de TI é de responsabilidade dos executivos e da alta direção, consistindo em aspectos de liderança, estrutura organizacional e processos que garantam que a área de TI da organização suporte e aprimore as estratégias e objetivos da organização (Glossário do Levantamento de Governança de TI do TCU, 2014)</p>
				</div>
				<div class="clear"></div>
				<p>O sistema de Governança de TI compreende as políticas, as práticas, os processos, as estruturas organizacionais, entre outros mecanismos (Acórdão 3.117/2014 do Plenário do TCU).</p>
				<p>De modo a avaliar o cenário de governança de TI na Administração Pública Federal, o TCU realiza, desde 2007<a href="javascript:void(0);" rel="popover" data-content="<p>Acórdão 1.603/2008-TCU-Plenário. Acórdão 2.308/2010-TCU-Plenário.  Acórdão 2.585/2012-TCU-Plenário. Acórdão 755/2014-TCU-Plenário, Acórdão 1.684/2014-TCU-Plenário e Acórdão 1.015/2014-TCU-Plenário. Disponíveis na biblioteca complementar do curso.</p>" data-toggle="popover" data-size="popover-small"><sup>7</sup></a>, levantamentos baseados em questionários que abordam práticas de governança e de gestão de TI previstas em leis, regulamentos, normas técnicas e modelos internacionais de boas práticas.</p>
				<p>O trabalho do TCU é realizado a cada dois anos e é conduzido pela Sefti. Tem o objetivo de acompanhar a situação da Governança de Tecnologia da Informação na Administração Pública Federal. As informações obtidas no trabalho permitem identificar os pontos mais vulneráveis da governança de TI na APF, orientar a atuação do TCU como indutor do processo de aperfeiçoamento da governança de TI e, ao mesmo tempo, auxiliar na identificação de bons exemplos e modelos a serem disseminados.</p>
				<p>Para realizar as análises, o TCU classifica as organizações (estágios inicial, intermediário e aprimorado) de acordo com o iGovTI<a href="javascript:void(0);" rel="popover" data-content="<p>O índice de governança de TI (iGovTI) foi criado em 2010, no âmbito do 2º Levantamento de Governança de TI (Acórdão 2.308/2010-TCU-Plenário), com o propósito de orientar as organizações públicas no esforço de melhoria da governança e da gestão de TI. O índice também permite ao TCU avaliar, de um modo geral, a efetividade das ações adotadas para induzir a melhoria da situação de governança de TI na Administração Pública Federal. O iGovTI é o resultado da consolidação das respostas das organizações públicas ao questionário de governança de TI elaborado pela Sefti, por meio de fórmula que resulta em um valor que varia de 0 a 1.</p>" data-toggle="popover" data-size="popover-small"><sup>8</sup></a>. Em 2014, foram selecionadas 373 organizações públicas federais, tendo como critério principal a representatividade no orçamento da União e a autonomia de governança de TI. </p>
				<p>As organizações selecionadas foram organizadas em seis grupos, a saber:</p>
<style>
                    .blocoItens {
                      background: #1C6094;
                      color: white;
                      font-size:14px;
                      border-bottom: 2px solid white;
                      height: 132px;
                      padding:0px;

                    }
                    .blocoItens p.titulo {
                      text-indent:0em; text-align:center; font-size: 18px; padding-bottom:5px;
                      color: white !important;
                    }
                    .blocoItens p {
                      text-indent:0em;
                      color: white !important;
                    }                    
                  </style>
                  <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 hidden-xs">
                      
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                      <div class="row">
                            <div class=" blocoItens col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img class="hidden-xs" src="../include/img/icons/setaazul.png" style="float:right; left:0px;" alt="">
                                <div style="padding:20px">
                                  <p class="titulo"><strong>EXE- Dest</strong><br></p>
                                  <p>Empresas públicas federais e sociedade economia mista</p>
                                </div>
                            </div>
                            <div class=" blocoItens col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img class="hidden-xs" src="../include/img/icons/setabranca.png" style="float:left; right:0px;" alt="">
                                <img class="hidden-xs" src="../include/img/icons/setaazul.png" style="float:right; left:0px;" alt="">
                                <div style="padding:20px">                                
                                  <p class="titulo"><strong>JUD</strong><br></p>
                                  <p>Organizações  que fazem parte do Poder Judiciário</p>
                                </div>
                            </div>       
                            <div class=" blocoItens col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img class="hidden-xs" src="../include/img/icons/setabranca.png" style="float:left; right:0px;" alt="">
                                <div style="padding:20px">                                
                                  <p class="titulo"><strong>EXE- Sisp</strong><br></p>
                                  <p>Sistema de Administração dos Recursos de Informação e Informática (Sisp)</p>
                                </div>
                            </div>
                            <div class=" blocoItens col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img class="hidden-xs" src="../include/img/icons/setaazul.png" style="float:right; left:0px;" alt="">
                                <div style="padding:20px">                                
                                  <p class="titulo"><strong>LEG</strong><br></p>
                                  <p>Organizações que fazem parte do Poder Legislativo</p>
                                </div>
                            </div>
                            <div class=" blocoItens col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img class="hidden-xs" src="../include/img/icons/setabranca.png" style="float:left; right:0px;" alt="">
                                <img class="hidden-xs" src="../include/img/icons/setaazul.png" style="float:right; left:0px;" alt="">
                                <div style="padding:20px">                                
                                  <p class="titulo"><strong>MPU</strong><br></p>
                                  <p>Organizações  que fazem parte do Ministério Público da União (MPU)</p>
                                </div>
                            </div>
                            <div class=" blocoItens col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <img class="hidden-xs" src="../include/img/icons/setabranca.png" style="float:left; right:0px;" alt="">
                                <div style="padding:20px">                                
                                  <p class="titulo"><strong>Terceiro Setor</strong><br></p>
                                  <p>Não se enquadram em nenhum dos seguimentos anteriores. Ex.: Associação das Pioneiras Sociais</p>
                                </div>
                            </div>               
                      </div>
                  </div>                  
                                                                                                          
                  </div>
				  <p class="paddingTop20">Conforme mencionado pelo próprio TCU, inicialmente a situação era de desgovernança, contudo, tendo em vista sua atuação, em parceria com os órgãos envolvidos e outros organismos e entidades, o cenário vem apresentando melhoras significativas.</p>
				  <p>A análise dos dados obtidos revela uma tendência de evolução da situação, reforçando a importância da continuidade das ações de indução de melhoria da governança de TI (Acórdão 3.117/2014 do Plenário do TCU). </p>
				  <p>Apesar da melhora, verifica-se que o nível de adoção de boas práticas ainda é insuficiente em relação a muitas atividades fundamentais para que a TI agregue o valor devido aos resultados organizacionais.</p>
    			       
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula3pagina9.php', 'aula3pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


