<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Governança', 'exibir', '3','2', '13', 'index.php', 'aula3pagina3.php', '<h4 style="font-weight:bold">Governança</h4>');
?> 
<link rel="stylesheet" type="text/css" href="../include/css/stylesheet2.css" />

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
				<h3 class="titulo">1.  GOVERNANÇA </h3>
				<p>A palavra governança está na moda. Hoje em dia, é muito comum ouvirmos esse termo no noticiário, associado à atuação do setor privado e da Administração Pública.</p>
				<p>Quando falamos em governança no Brasil, devemos destacar a atenção especial dada ao tema pelo <a href="http://www.ibgc.org.br/index.php" target="_blank" title="link para o IBGC">Instituto Brasileiro de Governança Corporativa – IBGC</a> e pelo Tribunal de Contas da União, que, há alguns anos, vem desenvolvendo trabalhos de divulgação da matéria, por meio de guias referenciais, palestras e acórdãos abordando o assunto<a href="javascript:void(0);" rel="popover" data-content="<p>Acordão nº-2585/2012 Plenário TCU. Relatório de levantamento. Avaliação da governança de Tecnologia da Informação na Administração Pública Federal. Oportunidades de melhoria. Recomendações.</p>" data-toggle="popover" data-size="popover-small"><sup>1</sup></a>.</p>
				<div class="espacamentoBottom">
					<div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
						<p class="tituloSaibaMais">Instituto Brasileiro de Governança Corporativa – IBGC</p>
						<p>O Instituto Brasileiro de Governança Corporativa – IBGC é uma organização exclusivamente dedicada à promoção da Governança Corporativa no Brasil e o principal fomentador das práticas e discussões sobre o tema no País, tendo alcançado reconhecimento nacional e internacional. Fundado em 27 de novembro de 1995, o IBGC – sociedade civil de âmbito nacional, sem fins lucrativos – tem o propósito de ser referência em Governança Corporativa, contribuindo para o desempenho sustentável das organizações e influenciando os agentes da nossa sociedade no sentido de maior transparência, justiça e responsabilidade. Fonte: Instituto Brasileiro de Governança Corporativa. <a href="http://www.ibgc.org.br/userfiles/Codigo_julho_2010_a4.pdf" target="_blank" title="Código de melhores práticas de governança">Código das melhores práticas de Governança Corporativa</a>. 4.ed. / Instituto Brasileiro de Governança Corporativa. São Paulo, SP : IBGC, 2009.-publica.Para conhecer mais sobre o IBGC e sua atuação no Brasil, assista ao vídeo a seguir:</p>
						<div class="video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/pnqeriqKWPg" frameborder="0" allowfullscreen></iframe>
                        </div>
					</div>
				</div>
				<div class="clear"></div>
				<p>Em nosso estudo, utilizaremos o <a href="http://goo.gl/yJozPv" target="_blank" title="link para o codigo das melhores práticas">Código das melhores práticas de Governança Corporativa do IBGC, versão 2009</a>,
 e o <a href="http://portal2.tcu.gov.br/portal/pls/portal/docs/2624039.PDF" target="_blank" title="link para o referencial básico de governança do TCU">Referencial básico de Governança do TCU</a>, além de outras publicações pertinentes ao assunto.</p>

				<h4 class="subTitulo">1.1. ORIGEM DO TERMO</h4>
				<p>A gênese do termo governança está associada ao momento em que organizações privadas deixaram de ser geridas diretamente por seus proprietários e passaram a ser administradas por terceiros, mediante delegação de autoridade e de poder para gerir recursos pertencentes aos proprietários (Referencial básico de Governança do TCU, 2014).</p>
				<p>Embora o termo seja antigo, sua importância advém das últimas três décadas, associado inicialmente às empresas privadas e, posteriormente, também ao Estado e às organizações públicas.</p>
				<p>A seguir listamos, resumidamente, alguns marcos evolutivos importantes para a construção do termo governança:</p>
					

				<h3 class="titulo" style="margin:50px; margin-bottom:100px; text-align:center;">Evolução do termo governança </h3>

				<div class="container">
				    
				    <img src="images/topo.png" alt="" class="hidden-xs" style="left: 50%; margin-left:19px;   position: absolute;    transform: translate(-50%, -50%);    margin-top: -21px;">
				    <ul class="timeline" >
				        <li>
				          <div class="timeline-badge">2015</div>
				            <div class="timeline-panel timeline-panel-botao f-2015">
				              <a class="btn btn-white b-2015">Clique para ver 2015</a>
				            </div>
				            <div class="timeline-panel a-2015">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 2015</h4>
				            </div>
				            <div class="timeline-body">
				              <p>Contemporaneamente, o G8 (grupo dos oito países mais desenvolvidos do mundo) e organizações como o Banco Mundial, o Fundo Monetário Internacional – FMI – e a Organização para Cooperação e Desenvolvimento Econômico – OCDE – trabalham para difundir a governança.</p>
				            </div>
				          </div>
				        </li>
				        <li class="timeline-inverted">
				          <div class="timeline-badge">2004</div>
				            <div class="timeline-panel timeline-panel-botao f-2004">
				              <a class="btn btn-white b-2004">Clique para ver 2004</a>
				            </div>
				            <div class="timeline-panel a-2004">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 2004</h4>
				            </div>
				            <div class="timeline-body">
				              <p>Em 2004, o COSO publicou o Enterprise Risk Management - Integrated Framework (Gerenciamento de riscos corporativos – Estrutura integrada). Ainda hoje é tido como referência no tema gestão de riscos.</p>
				            </div>
				          </div>
				        </li>
				        <li>
				          <div class="timeline-badge">2002</div>
				            <div class="timeline-panel timeline-panel-botao f-2002">
				              <a class="btn btn-white b-2002">Clique para ver 2002</a>
				            </div>
				            <div class="timeline-panel a-2002">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 2002</h4>
				            </div>
				            <div class="timeline-body">
				              <p>Em 2002, em decorrência de escândalos envolvendo demonstrações contábeis fraudulentas ratificadas por empresas de auditorias, publicou-se, nos Estados Unidos, a Lei Sarbanes-Oxley<a href="javascript:void(0);" rel="popover" data-content="<p  style='color:#000;'>Lei Sarbanes-Oxley - Ato normativo de reformas corporativas idealizado pelos parlamentares Paul Sarbanes e Michael Oxley, já considerada a mais importante legislação do mercado de capitais. Seu objetivo é proteger os investidores através de medidas rígidas que visam eliminar o conflito de interesses, permitir uma auditoria eficaz e garantir a fidelidade das informações nos relatórios financeiros. Fonte: <a href='http://www.sec.gov/' target='_blank'>http://www.sec.gov</a>. Glossário de Riscos Banco Central do Brasil.</p>" data-toggle="popover" data-size="popover-small"><sup  style="color:red;">4</sup></a>. O objetivo era melhorar os controles para garantir a fidedignidade das informações constantes dos relatórios financeiros.</p>
				            </div>
				          </div>
				        </li>
				        <li class="timeline-inverted">
				          <div class="timeline-badge">1992</div>
				            <div class="timeline-panel timeline-panel-botao f-1992">
				              <a class="btn btn-white b-1992">Clique para ver 1992</a>
				            </div>
				            <div class="timeline-panel a-1992">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 1992</h4>
				            </div>
				            <div class="timeline-body">
				              <p>Em 1992, o Committee of Sponsoring Organizations of the Treadway Commission – COSO<a href="javascript:void(0);" rel="popover" data-content="<p  style='color:#000;'>COSO - Committee of Sponsoring Organizations of the Treadway Commission é uma organização norte-americana voluntária do setor privado, dedicada a melhoria da qualidade dos relatórios financeiros de forma que reflitam ética no negócio, controles internos eficazes e governança corporativa. COSO foi composto originalmente em 1985 para assessorar a “National Commission on Fraudulent Financial Reporting”, através de uma iniciativa independente do setor privado que deve estudar os motivos que estimulam a fraude nos relatórios financeiros e produzir recomendações para as empresas públicas, suas respectivas auditorias independentes, para a SEC, outros órgãos reguladores e para instituições educacionais. Fonte: <a href='http://wwww.coso.org/' target='_blank'>www.coso.org</a>. Glossário de Riscos Banco Central do Brasil.</p>" data-toggle="popover" data-size="popover-small"><sup  style="color:red;">3</sup></a> (Comitê das Organizações Patrocinadoras da Comissão Treadway) publicou o Internal Control - Integrated Framework (Controle Interno – Estrutura Integrada).</p>
				            </div>
				          </div>
				        </li>
				        <li>
				          <div class="timeline-badge">1990</div>
				            <div class="timeline-panel timeline-panel-botao f-1990">
				              <a class="btn btn-white b-1990">Clique para ver 1990</a>
				            </div>
				            <div class="timeline-panel a-1990">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 1990</h4>
				            </div>
				            <div class="timeline-body">
				              <p>No começo da década de 90, diante das crises financeiras, o Banco da Inglaterra criou o Comitê de Governança Corporativa<a href="javascript:void(0);" rel="popover" data-content="<p  style='color:#000;'>O Comitê de Governança Corporativa foi criado em resposta à contínua preocupação com padrões de relatórios financeiros e prestação de contas, em decorrência dos escândalos financeiros envolvendo principalmente o Banco de Crédito e Comércio Internacional (BCCI)  e casos Maxwell. O Comitê foi presidido por Sir Adrian Cadbury e tinha a missão de avaliar os aspectos de governança corporativa relativas às demonstrações financeiras e prestação de contas. O relatório final “Os aspectos financeiros da governança corporativa”(conhecido como o Relatório Cadbury) foi publicado em Dezembro de 1992 e continha uma série de recomendações para elevar os padrões de governança corporativa.</p>" data-toggle="popover" data-size="popover-small"><sup  style="color:red;">2</sup></a>, que elaborou o Código das melhores práticas de Governança Corporativa, trabalho que resultou no Cadbury Report  (Referencial básico de Governança do TCU, 2014, p.15).</p>
				            </div>
				          </div>
				        </li>
				        <li class="timeline-inverted">
				          <div class="timeline-badge">1934</div>
				            <div class="timeline-panel timeline-panel-botao f-1934">
				              <a class="btn btn-white b-1934">Clique para ver 1934</a>
				            </div>
				            <div class="timeline-panel a-1934">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 1934</h4>
				            </div>
				            <div class="timeline-body">
				              <p>Em 1934, foi criada, nos Estados Unidos, a US Securities and Exchange Comission, organização americana responsável por proteger investidores; garantir a justiça, a ordem e a eficiência dos mercados e facilitar a formação de capital até os dias atuais.</p>
				            </div>
				          </div>
				        </li>
				        <li>
				          <div class="timeline-badge">1932</div>
				            <div class="timeline-panel timeline-panel-botao f-1932">
				              <a class="btn btn-white b-1932">Clique para ver 1932</a>
				            </div>
				            <div class="timeline-panel a-1932">
				            <div class="timeline-heading">
				              <h4 class="timeline-title"> 1932</h4>
				            </div>
				            <div class="timeline-body">
				              <p>Berle e Means (1932 apud TCU), desenvolvem um dos primeiros estudos acadêmicos tratando de assuntos correlatos à governança. É papel do Estado regular as organizações privadas.</p>
				            </div>
				          </div>
				        </li>      
				    </ul>
				    <img src="images/bottom.png" alt="" class="hidden-xs" style="left: 50%; margin-left:-21px;   position: absolute;    transform: translate(-50%, -50%);    margin-top: 10px;">
				    <div class="clear" style="margin-bottom:50px"></div>
				</div>
				</div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'index.php', 'aula3pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 

<script>

$(document).ready(function(){
	  $('.a-2015').hide();
	  $('.a-2004').hide();
	  $('.a-2002').hide();
	  $('.a-1992').hide();
	  $('.a-1990').hide();  
	  $('.a-1934').hide();
	  $('.a-1932').hide();  
	  
	  $('.b-2015').click(function(){
	    $('.f-2015').hide();
	    $('.a-2015').show();
	  })
	  
	  $('.b-2004').click(function(){
	    $('.f-2004').hide();
	    $('.a-2004').show();
	  })
	  
	  $('.b-2002').click(function(){
	    $('.f-2002').hide();
	    $('.a-2002').show();
	  })
	  
	  $('.b-1992').click(function(){
	    $('.f-1992').hide();
	    $('.a-1992').show();
	  })
	  
	  $('.b-1990').click(function(){
	    $('.f-1990').hide();
	    $('.a-1990').show();
	  })
	  
	  $('.b-1934').click(function(){
	    $('.f-1934').hide();
	    $('.a-1934').show();
	  })  
	  
	  $('.b-1932').click(function(){
	    $('.f-1932').hide();
	    $('.a-1932').show();
	  })    
	})	

</script>


