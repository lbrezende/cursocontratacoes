<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Otimizando a Demanda – Noções sobre Planejamento', 'exibir', '2','4', '13', 'aula2pagina3.php', 'aula2pagina5.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">2.  OTIMIZANDO A DEMANDA – NOÇÕES SOBRE PLANEJAMENTO</h3>
						<p>De um planejamento ineficiente, nasce a maioria dos problemas que permeiam a execução das demandas públicas. Assim, é uma <span class="semi-bold">boa prática</span> elaborar com atenção um planejamento do que se deseja demandar. Mas como isso pode ser feito? Veremos a seguir.</p>
						<h4 class="subTitulo">2.1.  O QUE É O PLANEJAMENTO - CONCEITOS, ETAPAS, IMPORTÂNCIA</h4>
						<p>O planejamento é essencial para que as organizações operem de modo assertivo, especialmente em ambientes dinâmicos, complexos e competitivos. O objetivo é dar continuidade às suas operações rumo ao sucesso de seus projetos.</p>
						<p>Planejar significa focar no futuro e escolher procedimentos e métodos com vistas ao alcance de objetivos traçados.</p>
						<p>Para a <a id="modal" title="informações do Min Carlos Ayres Britto" href="javascript:void(0);" style="padding-left:0px;" class="btn btn-primary btn-small" data-toggle="modal" data-target="#myModal">Profª Sulivan D. Fischer Salgado <sup><img src="../include/img/icons/plus.jpg" width="10px" height="10px" alt="clique aqui para saber mais sobre a Professora Sulivan D. Fischer Salgado" style="margin-left:5px;" /></sup></a> (2003):</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">O planejamento ajuda a preparar a organização para mudanças contínuas. Apesar de os planejadores não poderem controlar o futuro, eles deveriam ao menos tentar identificar e isolar fatores ambientais e mudanças que possam influenciar o futuro.</p>
						</div>
						<div class="clear"></div>
						<p>A função de planejamento exige o reconhecimento do ambiente da organização, o estímulo à inovação, a persistência e o encorajamento contínuo da equipe.</p>
						<p>Planejar é função inicial da administração. Antes que qualquer outra função administrativa seja executada, a Administração precisa planejar, determinar os objetivos e os meios necessários para alcançá-los adequadamente.</p>
						<p>A seguir, alguns <span class="semi-bold">conceitos</span> de Planejamento (SULIVAN, 2003):</p>
						<div class="paddingBottom300">
							<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-0">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />  Planejar é o ato de determinar os objetivos da organização e os meios para alcançá-los (<a href="https://en.wikipedia.org/wiki/Richard_L._Daft" target="_blank" title="acesse o curriculo de Richard L. Daft">Richard L. Daft</a>).</p>
								<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
								<div id="caixa-1">
									<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />  Planejar é decidir antecipadamente aquilo que deve ser feito, como fazer, quando fazer e quem deve fazer (<a href="http://www.amazon.com/Study-accompany-Koontz-ODonnell-Weihrich/dp/0070353794" target="_blank" title="acesse o livro de Koontz, O’Donnell & Heihrich">Koontz, O’Donnell & Heihrich</a>).</p>
									<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
									<div id="caixa-2">
										<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />  Planejar é o processo de determinar como o sistema administrativo deverá alcançar os seus objetivos. Em outras palavras, é determinar como a organização deverá ir para onde deseja chegar (<a href="https://books.google.com.br/books/about/Administra%C3%A7%C3%A3o_moderna.html?hl=pt-BR&id=8m8ZAAAACAAJ" target="_blank" title="acesse o livro de Samuel C. Certo">Samuel C. Certo</a>).</p>
										<p class="abreCaixa" id="3"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
										<div id="caixa-3">
											<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />  Planejar é o processo de estabelecer objetivos e de determinar o que deve ser feito para alcançá-los (<a href="http://www.amazon.com/John-R.-Schermerhorn/e/B001IGNL2O " target="_blank" title="acesse o livro de John R. Schermerhorn, Jr">John R. Schermerhorn, Jr.</a>).</p>
											<p class="paddingTop20">O planejamento possui, em regra, três <span class="semi-bold">etapas</span> básicas:</p>
											<p class="textAlignCenter">
												<img src="../include/img/aulas/1--Definicao-de-um-objetivo-ou-meta.png" alt="1 Definição de um objetivo ou meta" />
												<img src="../include/img/aulas/2-Identificacao-e-avaliacao-da-situacao-atual.png" alt="2 Identificação e avaliação da situação atual" />
												<img src="../include/img/aulas/3-Desenvolvimento-de-estrategias.png" alt="3-Desenvolvimento-de-estratégias" />
											</p>
											<p>Além dessas três, há ainda etapas adicionais, citadas por alguns autores, que agregam outras funções ao planejamento:</p>
											<p class="textAlignCenter">
												<img src="../include/img/aulas/4---Implementacao-de-um-plano.png" alt="4---Implementação-de-um-plano" />
												<img src="../include/img/aulas/5-Controle-da-implementacao-do-plano.png" alt="5-Controle-da-implementação-do-plano" />
												<img src="../include/img/aulas/6---Avaliacao-e-monitoramento-da-eficacia-do-plano.png" alt="6---Avaliação-e-monitoramento-da-eficácia-do-plano" />
											</p>
											<p>O acréscimo dessas etapas decorre do fato de que o processo de planejamento é cíclico e dinâmico, deve ser avaliado, controlado e monitorado durante a implementação. </p>
										</div>
									</div>
								</div>
							</div>
						</div>
	
						
                    </div>
                  </div>   
                 </div>
				</div>
            </article> 
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <br>
              <i class="icon-credit-card icon-7x"></i>
              <h4 id="myModalLabel" class="semi-bold">Prof. Dra. Sulivan Desirée Fischer Salgado</h4>
             
            </div>
            <div class="modal-body">
              <div class="row form-row" style="text-align:center;">
                <div class="row">

                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 "><img src="../include/img/aulas/profaSulivan.png" alt="" style="width:133px"><br><br></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <p class="no-margin">Doutora em Administração, atualmente é Coordenadora do curso de Administração Pública da UDESC.</p>
              <br></div>
                </div>
                  
              </div>
            </div>
            <div class="modal-footer">
              <a class="btn btn-default" data-dismiss="modal">Voltar para a aula</a>
              <a href="http://lattes.cnpq.br/6110817489088621"  target="_blank" class="btn btn-primary">Ver currículo de Prof. Dra. Sulivan Desirée</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->					
<?php  configNavegacaoRodape('exibir', 'aula2pagina3.php', 'aula2pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


