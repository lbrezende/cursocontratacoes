<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Considerações Finais', 'exibir', '2','12', '13', 'aula2pagina11.php', 'aula2pagina13.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">RESUMO DA AULA</h3>
						<p>Nesta aula, verificamos que o planejamento está inserido em nossas vidas. Tivemos a oportunidade de elencar os principais pontos dessa importante função administrativa, que é essencial ao trabalho de um bom gestor público. Por fim, verificamos que, nas contratações públicas, o planejamento inicial deve ser elaborado a partir do estudo e da definição da real necessidade do órgão, considerando-se todos os aspectos técnicos relevantes ao objeto que se deseja contratar. </p>
						<p>Na próxima aula, conheceremos a governança e a governança de TI.</p>
						<p>Até lá...</p>
						<p>Abraços e bom estudo.</p>
						<h3 class="titulo">MENSAGEM</h3>
						<p class="textAlignCenter"><img src="../include/img/aulas/Mensagem_Aula 2.jpg" style="" alt="Palavra do Autor" /></p>
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina11.php', 'aula2pagina13.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


