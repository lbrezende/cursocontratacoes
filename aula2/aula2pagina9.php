<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('O Gestor Público e o Planejamento', 'exibir', '2','9', '13', 'aula2pagina8.php', 'aula2pagina10.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">3.  O GESTOR PÚBLICO E O PLANEJAMENTO </h3>
						<p>Planejar é atividade inerente à atuação do gestor público. <span class="semi-bold">Planejar</span> é buscar a <span class="semi-bold">efetividade</span>.</p>
						<p>À medida que um planejamento é bem elaborado, todos os pontos de especificação da demanda são abordados e trabalhados detalhadamente. Assim, a instrução processual retratará o teor do planejamento preliminar destinado a justificar o processo de contratação.</p>
						<p>Um planejamento adequado facilita o trabalho das áreas envolvidas nos procedimentos administrativos. Os autos tramitam mais rapidamente entre as áreas, pois o planejamento consolida todos os elementos necessários para avaliação da demanda.</p>
						<p>Uma demanda bem planejada gera um processo administrativo completo e consistente, que possui os pontos essenciais para a perfeita avaliação de conformidade e aprovação da despesa.</p>
						<p><span class="semi-bold">Planejar</span> é obrigação. </p>
						<p>De acordo com CAVALCANTI (2013), planejar é <span class="semi-bold">dever</span> do administrador público, conforme abaixo:</p>
						<div class="paddingBottom300">
							<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-0">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Dever jurídico-constitucional,</span>  tendo em vista que os resultados de um bom planejamento devem atender às diretrizes constitucionais, em especial ao Princípio da eficiência, insculpido no art.37 da Carta Magna Brasileira<a href="javascript:void(0);" rel="popover" data-content="<p> Constituição Federal Brasileira Art. 37. A administração pública direta e indireta de qualquer dos Poderes da União, dos Estados, do Distrito Federal e dos Municípios obedecerá aos princípios de legalidade, impessoalidade, moralidade, publicidade e eficiência e, também, ao seguinte: (...)(Redação dada pela Emenda Constitucional nº 19, de 1998)</p>" data-toggle="popover" data-size="popover-small"><sup>5</sup></a>.</p>
								<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
								<div id="caixa-1">
									<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Dever jurídico-legal,</span> desde muito tempo consubstanciado no ordenamento jurídico brasileiro, como podemos constatar a partir do exame do <a href="http://www.planalto.gov.br/ccivil_03/decreto-lei/Del0200compilado.htm" target="_blank" title="link para o Decreto lei 200/1967">Decreto-Lei 200/1967</a>, que dispõe sobre a organização da Administração Federal, estabelece diretrizes para a Reforma Administrativa e conforme excerto abaixo, <i>in verbis</i>:</p>
									<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
									<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
										<p class="fonteMenor">DOS PRINCÍPIOS FUNDAMENTAIS</p>
										<p class="fonteMenor semPaddingBorda"> Art. 6º As atividades da Administração Federal obedecerão aos seguintes princípios fundamentais:</p>
										<p class="fonteMenor semPaddingBorda"><span class="semi-bold">I - Planejamento.</span></p>
										<p class="fonteMenor semPaddingBorda">II - Coordenação.</p>
										<p class="fonteMenor semPaddingBorda">III - Descentralização.</p>
										<p class="fonteMenor semPaddingBorda">IV - Delegação de Competência.</p>
										<p class="fonteMenor">V - Contrôle. [sic]</p>
										<p class="fonteMenor">CAPÍTULO I</p>
										<p class="fonteMenor">DO PLANEJAMENTO</p>
										<p class="fonteMenor">Art. 7º A ação governamental obedecerá a planejamento que vise a promover o desenvolvimento econômico-social do País e a segurança nacional, norteando-se segundo planos e programas elaborados, na forma do Título III, e compreenderá a elaboração e atualização dos seguintes instrumentos básicos: (...) (Grifamos)</p>
										
									</div>
									<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
									<div id="caixa-2">
										<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Dever político,</span> pois o gestor público gere recursos provenientes do bolso dos consumidores. Tendo em vista essa origem de recursos, espera-se, sempre, que o dinheiro público administrado pelos agentes governamentais públicos retorne, de alguma maneira, em benefícios para a sociedade. Nesse ponto, o Manual para Gestores de Contratos do STF destaca:</p>
										<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
										<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
											<p class="fonteMenor">A complexidade da gestão no âmbito da Administração Pública, e principalmente no Poder Judiciário, de quem a sociedade espera exemplos de retidão nas ações administrativas implementadas, exige atenção especial e mobilização permanente dos gestores, nos diversos níveis da estrutura orgânica de cada instituição. Esse comprometimento é essencial para o fiel cumprimento dos objetivos estratégicos estabelecidos e para a obtenção de resultados que possam ser acolhidos, compreendidos e aprovados não só pelos órgãos de controle, mas sobretudo pelo cidadão brasileiro, que exerce controle social soberano na avaliação dos atos públicos.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<p>É interessante observar que o próprio processo de planejamento deve ser programado de modo participativo com a equipe de trabalho, para que os resultados obtidos com o esforço possam alcançar os objetivos definidos.</p>
						<p>Conforme é possível verificar, a atividade de planejamento é função inicial da administração que está intrinsecamente associada ao trabalho de um gestor efetivo. O tempo despendido com o planejamento de uma demanda resulta em maior qualidade da instrução processual, bem como na otimização do trâmite processual.</p>

                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina8.php', 'aula2pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


