<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referências Bibliográficas', 'exibir', '2','13', '13', 'aula2pagina12.php', '', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">SUGESTÕES DE LEITURA COMPLEMENTAR</h3>
						<p><span class="semi-bold">Artigos:</span></p>
						<p>MUSSAK, Eugênio. <i>O que fazer primeiro? O importante ou o urgente?</i> Você S/A. Disponível em:<a href="http://exame.abril.com.br/revista-voce-sa/edicoes/194/noticias/o-que-fazer-primeiro" target="_blank" title="acesse o artigo"> http://exame.abril.com.br/revista-voce-sa/edicoes/194/noticias/o-que-fazer-primeiro</a>, acesso em maio/2015.</p>
						<p>ROCHA, Fátima. <i>Seis passos para tomar boas decisões. Arquivo disponível na plataforma de ensino</i>.</p>
						<p><i>Guia de boas práticas em contratação de soluções de TI</i>. Ministério do Planejamento, Orçamento e Gestão. Disponível em:<a href="http://goo.gl/qSIEFG" target="_blank" title="acesse o artigo">  http://goo.gl/qSIEFG</a>. Arquivo disponível  na plataforma de ensino.</p>
						<p><span class="semi-bold">Livro:</span></p>
						<p>FURTADO, Lucas Rocha. Curso de Direito Administrativo. 4. ed. Belo Horizonte: Fórum, 2012.</p>
						<p><span class="semi-bold">Vídeo:</span></p>
						<p>Administração do Tempo - Como definir prioridades para seu dia a dia. Disponível em:<a href="https://www.youtube.com/watch?v=3opTob_CEVg" target="_blank" title="acesse o artigo"> https://www.youtube.com/watch?v=3opTob_CEVg</a></p>
						<h3 class="titulo">REFERÊNCIAS BIBLIOGRÁFICAS</h3>
						<p>ATENDIMENTO FANTÁSTICO. Artigo <i>O novo diferencial de sua empresa</i>. Disponível em: <a href="http://atendimentofantastico.com/prioridade-x-urgencia-x-importancia/" target="_blank" title="acesse a referencia"> http://atendimentofantastico.com/prioridade-x-urgencia-x-importancia/</a>, Fevereiro de 2014. Acesso em 15/10/2014.
						<p>BIO, Sérgio Rodrigues. <i>Sistemas de Informação: um enfoque gerencial</i>. São Paulo: Atlas, 1996.</p>
						<p>CASTRO, Rodrigo Batista de. <i>Eficácia, Eficiência e Efetividade na Administração Pùblica</i>. 30º Encontro da ANPAD. Setembro de 2006. Salvador/BA – Brasil. Disponível em: <a href="" target="_blank" title="acesse a referência">http://www.anpad.org.br/enanpad/2006/dwn/enanpad2006-apsa-1840.pdf</a>. Acesso em 10/10/2014.</p>
						<p>Brasil. Tribunal de Contas da União. <i>Guia de boas práticas em contratação de soluções de tecnologia da informação: riscos e controles para o planejamento da contratação / Tribunal de Contas da União</i>. – Versão 1.0. – Brasília: TCU, 2012.</p>
						<p>Brasil. Tribunal de Contas da União. <i>Governança Pública: referencial básico de governança aplicável a órgãos e entidades da administração pública e ações indutoras de melhoria / Tribunal de Contas da União</i>. – Brasília: TCU, Secretaria de Planejamento, Governança e Gestão, 2014. </p>
						<p>CAVALCANTI, Augusto Sherman. <i>O novo modelo de contratação de soluções de TI pela Administração Pública</i>. Belo Horizonte: Fórum, 2013.</p>
						<p>Constituição Federal Brasileira.</p> 
						<p>CHIAVENATO, Idalberto. <i>Introdução à Teoria Geral da Administração: uma visão abrangente da moderna administração das organizações</i>. Rio de Janeiro: Elsevier, 2003.</p>
						<p>CHIAVENATO, Idalberto. <i>Recursos Humanos na Empresa: pessoas, organizações e sistemas</i>. São Paulo: Atlas, 1994.</p>
						<p>Decreto 2.271, de 7 de julho de 1997. Dispõe sobre a contratação de serviços pela Administração Pública Federal direta, autárquica e fundacional e dá outras providências.</p>
						<p>FURTADO, Lucas Rocha. <i>Curso de Direito Administrativo</i>. 4. ed. Belo Horizonte: Fórum, 2012.</p>
						<p>IN MP/SLTI Nº4/2010, art. 2º, inciso III. Disponível em: <a href="http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/instrucao-normativa-mp-slti-no04" target="_blank" title="acesse a referência">http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/instrucao-normativa-mp-slti-no04</a>.</p>
						<p>Lei 8.666, de 21 de junho de 1993. Regulamenta o art. 37, inciso XXI, da Constituição Federal, institui normas para licitações e contratos da Administração Pública e dá outras providências.</p>
						<p>Manual para Gestores de Contratos do STF .</p>
						<p>MEIRELLES, Hely Lopes. <i>Direito Administrativo Brasileiro</i>. São Paulo: Malheiros, 2002.</p>
						<p>PIETRO, Maria Sylvia Zanella Di. <i>Direito Administrativo</i>. São Paulo: Atlas, 2007.</p>
						<p>Relatório de Auditoria de Gestão, Supremo Tribunal Federal. Prestação de Contas Anual, Exercício 2012.</p>
						<p>Relatório de Auditoria de Gestão, Supremo Tribunal Federal. Prestação de Contas Anual, Exercício 2013.</p>
						<p>SALGADO, Sulivan D. Fischer, revisado e ampliado por VENDRAMINI, Patrícia. <i>TEORIA GERAL DA ADMINISTRAÇÃO II - AS FUNÇÕES ADMINISTRATIVAS - A Função Planejamento</i>. Universidade Estácio. Santa Catarina, Agosto de 2003. Disponível em: <a href="http://material-estacio.tripod.com/arquivos/planej_completo.pdf" target="_blank" title="acesse a referência">http://material-estacio.tripod.com/arquivos/planej_completo.pdf</a>. Acesso em 9/10/2014.</p>
						<p>Santa Catarina. Disponível em: <a href="http://material-estacio.tripod.com/arquivos/planej_completo.pdf" target="_blank" title="acesse a referência">http://material-estacio.tripod.com/arquivos/planej_completo.pdf</a>. Acesso em 9/10/2014.</p>
						<p>TORRES, Marcelo Douglas de Figueiredo. <i>Estado, democracia e administração pública no Brasil</i>. Rio de Janeiro: Editora FGV, 2004.</p>
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina12.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


