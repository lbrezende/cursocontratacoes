<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Prioridade, Urgência e Importância', 'exibir', '2','8', '13', 'aula2pagina7.php', 'aula2pagina9.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h4 class="subTitulo">2.2.2.   PRIORIDADE, URGÊNCIA E IMPORTÂNCIA</h4>
						<div class="col-lg-8 col-md-8 col-sm-8" ><p>Para tornar um planejamento efetivo, também devemos destacar as definições de prioridade, urgência e importância. No dia a dia, esses conceitos se misturam muito, mas, quando falamos em ambiente profissional corporativo, a distinção entre eles é essencial.</p></div>
						<div class="col-lg-4 col-md-4 col-sm-4"><img width="150px" height="139px" alt="" src="../include/img/aulas/PUI.png" /></div>
						<div class="clear"></div>
						<p>O conceito de <span class="semi-bold">prioridade</span> é simples e está associado à clareza de ações que devem ser executadas segundo um <span class="semi-bold">ordenamento</span>, ou seja, uma fila, previamente definida. É saber o que será feito em primeiro lugar ou, ainda, é a “condição do que é o primeiro” (ATENDIMENTO FANTÁSTICO, 2014).</p>
						<p>A noção de prioridade irá depender da gestão exercida em cada órgão ou entidade. O que é prioritário em uma instituição poderá não ser em outra em um dado momento. Por exemplo, para um tribunal da Justiça Eleitoral, várias demandas deverão ter suas execuções priorizadas em um ano eleitoral. Essa medida serve para ordenar as atividades necessárias à perfeita coleta dos votos no dia do pleito.</p>
						<p>A <span class="semi-bold">urgência</span> está ligada ao <span class="semi-bold">tempo</span> de execução das tarefas. Normalmente está associada a ações que, se não executadas, podem causar impactos relevantes ao órgão. Urgente é o “que é necessário ser atendido ou feito com rapidez; que não pode ser retardado”. A urgência gera alterações na fila de prioridades.</p>
						<p>Como exemplo, imagine um tribunal que, após inaugurar fórum, teve vários equipamentos danificados por variações de energia elétrica. Nesse caso, a aquisição de equipamentos estabilizadores e de proteção de rede elétrica interna é um exemplo de projeto urgente a ser trabalhado.</p>
						<p>O conceito de <span class="semi-bold">importância</span> refere-se à sensibilidade de medir os impactos das ações para o órgão. Está diretamente relacionada com os <span class="semi-bold">níveis de riscos</span> envolvidos para a organização. </p>
						<p>Particularmente, dada a importância e a complexidade que possui, a contratação de serviços terceirizados deverá receber atenção especial do órgão demandante, que deverá planejá-la criteriosamente, identificando e gerindo os riscos envolvidos nesse tipo de contratação.</p>
						<p>Enquanto a “urgência” impacta a fila de prioridade devido ao tempo, a “importância” impacta a prioridade devido aos riscos envolvidos e às consequências danosas dos problemas que esses riscos podem acarretar.</p>
						<p>A classificação das demandas a serem trabalhadas pelo gestor público é uma boa prática nas contratações. Cada gestor deve analisar a demanda e classificá-la de acordo com as necessidades de seu órgão.</p>
						<p>Abaixo, temos um mapa de priorização, que poderá auxiliar os gestores na definição e classificação das demandas a serem executadas.</p>
						<p class="textAlignCenter"><img src="../include/img/aulas/graficoTela8.png" /> </p>
						<p>A numeração indica a ordem de prioridade na execução das demandas. Uma necessidade classificada no quadrante de número 1 deverá receber atenção especial e ser atendida em primeiro lugar, tendo em vista que possui alto grau de urgência e de importância.</p>
						</div>
					</div>   
				 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina7.php', 'aula2pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


