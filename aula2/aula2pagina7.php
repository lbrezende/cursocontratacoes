<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Planejamento Efetivo', 'exibir', '2','7', '13', 'aula2pagina6.php', 'aula2pagina8.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h4 class="subTitulo">2.2.2.   EFICÁCIA</h4>
						<p>Um bom planejamento também deve buscar a <span class="semi-bold">eficácia</span>, que está relacionada, resumidamente, ao cumprimento das ações priorizadas. É a capacidade de fazer as coisas “certas” com foco em resultados.</p>
						<p>Para o professor Idalberto Chiavenato (1994, p. 70):</p>
						<div class="col-lg-2 col-md-2 col-sm-2  textAlignRight">
							<img src="../include/img/icons/eficacia.png" alt="Eficácia" width="55" height="82"/>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">(...)<span class="semi-bold"> Eficácia</span> é uma medida normativa do alcance dos <span class="semi-bold">resultados</span>. (...) Quando o administrador verifica se as coisas bem feitas (eficiência) são as que realmente deveriam ser feitas, então ele está se voltando para a eficácia (alcance dos objetivos através dos recursos disponíveis).</p>
						</div>
						<div class="clear"></div>
						<p>Para BIO (1996, p. 20 – 23), “eficácia diz respeito a resultados, a produtos decorrentes de uma atividade qualquer. Trata-se da escolha da solução certa para determinado problema ou necessidade. (...) Uma empresa eficaz coloca no mercado o volume pretendido do produto certo para determinada necessidade”.</p>
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>Um órgão será eficaz em um certame licitatório quando, por exemplo, atingir plenamente os objetivos e resultados pretendidos com a licitação, ou seja, o objeto adquirido supre a demanda conforme especificado e esperado.</p>
							<p>Podemos pensar especificamente em uma demanda para montagem de tendas para a execução de um evento em certa data. Se os prazos são atendidos, os custos reduzidos e a montagem é concluída no prazo previsto pela Administração, com o esmero superior ao aguardado, houve eficiência nessa demanda. </p>
							<p>Porém, se apesar de executar o serviço conforme acima, a empresa monta as tendas em local distinto do especificado pela administração, faltou eficácia, tanto da empresa, que não observou atentamente o local indicado, como da administração, que não fiscalizou adequadamente a execução do objeto.</p>
						</div>
						<h4 class="subTitulo">2.2.3.   EFETIVIDADE</h4>
						<p>Outro parâmetro de qualidade a ser alcançado com o uso do planejamento de uma demanda é a <span class="semi-bold">efetividade</span>. Trata-se de conceito mais amplo, moderno, complexo e especialmente válido para a Administração Pública, considerando-se que afere em que medida os resultados de uma ação trazem benefício à população (CASTRO, 2006). </p>
						<p>A efetividade é abordada com precisão por Torres (2004, p.175), para quem essa é direcionada para a qualidade do resultado em prol do interesse público:</p>
						<div class="col-lg-2 col-md-2 col-sm-2  textAlignRight">
							<img src="../include/img/icons/alvo.png" alt="Eficácia" width="55" height="82"/>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">Efetividade: é o mais complexo dos três conceitos, em que a preocupação central é averiguar a real necessidade e oportunidade de determinadas ações estatais, deixando claro que setores são beneficiados e em detrimento de que outros atores sociais. Essa averiguação da necessidade e oportunidade deve ser a mais democrática, transparente e responsável possível, buscando sintonizar e sensibilizar a população para a implementação das políticas públicas. Este conceito não se relaciona estritamente com a idéia de eficiência, que tem uma conotação econômica muito forte, haja vista que nada mais impróprio para a administração pública do que fazer com eficiência o que simplesmente não precisa ser feito.</p>
						</div>
						<div class="clear"></div>
						<h4 class="subTitulo">2.2.4.   PRINCÍPIO CONSTITUCIONAL DA EFICIÊNCIA</h4>
						<p>Outro ponto importante, não só para o planejamento, mas para toda a administração pública brasileira, foi a introdução do Princípio da Eficiência na <a href="http://www.planalto.gov.br/ccivil_03/constituicao/constituicaocompilado.htm" target="_blank" title="clique aqui para ver a Constituição Federal">Constituição Federal</a><a href="javascript:void(0);" rel="popover" data-content="<p>Constituição Federal - Art. 37. A administração pública direta e indireta de qualquer dos Poderes da União, dos Estados, do Distrito Federal e dos Municípios obedecerá aos princípios de legalidade, impessoalidade, moralidade, publicidade e eficiência e, também, ao seguinte(...)</p>" data-toggle="popover" data-size="popover-small"><sup>3</sup></a>, por meio da <a href="http://www.planalto.gov.br/ccivil_03/constituicao/Emendas/Emc/emc19.htm" target="_blank" title="clique aqui para ver a EC 19">Emenda Constitucional 19</a>, que representou um marco para as reformas gerenciais no Brasil (CASTRO, 2006).</p>
						<p>Acerca desse Princípio, <a id="modal" title="informações de Idalberto Chiavenato" href="javascript:void(0);" style="padding-left:0px;" class="btn btn-primary btn-small" data-toggle="modal" data-target="#myModal">MEIRELLES <sup><img src="../include/img/icons/plus.jpg" width="10px" height="10px" alt="clique aqui para saber mais sobre Hely Lopes Meirelles" style="margin-left:5px;" /></sup></a> (2002, p.94) ensina:</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">O Princípio da Eficiência exige que a <span class="semi-bold">atividade administrativa seja exercida com presteza, perfeição e rendimento funcional</span>. É o mais moderno princípio da função administrativa, que já não se contenta em ser desempenhada apenas com legalidade, <span class="semi-bold">exigindo resultados positivos para o serviço público</span> e satisfatório atendimento das necessidades da comunidade e seus membros. (Grifamos).</p>
						</div>
						<div class="clear"></div>
						<p>Em que pese alguma discussão doutrinária crítica acerca dos efeitos práticos desse princípio<a href="javascript:void(0);" rel="popover" data-content="<p>   Alguns doutrinadores questionam a necessidade da Constituição Federal trazer expressamente o Princípio da Eficiência, por entenderem que trata-se de uma obrigação natural do gestor público ser eficiente em suas ações. Rodrigo Batista de Castro no artigo Eficácia, Eficiência e Efetividade na Administração Pùblica, 30º Encontro da ANPAD, traz a crítica de alguns autores.</p>" data-toggle="popover" data-size="popover-small"><sup>4</sup></a> , trata-se de relevante instituto inserido no ordenamento jurídico brasileiro, que insta o gestor público a agir no sentido de dar efetivas respostas à população, em relação à adequada aplicação dos recursos públicos.</p>
                    </div>
                  </div>   
                 </div>
				</div>
            </article>
<!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <br>
              <i class="icon-credit-card icon-7x"></i>
              <h4 id="myModalLabel" class="semi-bold">Hely Lopes Meirelles</h4>
             
            </div>
            <div class="modal-body">
              <div class="row form-row" style="text-align:center;">
                <div class="row">

                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 "><img src="../include/img/aulas/helyMeirelles.png" alt="" style="width:133px"><br><br></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <p class="no-margin">Hely Lopes Meirelles foi um jurista brasileiro reconhecido como um dos principais doutrinadores do Direito Administrativo e do Direito Municipal.</p>
              <br></div>
                </div>
                  
              </div>
            </div>
            <div class="modal-footer">
              <a class="btn btn-default" data-dismiss="modal">Voltar para a aula</a>
              <a href="https://pt.wikipedia.org/wiki/Hely_Lopes_Meirelles"  target="_blank" class="btn btn-primary">Ver currículo de Hely Lopes Meirelles</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->					
<?php  configNavegacaoRodape('exibir', 'aula2pagina6.php', 'aula2pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


