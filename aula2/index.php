<?php   
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
 if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

  $acessibilidadeTxt = null;
  if ($acessibilidade == "sim") { 
    $acessibilidadeTxt = "?ac=sim";
  }; 

configHeader('Bem-vindo', 'exibir', '2','1', '13', 'index.php', 'aula2pagina2.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<p class="semi-bold">Caros colegas,</p>
							<p>Sejam bem-vindos à segunda aula do curso “Planejamento das Contratações: buscando a gestão efetiva dos gastos públicos”. Vamos iniciar hoje com o interessante vídeo “Planejamento é a solução de todos os problemas”. O vídeo abaixo destaca que a ação de planejar pode mudar o futuro e gerar alegria. </p>
							<div class="col-lg-3 col-md-3 col-sm-3"></div>
							<div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/rmZuR6D4f3I" frameborder="0" allowfullscreen></iframe>
                                </div><br><br>
                              </div>
							<div class="col-lg-3 col-md-3 col-sm-3"></div>
							<div class="clear"></div>
							<p>E aí, gostaram? Nada como se preparar com antecedência! Façamos nossas reflexões...</p>
							<p>Vamos iniciar?</p>
							  
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    

            <footer>  
              <nav role="navigation">
                <div class="textAlignCenter">
                  <a href=<?php echo '"aula2pagina2.php'.$acessibilidadeTxt.'"';?> class="btn btn-lg btn-success" title="Ir para próxima página">Avan&ccedil;ar <i class="fa fa-arrow-circle-o-right">  </i></a>
                </div>
              </nav>
            </footer>  
          <!-- <?php  //configNavegacaoRodape('exibir', 'index.php', 'aula2pagina3.php'); //Rodapé automático aqui  ?>-->         
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


