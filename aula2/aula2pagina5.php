<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Otimizando a Demanda – Noções sobre Planejamento', 'exibir', '2','5', '13', 'aula2pagina4.php', 'aula2pagina6.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 
			<article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<p style="margin-top:10px;">Várias são as razões que justificam a ação de planejar. De acordo com a Profª Sulivan, 2003:</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">A segunda razão para o planejamento é aumentar as possibilidades de tomar melhores decisões hoje, para ajudar a conseguir melhor desempenho no futuro. Para ajudar uma organização e seus administradores a antever o futuro a fim de permanecer funcionando e prosperar em um mundo mutável, deve haver um planejamento antecipado que seja ativo, vigoroso, contínuo e criativo, isto é, um planejamento proativo. A administração que não faz planejamento proativo será somente reativa – respondendo ao ambiente atual e não sendo uma participante ativa do mundo competitivo.</p>
							<p class="fonteMenor">O Planejamento é a função inicial da administração e deve ser vista como a locomotiva que puxa o trem das ações de organizar, liderar e controlar. Ou talvez devêssemos pensar no planejamento como a raiz principal de uma magnífica árvore, da qual saem os ramos das demais funções.</p>
							<p class="fonteMenor">Sem planos, os administradores não podem saber como devem organizar as pessoas e os recursos; podem até mesmo não ter uma ideia clara sobre o que precisam organizar. Sem um plano, não podem liderar com confiança ou esperar que os outros o sigam. E sem um plano, os administradores e seus seguidores têm pouca chance de alcançar seus objetivos ou de saber quando e onde saíram do caminho. Frequentemente, planos falhos afetam a saúde de toda a organização.</p>
						</div>
						<div class="clear"></div>
						<p>Conforme a amplitude do objetivo a ser alcançado, o grau de complexidade do planejamento irá variar proporcionalmente. Em regra, um bom planejamento, para ser útil, deverá possuir as características abaixo:</p>
						<div class="paddingBottom300">
							<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-0">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Escopo definido</span>: Os planos eficazes devem ter uma extensão adequada e refletir amplitude, unidade e coerência. Para que o objetivo seja atingido, é essencial a correta delimitação do que deve ser feito, de modo a reduzir esforços desnecessários em ações que não estejam direcionadas ao alcance do objetivo proposto.</p>
								<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
								<div id="caixa-1">
									<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Flexibilidade</span> – Um planejamento bem sucedido é o resultado de análises e previsões cuidadosas, que devem ser acompanhadas continuamente. O plano deve ser, portanto, capaz de sofrer uma adaptação rápida e suave a novas condições, sem muita perda de eficiência.</p>
									<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
									<div id="caixa-2">
										<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Estabilidade</span> – Esse é um fator que dá credibilidade ao plano. Se os planos mudam com muita frequência, os administradores não se familiarizam com eles como instrumento operacional e não os usam efetivamente. Um plano estável é aquele que não tem de ser abandonado ou modificado amplamente por causa das mudanças no ambiente organizacional.</p>
										<p class="abreCaixa" id="3"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
										<div id="caixa-3">
											<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold">Simplicidade</span> – Algumas vezes, os planos podem ser mais complicados do que o necessário. Se forem complexos, serão mais difíceis de implementar e de controlar. Assim, um plano simples ajuda na realização de seus objetivos, de forma a minimizar a possibilidade de complicações.</p>
											<p class="abreCaixa" id="4"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
											<div id="caixa-4">
												<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold"> Contingenciamento<a href="javascript:void(0);" rel="popover" data-content="<p>Contingenciamento no sentido de descrever as medidas a serem tomadas, caso o plano principal sofra impactos que prejudiquem o seu curso regular. São medidas alternativas que podem ser utilizadas em caso de necessidade.</p>" data-toggle="popover" data-size="popover-small"><sup>1</sup></a></span>  – Todo ambiente organizacional pode sofrer mudanças. Desse modo, a inclusão de planos contingenciais deve ser avaliada, de modo a proporcionar alternativas caso o plano principal apresente problemas na execução.</p>
												<p class="abreCaixa" id="5"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
												<div id="caixa-5">
													<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold"> Precisão e objetividade</span>: os planos devem buscar a clareza, a concisão e a exatidão. O planejamento deve se basear em um pensamento realista e concreto sobre os requisitos necessários para alcançar objetivos organizacionais, e não objetivos individuais dos responsáveis pelo planejamento.</p>
													<p class="abreCaixa" id="6"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
													<div id="caixa-6">
														<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> <span class="semi-bold"> Senso de oportunidade</span>: É difícil para os planejadores preverem com precisão os eventos futuros. Isso decorre do grande número e variedade de eventos que podem surgir e sobre os quais a administração não possui muito controle. Apesar dessa questão, os riscos aos objetivos<a href="javascript:void(0);" rel="popover" data-content="<p>Os riscos aos objetivos são situações que podem ocorrer desviando ou inviabilizando o alcance de metas e objetivos. Serão tratados com mais detalhes na sequência deste treinamento.</p>" data-toggle="popover" data-size="popover-small"><sup>2</sup></a> sempre deverão ser considerados. É importante também avaliar se o momento é adequado para a apresentação de uma demanda ou projeto. </p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<p>Entenda-se que o momento oportuno deve ser analisado como aquele mais favorável à tramitação processual. Iniciar uma demanda próximo a um período de troca de gestão do órgão ou a um período de recesso de servidores pode gerar impactos no prazo de tramitação processual.</p>
						<p>Esse aumento no prazo pode ocorrer em decorrência de alterações na estrutura administrativa do órgão, por exemplo, devido à troca de pessoas ou à diminuição do quadro funcional nesse período.</p>
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>Algumas vezes, é melhor aguardar mais um pouco, revisar e reforçar pontos do planejamento antes de iniciar a demanda.</p>
						</div>
						
                    </div>
                  </div>   
                </div>
				</div>
            </article>    <?php  configNavegacaoRodape('exibir', 'aula2pagina4.php', 'aula2pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


