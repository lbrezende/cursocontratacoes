<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Ações de Planejamento no Nosso Curso', 'exibir', '2','11', '13', 'aula2pagina10.php', 'aula2pagina12.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<p>Contratamos bem, contudo, é importante melhorar. Conforme destacado na Aula 1, somente uma pequena parte dos procedimentos licitatórios realizados em 2012 e 2013 não obtiveram êxito nas contratações<a href="javascript:void(0);" rel="popover" data-content="<p>Em 2012, apenas 4,23% dos 189 procedimentos licitatórios realizados no Supremo Tribunal Federal não obtiveram êxito. Já em 2013, o quantitativo foi de 9,41% de 170 procedimentos licitatórios. </p>" data-toggle="popover" data-size="popover-small"><sup>11</sup></a>.</p>
						<p>Reiterando, o <span class="semi-bold">planejamento prévio</span><a href="javascript:void(0);" rel="popover" data-content="<p>Acórdão 2080/2007 e Acórdão 2387/2007 ambos do Plenário TCU. “Realize o planejamento prévio dos gastos anuais, de modo a evitar o fracionamento de despesas de mesma natureza, observando que o valor limite para as modalidades licitatórias e cumulativo ao longo do exercício financeiro, a fim de não extrapolar os limites estabelecidos nos artigos 23, § 2°, e 24, inciso II, da Lei no 8.666/1993. Adote a modalidade adequada de acordo com os arts. 23 e 24 da Lei no 8.666/1993, c/c o art. 57, inciso II, da Lei no 8.666/1993, de modo a evitar que a eventual prorrogação do contrato administrativo dela decorrente resulte em valor total superior ao permitido para a modalidade utilizada, tendo em vista a jurisprudência do Tribunal (Vide também Acórdãos 842/2002 e 1725/2003, da Primeira Câmara e Acórdãos 260/2002, 1521/2003, 1808/2004 e 1878/2004, do Plenário).”. </p>" data-toggle="popover" data-size="popover-small"><sup>12</sup></a> é essencial na hora da elaboração de um Projeto básico ou Termo de referência para instrução de autos de processos que objetivem a aquisição de bens, produtos ou serviços, com ou sem licitação.</p>
						<p>Embora pareça redundância, o termo planejamento prévio utilizado pelo Tribunal de Contas da União - TCU - objetiva reforçar a ideia de que o planejamento deve ser anterior à execução da demanda. O que se pode fazer, ao longo das fases interna e externa dos procedimentos da licitação, conforme o caso, está restrito a ajustes pontuais para manutenção do curso correto de cada demanda, com vistas ao pleno alcance do objetivo desejado.</p>
						<p>Se o objeto da licitação está dividido em itens em um único processo licitatório, o objetivo e o sucesso do certame estarão concretizados na adjudicação - conforme as condições definidas no edital do certame - de todos os itens licitados às empresas vencedoras. Nesse caso, o pleno alcance do objetivo da licitação está no sucesso da contratação de todos os itens, sem que nenhum deles tenha sido deserto<a href="javascript:void(0);" rel="popover" data-content="<p>De acordo com a Profª. Maria Sylvia Zanella Di Pietro na licitação deserta ninguém chegou a apresentar documentação para participar da licitação.</p>" data-toggle="popover" data-size="popover-small"><sup>13</sup></a> ou fracassado<a href="javascript:void(0);" rel="popover" data-content="<p>Conforme Maria Sylvia Zanella Di Pietro, na Licitação fracassada houve manifestação de interesse, de modo que foram apresentadas propostas. Porem, todas essas propostas foram inabilitadas ou desclassificadas, de modo que não restou uma única proposta na licitação que pudesse ser aproveitada pela Administração.</p>" data-toggle="popover" data-size="popover-small"><sup>14</sup></a>, etc. e tenha que ser repetido em outro procedimento licitatório.</p>
						<p>Acerca do Processo de planejamento da contratação de soluções de TI, o <a href="../include/file/boas_praticas_contratacoesTI_TCU.pdf" target="_blank" tilte="acesse o guia de boas práticas de contratação de TI do TCU">Guia de boas práticas em contratação de soluções de Tecnologia da Informação: riscos e controles para o planejamento da contratação, do TCU (2012, p. 51)</a>, (colocar link para arquivo que vou passar) trata o planejamento em termos conceituais como um projeto sistematizado que se inicia, possui o transcurso temporal e termina com a consecução do objetivo proposto, vejamos: </p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">O planejamento da contratação de uma solução de TI, em termos conceituais, é um projeto, pois tem início, meio e fim. Entretanto, cada vez que uma contratação é realizada, o planejamento deve seguir essencialmente os mesmos passos, de maneira que haja previsibilidade com relação à execução e se garanta a aderência à legislação e à jurisprudência. Ou seja, cada contratação de solução de TI deve seguir um mesmo processo de trabalho.</p>
							<p class="fonteMenor">Para garantir que o processo de trabalho de planejamento da contratação de soluções de TI seja seguido de forma padronizada, torna-se necessária a sua formalização, divulgação e capacitação dos servidores envolvidos. Esse processo de trabalho deve ser publicado após sua aprovação pela alta administração do órgão.</p>
						</div>
						<div class="clear"></div>
						<p>Nesse contexto, o Guia sugere, na página 53, que, no planejamento da contratação, sejam produzidos, pelo menos, os seguintes Artefatos, conforme <a href="http://www.planalto.gov.br/ccivil_03/leis/L8666compilado.htm" target="_blank" title="link para lei 8666">Lei 8.666/1993</a> e o <a href="http://www010.dataprev.gov.br/sislex/paginas/23/1997/2271.htm" target="_blank" title="link para Decreto 2271">Decreto 2.271/1997</a>:</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor"><span class="semi-bold">a) estudos técnicos preliminares</span> (Lei 8.666/1993, art. 6º, inciso IX 22), que constituem a etapa preliminar e obrigatória do planejamento da contratação. Objetiva assegurar a viabilidade técnica da contratação, e o tratamento de seu impacto ambiental (sustentabilidade), além de fortalecer o termo de referência ou o projeto básico, que somente é elaborado se a contratação for considerada viável, bem como o plano de trabalho, no caso de serviços, de acordo com exigência do Decreto 2.271/1997, art. 2º<a href="javascript:void(0);" rel="popover" data-content="<p>Decreto 2.271/1997 Art. 2º A contratação deverá ser precedida e instruída com plano de trabalho aprovado, pela autoridade máxima do órgão ou entidade, ou a quem esta delegar competência, e que conterá, no mínimo: I - justificativa da necessidade dos serviços; II - relação entre a demanda prevista e a quantidade de serviço a ser contratada; III - demonstrativo de resultados a serem alcançados em termos de economicidade e de melhor aproveitamento dos recursos humanos, materiais ou financeiros disponíveis.</p>" data-toggle="popover" data-size="popover-small"><sup>15</sup></a>;</p>
							<p class="fonteMenor"><span class="semi-bold">b) plano de trabalho, no caso da contratação de serviços</span> (Decreto 2.271/1997, art. 2º ). O plano de trabalho favorece a efetiva governança das contratações de TI, pois é aprovado pela autoridade máxima do órgão ou a quem ela delegar essa competência (e.g. Comitê Diretivo de TI).;</p>
							<p class="fonteMenor"><span class="semi-bold">c)     termo de referência ou projeto básico</span> (Lei 8.666/1993, art. 7º, inciso I, § 2º, inciso I, §§ 6º e 9º 24). Possui elementos necessários e suficientes caracterizadores do objeto. É subsequente à aprovação dos estudos técnicos preliminares, que indicaram formalmente a viabilidade da contratação. Sem ele nenhuma contratação deverá ser realizada, ainda que seja contratação direta por dispensa ou inexigibilidade de licitação (Lei 8.666/1993, art. 7º, inciso I, § 2º, inciso I, §§ 6º e 9º ). Também deve ser elaborado para contratação de empresa pública, ou adesão a uma ata de registro de preço, conforme IN - SLTI 4/2010, art. 18.
						</div>
						<div class="clear"></div>
						<p>Ainda, de acordo com o Guia de boas práticas em contratação de soluções de Tecnologia da Informação do TCU, na página 56, os principais itens que esses artefatos devem conter estão destacados abaixo:</p>
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
							  <h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								  Estudos técnicos preliminares
								</a>
							  </h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							  <div class="panel-body">
								<p><br />
								1.	 Necessidade da contratação.
								<br />2.	 Alinhamento entre a contratação de TI e os planos do órgão governante superior.
								<br />3.	 Requisitos da contratação.
								<br />4.	 Relação entre a demanda prevista e a quantidade de cada item.
								<br />5.	 Levantamento de características do mercado. 
								<br />6.	 Justificativas da escolha do tipo de solução a contratar.
								<br />7.	 Estimativas preliminares dos preços.
								<br />8.	 Descrição da solução de TI como um todo.
								<br />9.	 Justificativas para o parcelamento ou não da solução.
								<br />10.	 Resultados pretendidos.
								<br />11.	 Providências para a adequação do ambiente do órgão.
								<br />12.	 Análise de risco.
								<br />13.	 Declaração da viabilidade ou não da contratação.
								</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								  Plano de trabalho (no caso de contratação de serviços)
								</a>
							  </h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							  <div class="panel-body">
								<p><br />
								1.	Necessidade da solução de TI.
								<br />2.	Relação entre a demanda prevista e a quantidade de cada item.
								<br />3.	Demonstrativo de resultados a serem alcançados em termos de economicidade e de melhor aproveitamento dos recursos humanos, materiais ou financeiros disponíveis.
								</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								  Termo de referência ou projeto básico
								</a>
							  </h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							  <div class="panel-body">
								<p><br />
								1.	Definição do objeto.
								<br />2.	Fundamentação da contratação.
								<br />3.	Descrição da solução de TI.
								<br />4.	Requisitos da contratação.
								<br />5.	Modelo de execução do objeto.
								<br />6.	Modelo de gestão do contrato.
								<br />7.	Forma (procedimento) de seleção do fornecedor.
								<br />8.	Critérios de seleção (avaliação) do fornecedor. 
								<br />9.	 Estimativas dos preços. 
								<br />10.	 Adequação orçamentária.
								</p>
							  </div>
							</div>
						  </div>
						</div>
						<p>Embora o Guia oriente as contratações de TI, seu conteúdo é amplamente difundido para as demais áreas, no que é cabível, tendo em vista as boas práticas de planejamento e gestão que nele são expostas.</p>
						<p>Conforme observamos, o planejamento precisa ser programado para que o trabalho possa surtir os efeitos desejados em termos de organização da demanda, com vistas a dar celeridade ao processo administrativo.</p>
						<p>Planejar, na Administração Pública, exige prática. Não é difícil, porém as ações requerem a devida atenção, sistematização e foco nos objetivos definidos.</p>
						<p>O grau de dificuldade vai depender da complexidade do objeto demandado. À medida que evoluímos no planejamento de nossas demandas, as atividades de planejamento se tornam mais fáceis, tendo em vista que passam a ser absorvidas e incorporadas ao pensamento e aos valores das pessoas ou grupos componentes da Administração Pública.</p>
					  </div>
					</div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina10.php', 'aula2pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


