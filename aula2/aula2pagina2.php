<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Objetivos desta aula', 'exibir', '2','2', '13', 'index.php', 'aula2pagina3.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">Objetivos desta aula</h3>
						<div class="col-lg-6 col-md-6 col-sm-6 ">
							<p>Prezados participantes,</p>
							<p>Nesta segunda aula, estudaremos uma das mais importantes ferramentas de sucesso para as contratações públicas: o <span class="semi-bold">planejamento</span>.  </p>						
							<p>Ao final desta aula, o participante deverá ser capaz de:</p>

							<div class="paddingBottom300">
								<p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
								<div id="caixa-0">
									<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> compreender a importância do planejamento como ferramenta aplicada individualmente e no trabalho em equipe;</p>
									<p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
									<div id="caixa-1">
										<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> identificar ações de planejamento em sua rotina diária;</p>
										<p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
										<div id="caixa-2">
											<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> conhecer conceitualmente o planejamento, suas etapas e importância;</p>
											<p class="abreCaixa" id="3"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
											<div id="caixa-3">
												<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> perceber que um planejamento efetivo dá celeridade ao processo e melhora a qualidade da instrução processual.</p>
											</div>
										</div>
									</div>
								</div>
							</div>						
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
							<img src="../include/img/aulas/mapaMentalAula2Tela2.png" alt="" class="imgAulas" />
						</div>
						<div class="clear espacamentoLista"></div>
						
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'index.php', 'aula2pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


