<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Ações de Planejamento no Nosso Curso', 'exibir', '2','10', '13', 'aula2pagina9.php', 'aula2pagina11.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">4.  AÇÕES DE PLANEJAMENTO NO NOSSO CURSO</h3>
						<p>No ponto que nos interessa, o planejamento é focado em auxiliar a Administração Pública a melhorar qualitativamente seus processos. Mas como isso é possível?</p>
						<p>Nas contratações públicas, o planejamento inicial deve ser elaborado a partir do estudo e da definição da real necessidade do órgão, considerando-se todos os aspectos técnicos relevantes ao objeto, além dos aspectos legais e jurídicos afetos ao produto/serviço que se deseja contratar. </p>
						<p>O planejamento inicial revelará se a contratação está dentro das prioridades do órgão, se há orçamento previsto para a despesa<a href="javascript:void(0);" rel="popover" data-content="<p>Constituição Federal Brasileira Art. 165. Leis de iniciativa do Poder Executivo estabelecerão: (...) § 8º - A lei orçamentária anual não conterá dispositivo estranho à previsão da receita e à fixação da despesa, não se incluindo na proibição a autorização para abertura de créditos suplementares e contratação de operações de crédito, ainda que por antecipação de receita, nos termos da lei.</p>" data-toggle="popover" data-size="popover-small"><sup>6</sup></a> e se há alinhamento com os objetivos estratégicos<a href="javascript:void(0);" rel="popover" data-content="<p>Resumidamente, objetivos estratégicos são os resultados que a organização pretende atingir.</p>" data-toggle="popover" data-size="popover-small"><sup>7</sup></a>  e de governança do órgão<a href="javascript:void(0);" rel="popover" data-content="<p>Governança constitui um sistema/processo de geração de valor. Trataremos desse assunto na próxima aula.</p>" data-toggle="popover" data-size="popover-small"><sup>8</sup></a> . </p>
						<p>O planejamento elaborado pela área técnica deverá responder às seguintes perguntas:</p>
						<div class="paddingBottom300">
							<p class="abreCaixa" id="0">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
							<div id="caixa-0">
								<div class="col-lg-8 col-md-8 col-sm-8" >
									<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />O que será adquirido/contratado (características, quantidades, peculiaridades, etc.)?</p>
									<hr class="linhaInterrogacao" />
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
								<div class="clear"></div>
								<p>Todos os detalhes relevantes acerca do objeto da demanda deverão ser registrados no processo administrativo.</p>
								<p class="abreCaixa" id="1">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
								<div id="caixa-1">
									<div class="col-lg-8 col-md-8 col-sm-8" >
										<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />O objeto demandado está relacionado aos objetivos estratégicos da organização?</p>
										<hr class="linhaInterrogacao" />
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
									<div class="clear"></div>
									<p>É importante destacar o ponto de alinhamento da demanda com o planejamento institucional do órgão, procurando demostrar os projetos/áreas que serão beneficiados com a execução da demanda.</p>
									<p class="abreCaixa" id="2">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
									<div id="caixa-2">
										<div class="col-lg-8 col-md-8 col-sm-8" >
											<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Qual o preço de mercado do objeto da demanda?</p>
											<hr class="linhaInterrogacao" />
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
										<div class="clear"></div>
										<p>Os procedimentos atinentes à avaliação e registro dos preços estimados para a contratação deverão ser efetuados sempre com atenção, cuidado e parcimônia. O que se quer é a definição de um “preço aceitável, considerado como sendo aquele que não representa claro viés em relação ao contexto do mercado, ou seja, abaixo do limite inferior ou acima do maior valor constante da faixa identificada para o produto ou serviço”<a href="javascript:void(0);" rel="popover" data-content="<p>Brasil. Tribunal de Contas da União. Guia de boas práticas em contratação de soluções de tecnologia da informação, p. 189. “Vale destacar os itens 32 e 33 do voto do Ministro-Relator do Acórdão 2.170/2007-TCU-Plenário com relação aos conceitos de “preço aceitável” e “cesta de preços”: 32. Esclareço que preço aceitável é aquele que não representa claro viés em relação ao contexto do mercado, ou seja, abaixo do limite inferior ou acima do maior valor constante da faixa identificada para o produto (ou serviço). Tal consideração leva à conclusão de que as estimativas de preços prévias às licitações, os valores a serem aceitos pelos gestores antes da adjudicação dos objetos dos certames licitatórios, bem como na contratação e posteriores alterações, por meio de aditivos, e mesmo os parâmetros utilizados pelos órgãos de controle para caracterizar sobrepreço ou superfaturamento em contratações de TI devem estar baseados em uma “cesta de preços aceitáveis”. A velocidade das mudanças tecnológicas do setor exige esse cuidado especial. 33. Esse conjunto de preços ao qual me referi como “cesta de preços aceitáveis” pode ser oriundo, por exemplo, de pesquisas junto a fornecedores, valores adjudicados em licitações de órgãos públicos – inclusos aqueles constantes no Comprasnet –, valores registrados em atas de SRP, entre outras fontes disponíveis tanto para os gestores como para os órgãos de controle – a exemplo de compras/contratações realizadas por corporações privadas em condições idênticas ou semelhantes àquelas da Administração  Pública –, desde que, com relação a qualquer das fontes utilizadas, sejam expurgados os valores que, manifestamente, não representem a realidade do mercado.”" data-toggle="popover" data-size="popover-small"><sup>9</sup></a>.</p>
										<p class="abreCaixa" id="3">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
										<div id="caixa-3">
											<div class="col-lg-8 col-md-8 col-sm-8" >
												<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Para que a despesa é necessária?</p>
												<hr class="linhaInterrogacao" />
											</div>
											<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
											<div class="clear"></div>
											<p>Consiste em informar a utilidade do gasto.</p>
											<p class="abreCaixa" id="4">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
											<div id="caixa-4">
												<div class="col-lg-8 col-md-8 col-sm-8" >
													<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Onde será utilizado o objeto?</p>
													<hr class="linhaInterrogacao" />
												</div>
												<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
												<div class="clear"></div>
												<p>Definição do destino do objeto.</p>
												<p class="abreCaixa" id="5">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
												<div id="caixa-5">
													<div class="col-lg-8 col-md-8 col-sm-8" >
														<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Quando o gasto será realizado?</p>
														<hr class="linhaInterrogacao" />
													</div>
													<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
													<div class="clear"></div>
													<p>Avaliação temporal importante para demonstrar a oportunidade da aplicação dos recursos planejados.</p>
													<p class="abreCaixa" id="6">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
													<div id="caixa-6">
														<div class="col-lg-8 col-md-8 col-sm-8" >
															<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Houve captação de demanda para essa despesa (orçamento disponível)? </p>
															<hr class="linhaInterrogacao" />
														</div>
														<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
														<div class="clear"></div>
														<p>Em regra, trata-se de informar se o gasto foi planejado e se há disponibilidade orçamentária para a despesa.</p>
														<p class="abreCaixa" id="7">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
														<div id="caixa-7">
															<div class="col-lg-8 col-md-8 col-sm-8" >
																<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Como se dará a aquisição, com ou sem licitação? </p>
																<hr class="linhaInterrogacao" />
															</div>
															<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
															<div class="clear"></div>
															<p>É importante informar aspectos que possibilitem a avaliação do enquadramento do objeto no dever constitucional de licitar, que constitui a regra para as obras, serviços, compras e alienações públicas<a href="javascript:void(0);" rel="popover" data-content="<p>Constituição Federal. Art. 37. A administração pública direta e indireta de qualquer dos Poderes da União, dos Estados, do Distrito Federal e dos Municípios obedecerá aos princípios de legalidade, impessoalidade, moralidade, publicidade e eficiência e, também, ao seguinte: (...)XXI - ressalvados os casos especificados na legislação, as obras, serviços, compras e alienações serão contratados mediante processo de licitação pública que assegure igualdade de condições a todos os concorrentes, com cláusulas que estabeleçam obrigações de pagamento, mantidas as condições efetivas da proposta, nos termos da lei, o qual somente permitirá as exigências de qualificação técnica e econômica indispensáveis à garantia do cumprimento das obrigações. (...)." data-toggle="popover" data-size="popover-small"><sup>10</sup></a>.</p>
															<p class="abreCaixa" id="8">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
															<div id="caixa-8">
																<div class="col-lg-8 col-md-8 col-sm-8" >
																	<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Quais os resultados advindos dessa demanda?</p>
																	<hr class="linhaInterrogacao" />
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
																<div class="clear"></div>
																<p>O que se espera com a despesa?</p>
																<p class="abreCaixa" id="9">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
																<div id="caixa-9">
																	<div class="col-lg-8 col-md-8 col-sm-8" >
																		<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Quais os riscos envolvidos? </p>
																		<hr class="linhaInterrogacao" />
																	</div>
																	<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
																	<div class="clear"></div>
																	<p>Os riscos atinentes à execução da demanda deverão ser avaliados.</p>
																	<p class="abreCaixa" id="10">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
																	<div id="caixa-10">
																		<div class="col-lg-8 col-md-8 col-sm-8" >
																			<p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Qual a estratégia para condução dessa demanda? </p>
																			<hr class="linhaInterrogacao" />
																		</div>
																		<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
																		<div class="clear"></div>
																		<p>É importante definir a necessidade de reuniões com áreas de apoio e controle com vistas ao alinhamento de ideias e troca de experiências que favoreçam a celeridade processual, a eliminação de retrabalhos, a otimização dos recursos disponíveis e o bom aproveitamento desses. </p>
																		</p>
																		
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						
						
						
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>No nosso caso, observamos que, de modo geral, as diversas áreas do <span class="semi-bold">STF</span> necessitam de maior ênfase em conceitos relacionados à eficiência, eficácia e efetividade de um bom <span class="semi-bold">planejamento</span> como fator determinante do sucesso de uma contratação (<i>lato sensu</i>). Um planejamento detalhado facilita o trabalho do gestor contratual, pois agrega elementos de avaliação dos resultados.</p>

						</div>
					  </div>
                    </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina9.php', 'aula2pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


