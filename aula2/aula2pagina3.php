<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Planejamento Cotidiano', 'exibir', '2','3', '13', 'aula2pagina2.php', 'aula2pagina4.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">1.  PLANEJAMENTO COTIDIANO</h3>
						<p>O que queremos dizer quando afirmamos que o planejamento é importante?</p>
						<p>Lembra o vídeo a que você assistiu agora há pouco?</p>
						<p>Pense sobre a história retratada no vídeo e o tema deste curso.</p>
						<p>Pois bem, planejar não é atividade exclusiva de adultos teóricos e aptos a dominarem o mundo. A atividade de planejar é muito mais simples e atrativa do que podemos imaginar. Planejamos tudo desde pequeninos.</p>
						<p>Nosso foco neste momento está em compreender a importância do planejamento como ferramenta aplicada individualmente e no trabalho em equipe como meio para alcançar um fim específico.</p>
						<p>As atividades de planejamento estão muito mais próximas de nossas vidas do que imaginamos.</p>
						<p>Nossas ações, em regra, necessitam de planejamento. Até as atividades rotineiras, que muitas vezes executamos no modo automático, em algum momento anterior foram objeto de planejamento, ainda que de modo não sistematizado.</p>
						<h4 class="subTitulo">1.1.  COMO O PLANEJAMENTO NOS AJUDA NO DIA A DIA</h4>
						<p>Para ampliar o assunto, vamos pensar, por exemplo, no caso de uma mudança de residência que teremos de fazer. Normalmente essa atividade envolve uma série de procedimentos que devem ser seguidos de forma ordenada: escolha do novo endereço; dia e horário da mudança, contratação da transportadora, etc.</p>
						<p>Desde a definição da opção pela mudança até a colocação da última peça da mobília em seu devido lugar, a cada etapa, temos de planejar e tomar decisões para que tudo ocorra da melhor maneira possível. Essas ações procuram minimizar a ocorrência de problemas em decorrência da persecução do objetivo a ser atingido, que é sair de um lugar com toda a família e mobília, chegando a outro, predefinido, com todos os componentes iniciais, em segurança e sem danos físicos ou psicológicos. </p>
						<p>Realmente não é fácil. O planejamento é essencial para o sucesso dessa operação.</p>
						<div class="col-lg-6 col-md-6 col-sm-6 textAlignRight">
                                <div class="video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/LOyX-vgdQGQ" frameborder="0" allowfullscreen></iframe>
                                </div><br><br>

						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
							<p>Vamos parar um pouquinho para assistir a um filme?</p>
							<p>No vídeo a seguir, você verá a engraçada história de um porquinho que tem como objetivo alcançar alguns biscoitos em cima da geladeira, mas, como não realizou o planejamento adequadamente, acaba se desgastando bastante, além de não conseguir os desejados biscoitos.  </p>
							<p> Como esse vídeo pode ser associado ao nosso cotidiano? Você já passou por alguma situação parecida, em que queria muito alguma coisa, mas não conseguiu por falta de planejamento?</p>
						</div>

						<p>Em maior ou menor escala, o <span class="semi-bold">planejamento é utilizado individualmente ou no trabalho em equipe, com o fito de maximizar e atingir objetivos desejados</span>.</p>
						<p>Assim, a atividade de planejamento visa <span class="semi-bold">facilitar</span> o alcance de <span class="semi-bold">metas</span> e de <span class="semi-bold">objetivos</span> definidos previamente.</p>
						<p>Lembra o vídeo das damas mostrado na Aula 1? Aquele vídeo ressalta uma das mais importantes características de um bom planejador: a paciência. Sim, planejar é ter paciência para estabelecer objetivos e metas, ser estratégico, definir caminhos e fazer escolhas. É ter em mente que tudo na vida pode mudar. Assim, é ilusão achar que, uma vez elaborado o planejamento, o serviço estará cumprido.</p>
						<p>A atividade de planejamento consiste em um processo no qual a flexibilidade sempre deve ser considerada. Havendo necessidade, os rumos deverão ser ajustados, mantendo-se, contudo, a atenção e o foco no objetivo proposto.</p>
						<p>Assim, o planejamento está muito mais inserido em nossas vidas do que imaginamos. Para planejar, nem sempre precisamos de uma sala silenciosa, um computador e um dia inteiro de reflexão. Muitas vezes planejamos mentalmente e com precisão algo que necessitamos executar para atingir um objetivo definido.</p>
						<p>Quando tratamos de ambiente organizacional, é claro que as proporções e as responsabilidades são ampliadas na medida do objetivo a ser atingido. Nesse momento, saímos da esfera intuitiva e passamos a planejar sistematicamente.</p>
						<p>A partir deste ponto, passaremos a pensar no planejamento como ferramenta para alavancar a qualidade da instrução processual das demandas públicas que são executadas pela Administração. Para tanto, apresentaremos conceitos importantes que auxiliarão o processo de planejamento.</p>
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula2pagina2.php', 'aula2pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


