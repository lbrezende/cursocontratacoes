<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Planejamento Efetivo', 'exibir', '2','6', '13', 'aula2pagina5.php', 'aula2pagina7.php', '<h4 style="font-weight:bold">Planejamento das Contratações:</h4><h5>buscando a gestão efetiva dos gastos públicos</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
					  <h4 class="subTitulo">2.2.	O INÍCIO DE TUDO – PLANEJAMENTO EFETIVO</h4>
						<p>Para planejar, devemos definir objetivos a serem atingidos, metas a serem cumpridas, além de compilar todas as informações disponíveis, de modo a definir o que realmente deve ser feito e em que ordem de execução os projetos serão trabalhados.</p>
						<p>Nesse contexto, vale a pena destacar os conceitos de <span class="semi-bold">eficiência, eficácia, efetividade e do Princípio Constitucional da eficiência</span>, inseridos na concepção da moderna administração pública gerencial “(...) que busca a superação do modelo burocrático de gestão, mediante a adoção de mecanismos que visam à excelência administrativa e ao foco no cidadão(...)”(CASTRO, 2006).</p>
						<h4 class="subTitulo">2.2.1.	EFICIÊNCIA</h4>
						<p>Para <a id="modal" title="informações de Idalberto Chiavenato" href="javascript:void(0);" style="padding-left:0px;" class="btn btn-primary btn-small" data-toggle="modal" data-target="#myModal">Idalberto Chiavenato <sup><img src="../include/img/icons/plus.jpg" width="10px" height="10px" alt="clique aqui para saber mais sobre Idalberto Chiavenato" style="margin-left:5px;" /></sup></a> (1994, p. 70), as organizações devem ser analisadas sob o prisma da <span class="semi-bold">eficiência</span> e <span class="semi-bold">eficácia</span>:</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">(...)<span class="semi-bold"> eficiência</span> é uma medida normativa da utilização dos recursos. (...) A eficiência é uma relação entre <span class="semi-bold">custos e benefícios</span>. Assim, a eficiência está voltada para a melhor maneira pela qual as coisas devem ser feitas ou executadas (métodos), a fim de que os recursos sejam aplicados da forma mais racional possível (...).</p>
							<p class="fonteMenor">À medida que o administrador se preocupa em fazer corretamente as coisas, ele está se voltando para a <span class="semi-bold">eficiência (melhor utilização dos recursos disponíveis)</span>. (Grifamos).</p>
						</div>
						<div class="clear"></div>
						<p>BIO (1996, p. 20 – 23) pactua com esse entendimento e ensina: “eficiência diz respeito a método, a modo certo de fazer as coisas. (...) uma empresa eficiente é aquela que consegue o seu volume de produção com o menor dispêndio possível de recursos. Portanto, ao menor custo por unidade produzida”. </p>
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>Podemos pensar que um órgão eficiente é aquele que consegue concluir um certame licitatório em um prazo curto, obedecendo a todos os preceitos legais. Reduzindo-se os prazos operacionais, em regra, os custos com a licitação também serão reduzidos. </p>
						</div>
						<p>Assim, o planejamento deve buscar a <span class="semi-bold">eficiência</span> das ações, ou seja, realizá-las da melhor forma possível, em termos de custo-benefício (Referencial básico de Governança do TCU, 2014). </p>
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>Avaliar a eficiência na Administração Pública nem sempre é tarefa fácil, tendo em vista que faltam, em muitos casos, análise, definição e coleta de parâmetros objetivos de aferição de gastos/custos e de resultados das atividades administrativas. Isso ocorre, por exemplo, porque não é simples definir e medir quanto custa para um órgão planejar e executar uma licitação, considerando-se o grande volume e a complexidade de insumos a serem utilizados nessa avaliação (por exemplo, salários de servidores envolvidos, recursos de informática, luz, papel, etc., todos em função do tempo gasto com a tramitação processual). </p>
						</div>
                    </div>
                  </div>   
                 </div>
				</div>
            </article> 
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <br>
              <i class="icon-credit-card icon-7x"></i>
              <h4 id="myModalLabel" class="semi-bold">Prof. Dr. Idalberto Chiavenato </h4>
             
            </div>
            <div class="modal-body">
              <div class="row form-row" style="text-align:center;">
                <div class="row">

                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 "><img src="../include/img/aulas/idalbertoChiavenato.jpg" alt="" style="width:133px"><br><br></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <p class="no-margin">Possui Pós-doutorado em Administração e é autor de mais de 30 livros na área, atualmente exerce o cargo de Conselheiro no CRA-SP e de Presidente do Instituto Chiavenato de Educação.</p>
              <br></div>
                </div>
                  
              </div>
            </div>
            <div class="modal-footer">
              <a class="btn btn-default" data-dismiss="modal">Voltar para a aula</a>
              <a href="http://chiavenato.com/institucional/quem-e-idalberto-chiavenato.html"  target="_blank" class="btn btn-primary">Ver currículo de Idalberto Chiavenato</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->			
<?php  configNavegacaoRodape('exibir', 'aula2pagina5.php', 'aula2pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


