<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Processo Global de Avaliação de Riscos', 'exibir', '4','9', '20', 'aula4pagina8.php', 'aula4pagina10.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p>Relembrando, um <span class="semi-bold">evento</span> é uma ocorrência ou alteração em um conjunto específico de circunstâncias. Ele pode ter várias causas.</p>
                <p>As <span class="semi-bold">causas</span> do risco são as condições que dão origem à possibilidade de um evento acontecer. Causas também são chamadas fatores de riscos e podem ter origem no ambiente externo ou interno à organização.</p>
                <p class="subTitulo textAlignCenter">Causa = fonte + vulnerabilidade</p>
                <p><span class="semi-bold">Vulnerabilidades</span> são fragilidades, inadequações ou deficiências em uma fonte de risco. Ponto fraco que venha a expor um ativo crítico a um possível ataque de uma ameaça. </p>
                <p>Exemplos: procedimento inadequado de transporte de equipamentos, instrução processual ineficiente, sistema de controle de acessos deficiente, ausência de recursos de combate a incêndio, acesso a sistemas flexíveis , desconhecimento de normas e procedimentos, falta de treinamento em atividades críticas, entre outros (Glossário de Riscos, BACEN).</p>
                <p>A seguir, exemplificamos algumas causas de riscos associadas à respectiva fonte geradora do risco (NETO, et al. 2014). Clique nos ícones para ver alguns exemplos:</p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading1">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                      <img src="../include/img/icons/aula4IconHeading1.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Pessoas:</h4>
                    <ul><li>número insuficiente;</li><li>sem capacitação;</li><li>perfil inadequado;</li><li>desmotivadas.</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>

                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading2">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                      <img src="../include/img/icons/aula4IconHeading2.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Processos:</h4>
                    <ul><li>mal concebidos (fluxo, desenho);</li><li>sem manuais ou instruções formalizadas (procedimentos);</li><li>ausência de segregação de funções (Ex: Quem faz um pagamento não pode conferir a fatura depois).</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading3">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                      <img src="../include/img/icons/aula4IconHeading3.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Infraestrutura Física:</h4>
                    <ul><li>localização inadequada;</li><li>instalações ou leiaute inadequados;</li><li>inexistência de controles de acesso físico.</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="clear paddingBottom20"></div>

                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading4">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                      <img src="../include/img/icons/aula4IconHeading4.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Sistemas:</h4>
                    <ul><li>obsoletos;</li><li>sem integração;</li><li>sem manuais de operação;</li><li>inexistência de controles de acesso lógico (autenticação) / backups.</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading5">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
                      <img src="../include/img/icons/aula4IconHeading5.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Estrutura Organizacional:</h4>
                    <ul><li>falta de clareza quanto a funções e responsabilidades;</li><li>deficiências nos fluxos de informação e comunicação;</li><li>centralização de responsabilidades;</li><li>delegações exorbitantes.</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>


                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading6">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
                      <img src="../include/img/icons/aula4IconHeading6.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Tecnologia:</h4>
                    <ul><li>técnica de produção ultrapassada / produto obsoleto;</li><li>inexistência de investimentos em pesquisa e desenvolvimento;</li><li>tecnologia sem proteção de patentes;</li><li>processo produtivo (tecnologia) sem proteção contra espionagem.</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="clear paddingBottom20"></div>                  
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading7">
                    <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
                      <img src="../include/img/icons/aula4IconHeading7.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                    <div class="panel-body">
                    <h4 class="subTitulo ">Eventos Externos:</h4>
                    <ul><li>majoração tributária excessiva;</li><li>política econômica;</li><li>crise financeira;</li><li>eventos da natureza (ex: enchentes).</li></ul>
                    </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div class="clear"></div>

                  <p>Conforme destacado na Aula 1, as causas e as fontes de risco, se não trabalhadas, podem se transformar em problemas nas contratações. São exemplos de causas nesse contexto: a ausência de planejamento detalhado; projeto básico/termo de referência mal elaborado; falha na publicação legal do edital e a desclassificação indevida de um licitante durante o certame.</p>
                  <p>Causas como essas podem gerar eventos e consequências danosos ao bom andamento dos processos licitatórios e à imagem do órgão.</p>


              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina8.php', 'aula4pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


