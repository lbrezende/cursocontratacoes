<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Análise e Avaliação de Riscos', 'exibir', '4','11', '20', 'aula4pagina10.php', 'aula4pagina12.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p>Assim, quando falamos em riscos, formação e treinamento específicos são essencialmente necessários, dada a complexidade dos conceitos e do trabalho a ser executado.</p>
                <p>A figura abaixo ilustra os componentes do risco.</p>
                <p class="textAlignCenter"><img src="../include/img/aulas/Figura9.png" alt="Componentes do risco: causa (fontes e vulnerabilidades); evento (incidente e irregularidade) e consequência (impacto em um objetivo, ganho e perda)" /></p>
                <p class="textAlignCenter fonteMenor">Figura 9 – Componentes do risco (NETO, et al. 2014)</p>
                <p>Para identificar um risco, devemos descrever sempre o objetivo com o máximo de detalhes possível. A seguir, detalhamos alguns exemplos de riscos que podem impactar o objetivo abaixo:</p>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"></div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="post-it3 textAlignCenter">
                  <h1>Objetivo</h1>
                    <p>
                      Aquisição de equipamentos de TI, conforme especificado no termo de referência X no tempo máximo de Y dias com o menor custo, por meio da modalidade Z.</li>
                    </p>
                </div>
                </div>
                <div class="clear"></div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Exemplo de risco 1
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <p><span class="semi-bold" >Causa</span>: Devido à ausência de planejamento da contratação.</p>
                    <p><span class="semi-bold" >Evento</span>: Poderá ocorrer a contratação com sobrepreço.</p>
                    <p><span class="semi-bold" >Consequência</span>: Que acarretará a elevação dos custos da aquisição com grave infração à norma legal.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Exemplo de risco 2                   
                    </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                     <p><span class="semi-bold" >Causa</span>: Devido ao termo de referência mal elaborado.</p>
                    <p><span class="semi-bold" >Evento</span>: Poderá ocorrer a aquisição de produto que não atende à demanda.</p>
                    <p><span class="semi-bold" >Consequência</span>: Que acarretará a não satisfação da demanda e desperdício de recursos públicos.</p>
                    </div>
                  </div>
                  </div>
                  <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Exemplo de risco 3                      
                    </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                     <p><span class="semi-bold" >Causa</span>: Devido à desclassificação indevida de um licitante durante o certame.</p>
                    <p><span class="semi-bold" >Evento</span>: Poderá ocorrer a interposição de recurso por parte da prejudicada.</p>
                    <p><span class="semi-bold" >Consequência</span>: Que acarretará o atraso na conclusão do certame.</p>
                    </div>
                  </div>
                  </div>
                </div>                

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina10.php', 'aula4pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 