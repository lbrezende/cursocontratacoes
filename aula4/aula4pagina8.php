<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Processo Global de Avaliação de Riscos', 'exibir', '4','8', '20', 'aula4pagina7.php', 'aula4pagina9.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">3.  PROCESSO GLOBAL DE AVALIAÇÃO DE RISCOS</h3>
                <p>De acordo com IBGC (2007, p.24), na avaliação dos riscos, devemos considerar a capacidade da organização em lidar com essa questão. Isso significa ser capaz de identificar o risco, antecipá-lo, mensurá-lo, monitorá-lo e, se for o caso, mitigá-lo, conforme será exposto adiante.</p>
                <p>A ISO 31000 descreve o processo de avaliação de riscos como o processo global de <span class="semi-bold">identificação de riscos, análise de riscos</span> e <span class="semi-bold">avaliação de riscos</span>.</p>
                <h4 class="subTitulo">3.1.  IDENTIFICAÇÃO DE RISCOS</h4>
                <p>A identificação de riscos consiste no processo de busca, reconhecimento e descrição de um risco.  Para tanto, devemos ter um objetivo claramente definido.</p>
                <p class="abreCaixa" id="0">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p>
                <div id="caixa-0">
                  <div class="col-lg-8 col-md-8 col-sm-8" >
                    <p class="textAlignCenter tituloSaibaMais semPaddingBorda"><br />Mas para que os riscos devem ser identificados?</p>
                    <hr class="linhaInterrogacao" />
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
                  <div class="clear"></div>
                  <p>Cabe à administração do órgão assegurar que os objetivos sejam atingidos. Desse modo, como os riscos são eventos que podem impactar os objetivos organizacionais, a administração deve implementar medidas para lidar com os riscos, tendo em vista que “não podemos lidar com aquilo que não conhecemos”( NETO, et al. 2014).</p>
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                       Entre outros benefícios, clique aqui para saber como a identificação de riscos pode ser utilizada
                      </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                      <p><br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Maximizar as possibilidades de êxito de um projeto.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Melhorar a utilização dos recursos orçamentários. Com o maior êxito dos projetos, a utilização dos recursos também é otimizada, evitando-se desperdício. <br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Otimizar os prazos de tramitação de processos de contratação. A identificação dos riscos ajuda a administração a identificar e evitar percalços que podem ocorrer durante a tramitação processual.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Melhorar a gestão de uma área.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Preservar ativos de informação<a href="javascript:void(0);" rel="popover" data-content="<p>São as informações encontradas na forma de documentos, microformas, dados, arquivos, e bancos de dados, dentre outras de uma organização, bem como os recursos e equipamentos, incluindo <i>hardware</i> e <i>software</i> existentes para garantir o acesso, guarda, transporte, proteção das informações e possuem uma ou mais das seguintes características: 1 - São reconhecidos facilmente pelo valor que possuem para a organização. 2 - Não podem ser facilmente substituídos sem custos, conhecimento, tempo, recursos ou pela combinação desses recursos. 3 - São parte integrante da identidade corporativa da organização onde pela qual a mesma pode ser ameaçada. 4 - Em geral podem ser classificados como “Informação Proprietária”, “Altamente confidencial”, ou “Secreta”. 5 - Pessoas também podem ser consideradas como ativos da informação, pois detém conhecimentos que devem ser protegidos. Fonte Glossário de Riscos BACEN.</p>" data-toggle="popover" data-size="popover-small"><sup>1</sup></a>. Identificando riscos, podemos evitar eventos danosos que gerem prejuízos ao órgão.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Agregar valor ao negócio.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Proteger a imagem da instituição.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Reduzir custos. Como a tramitação processual passa a ser mais célere, os custos dos projetos são reduzidos.<br />
                       <img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />Aprimorar a qualidade de atendimento ao público. A identificação de riscos agrega informações importantes e detalhadas aos processos, que podem ser utilizadas, conforme o caso, por outras áreas do órgão na melhoria de suas funções.<br />
                      </p>
                      </div>
                    </div>
                    </div>
                  </div>
                  <p>Essa fase envolve a identificação das <span class="semi-bold">fontes de risco</span>, áreas de impacto, <span class="semi-bold">eventos</span> que podem impactar o objetivo, suas <span class="semi-bold">causas</span> (fatores de risco) e <span class="semi-bold">consequências</span> potenciais em termos de impactos no objetivo.</p>
                  <p><span class="semi-bold">Fonte de risco</span> é o elemento que, individualmente ou combinado, tem o potencial intrínseco para dar origem ao risco (ISO 31000). Algumas fontes de Risco são, por exemplo:</p>
                   <ul>
                    <li>Pessoas.</li>
                    <li>Processos.</li>
                    <li>Sistemas.</li>
                    <li>Infraestrutura física.</li>
                    <li>Estrutura organizacional.</li>
                    <li>Tecnologia.</li>
                    <li>Eventos externos, não gerenciáveis.</li>
                  </ul>
                </div>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina7.php', 'aula4pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


