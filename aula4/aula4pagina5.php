<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Conceitos', 'exibir', '4','5', '20', 'aula4pagina4.php', 'aula4pagina6.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			      <p>Verificando os conceitos apresentados, podemos observar que os riscos referem-se a <span class="semi-bold">eventos futuros</span>, que podem ocorrer ou não. Em regra, estão contidos na definição de risco a noção de futuro, de <span class="semi-bold">incerteza</span> e de <span class="semi-bold">impacto nos objetivos</span>.</p>
                <p>O evento futuro decorre do fato de que, se o risco já ocorreu, ele não é mais um risco, e sim um problema. De acordo com NETO, et al. (2014), problema é:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Situação ou condição <span class="semi-bold">existente</span> que afeta no momento <span class="semi-bold">presente</span> os objetivos de um indivíduo, grupo de pessoas, projeto ou organização e que requer uma solução.</p>
                  <p class="fonteMenor">É a diferença entre situação desejada e situação real.</p>
                  <p class="fonteMenor">Um risco, quanto se concretiza (isto é, quando o evento de risco ocorre), torna-se um problema.</p>
                </div>
                <div class="clear"></div>
                <div class="paddingBottom300">
                  <p>Para destacar a diferença entre Problema x Risco, os autores exemplificam:</p>
                  <div class="col-lg-4 col-md-4 col-sm-4 semPaddingLeft">
                  <p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/icone1.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-0">
                    <p class="textAlignCenter"><img src="../include/img/aulas/imagem1Aula3Tela5.png" alt="José Luiz trabalha em uma empresa em crise. Existe o risco de que, devido à necessidade de redução de custos, parte da força de trabalho seja demitida." border="0" class="marginRight10px" /></p>
                  </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 semPaddingLeft">
                  <p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/icone2.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-1">
                    <p class="textAlignCenter"><img src="../include/img/aulas/imagem2Aula3Tela5.png" alt="Se José Luiz perder o emprego, dizemos que o risco se concretizou e que ele tem um problema a enfrentar: a falta de renda." border="0" class="marginRight10px" /></p>
                  </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 semPaddingLeft">
                  <p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/icone3.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-2">
                    <p class="textAlignCenter"><img src="../include/img/aulas/imagem3Aula3Tela5.png" alt="Para enfrentar esse problema, ele adota duas medidas: lança mão de suas economias aplicadas na poupança e distribui currículos entre empresas." border="0" class="marginRight10px" /></p>
                  </div>
                  </div>

                </div>
                <div class="clear"></div>
                <p>Outro ponto importante no conceito de risco é a <span class="semi-bold">incerteza</span>. Ela diz respeito ao fato de que não podemos prever com exatidão se o evento ou condição ocorrerá ou não.</p>
                <p>Assim, também é importante distinguir risco de incerteza (NETO, et al. 2014):</p>
                <p class="textAlignCenter"><img src="../include/img/aulas/risco_incerteza-01.jpg" alt="Risco diz respeito ao evento em si, aquilo que pode acontecer. Incerteza diz respeito à nossa incapacidade para prever a sua ocorrência." border="0" class="" style="max-width:500px;padding:0px;" /></p>
                <p>Observe que a incerteza nos riscos reflete-se em incerteza nos resultados. Para minimizar a ocorrência de surpresas desagradáveis nos projetos, devemos conhecer os riscos e atuar sobre eles.</p>
                <p class="textAlignCenter"><img src="../include/img/aulas/Figura7.png" alt="os riscos podem afetar os resultados dos objetivos. " border="0" class="" style="max-width:500px;padding:0px;" /></p>                
                <p>Tendo por referência suas respectivas missão e visão, a Administração define os planos principais, seleciona as estratégias e determina o alinhamento dos <span class="semi-bold">objetivos</span> nos níveis da organização.</p>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina4.php', 'aula4pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


