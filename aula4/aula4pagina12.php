<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Análise e Avaliação de Riscos', 'exibir', '4','12', '20', 'aula4pagina11.php', 'aula4pagina13.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			     <h4 class="subTitulo">3.2.  ANÁLISE DE RISCOS</h4>
               <p>A análise de riscos é um processo que objetiva compreender, ou seja, conhecer melhor os riscos envolvidos. A análise fornece uma base para a avaliação e para as decisões sobre a necessidade de os riscos serem tratados e sobre as estratégias e métodos mais adequados de tratamento (ISO 31000).</p>
               <p>Essa análise é efetuada pela apreciação das causas e das fontes de risco, suas consequências positivas e negativas e a probabilidade de que essas consequências possam ocorrer.</p>
               <p>Na análise, também são verificados os controles existentes, sua eficácia e eficiência. O nível de detalhamento da análise pode variar conforme os riscos verificados, informações, dados e recursos disponíveis.</p>
               <h4 class="subTitulo">3.3. AVALIAÇÃO DE RISCOS</h4>
               <p>Esse processo consiste em comparar os resultados da análise de riscos com o objetivo de determinar a magnitude do risco – se ele é aceitável, tolerável ou não.</p>
               <p>Por meio dessa avaliação, iremos determinar o nível do risco em termos da sua magnitude. A definição da magnitude do risco é, portanto, um procedimento de valoração do risco, considerando-se aspectos relacionados à probabilidade e ao impacto sobre os objetivos. Quanto maior a probabilidade e maior o impacto, maior será o nível de risco.</p>
               <h4 class="subTitulo">3.3.1.  CRITÉRIOS PARA AVALIAÇÃO DE RISCOS</h4>
               <p>Para que os níveis de risco sejam determinados, é preciso estabelecer escalas para estimar a probabilidade e o impacto de cada risco. Com base na combinação desses dois fatores, podemos definir se, por exemplo, o nível de risco é baixo, médio, alto, extremo ou outro, a critério do órgão.</p>
               <p>Abaixo, são exemplificadas escalas qualitativas para auxiliar na valoração dos riscos, estimando probabilidades e impactos de eventos (NETO, et al. 2014). </p>
               <p class="textAlignCenter fonteMenor">Tabela 01: Escala de Probabilidades</p>
               <p class="textAlignCenter"><a href="../include/img/aulas/Tabela1.png" target="_blank" title=""><img src="../include/img/aulas/Tabela1.png" alt="" style="max-width:500px;" /></a></p>
               <p class="textAlignCenter fonteMenor">Tabela 02: Escala de Impactos</p>
               <p class="textAlignCenter"><a href="../include/img/aulas/Tabela2.png" target="_blank" title=""><img src="../include/img/aulas/Tabela2.png" alt="" style="max-width:500px;" /></a></p>
               



              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina11.php', 'aula4pagina13.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


