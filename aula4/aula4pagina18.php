<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Controles Internos Administrativos e Monitoramento dos Riscos', 'exibir', '4','18', '20', 'aula4pagina17.php', 'aula4pagina19.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">6.  CONTROLES INTERNOS ADMINISTRATIVOS</h3>
                <p>Os riscos podem ser mitigados por meio de atividades de controle, conhecidas como “controles internos administrativos”, que consistem em políticas e procedimentos adotados pela gestão para manter os riscos dentro dos níveis aceitáveis (NETO, et al. 2014).</p>
                <p>Destaque-se que os controles internos administrativos não se confundem com a unidade de controle interno ou de auditoria interna, prevista no art. 74 da Constituição Federal Brasileira , que tem finalidade precípua avaliativa, incluindo a análise da efetividade, da consistência, da qualidade e da suficiência dos controles internos implantados pelos gestores para assegurar o alcance dos objetivos definidos pela Administração.</p>
                <p>Abaixo, destacamos um exemplo de risco que impacta um processo de planejamento de contratação de soluções de TI com a sugestão de controle interno para tratá-lo (TCU, 2012, p. 217):</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor"><span class="semi-bold">Definição de prazo curto demais para a apresentação de proposta relativa a uma solução de TI complexa em licitação</span> do tipo pregão, de modo a favorecer fornecedor(es) específico(s) que tenham condições de oferecer propostas de forma mais rápida.</p>
                  <p class="fonteMenor"><span class="semi-bold">Sugestões de controles internos</span>:</p>
                  <p class="fonteMenor">Em função da complexidade da solução, <span class="semi-bold">a equipe de planejamento da contratação deve encaminhar à equipe responsável pela condução da licitação</span> (Comissão Permanente de Licitação) o prazo adequado para que as licitantes formulem propostas na etapa de seleção do fornecedor. Salienta-se que o prazo citado não pode ser inferior a oito dias úteis (Lei 10.520/2002, art. 4º, inciso V), mas esse prazo pode ser curto demais para se elaborar propostas para determinadas soluções de TI.</p>
                  <p class="fonteMenor">Considerações: em geral, <span class="semi-bold">quanto maior a complexidade da solução, mais numerosos e mais complexos são os requisitos</span>. Dessa forma, deve-se garantir a <span class="semi-bold">definição de prazo razoável para elaboração das propostas</span>. (Grifamos).</p>
                </div>
                <div class="clear"></div>
                <p>Registre-se que, nos termos do Acórdão 1.024/2009, Plenário, TCU, os controles internos administrativos servem para que o órgão evite ou reduza o “impacto ou a probabilidade da ocorrência de eventos de risco na execução de seus processos e atividades, que possam impedir ou dificultar o alcance de objetivos estabelecidos.”. São, portanto, controles que a própria Administração deve manter para exercer sua finalidade com maior segurança.</p>
                <h3 class="titulo">7.  MONITORAMENTO DOS RISCOS</h3>
                <p>Conforme destacado ao longo desta aula, cada organização, por meio de sua alta administração, irá inicialmente estabelecer e, ao longo do processo, avaliar <span class="semi-bold">continuamente</span> a adequação e a eficácia de seu modelo de gestão de riscos corporativos.</p>
                <p>O modelo de gestão adotado deve ser constantemente monitorado, com o objetivo de assegurar a presença e o funcionamento de todos os seus componentes ao longo do tempo (IBGC, 2007, p.24).</p>
                <p>No que se refere ao monitoramento regular, esse deve ocorrer no curso normal das atividades gerenciais. </p>
                <p>O escopo e a frequência de avaliações ou revisões específicas dependerão de uma avaliação do perfil de riscos da organização e da eficácia dos procedimentos regulares de monitoramento. </p>
                <p>O modelo deve funcionar em harmonia com os objetivos organizacionais a serem resguardados. As vulnerabilidades e deficiências devem ser reportadas aos níveis superiores de gestão e, dependendo da gravidade, à alta administração.</p>
                <p>O objetivo do monitoramento é dar perenidade ao modelo de gestão de riscos, avaliando e corrigindo os pontos necessários, de modo a fechar o ciclo do processo de gestão de riscos.</p>



              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina17.php', 'aula4pagina19.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


