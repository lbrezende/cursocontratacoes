<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Processo Global de Avaliação de Riscos', 'exibir', '4','10', '20', 'aula4pagina9.php', 'aula4pagina11.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 
 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			       <p>Outro ponto importante na identificação de riscos é o exame das <span class="semi-bold">consequências</span> potenciais em termos de impactos no objetivo. Para simplificar, a consequência é o resultado de um evento que afeta os objetivos (NETO, et al. 2014).</p>
                 <p>À vista do exposto, percebemos que a finalidade da identificação de riscos é gerar uma lista abrangente de riscos baseada nos eventos impactantes que a organização deseja trabalhar. Para tanto, a identificação pode ser efetuada tendo por referência dados históricos, análises teóricas, necessidades das partes interessadas e opiniões de pessoas informadas e especialistas.</p>
                 <p>Toda essa dinâmica de trabalho objetiva definir qual será o tratamento dado aos riscos identificados pela organização. A ISO 31000 destaca que é conveniente que a organização aplique ferramentas e técnicas de identificação de riscos que sejam adequadas aos seus objetivos, capacidades e aos riscos a serem enfrentados. Recomenda também que pessoas com um conhecimento apropriado sejam envolvidas na identificação dos riscos. </p>
                 <p>Acerca da identificação de riscos, apresentamos o exemplo a seguir:</p>

                  <div class="notebook">
                  <div class="lines"></div>

                  <ul class="list">
                    <li class="rosa">Objetivo do TSE (Tribunal Superior Eleitoral):</li>
                    <li>Realizar recadastramento biométrico de 14,2 milhões de eleitores até março de 2014.</li>
                    <li><span class="rosa">Possíveis riscos</span>:</li>
                    <li>1.  Eleitores desinformados não comparecerem aos cartórios eleitorais dentro do prazo.</li>
                    <li>2.  Eleitores comparecerem sem portar os documentos necessários.</li>
                    <li>3.  Pane no sistema de informação do TSE.</li>
                    <li>4.  Greve dos servidores da Justiça Eleitoral.</li>
                    <li>Algumas <span class="rosa">ferramentas e técnicas</span> que podem auxiliar na identificação e avaliação de riscos:</li>
                     <li>Análise de Cenários</li>
                    <li>Brainstorming</li>
                    <li>Benchmarking</li>
                    <li>GAO</li>
                    <li>Construção da Matriz SWOT</li>
                    <li>Análise dos Objetivos Estratégicos</li>
                    <li>Análise da Cadeia de Valores do orgão</li>
                    <li>Análise de dados históricos</li>
                    <li>Entrevistas, etc</li>
                  </ul>
                  </div>
                <p class="paddingTop20">No tópico “Sugestões de Leitura Complementar desta Aula”, indicamos um livro que apresenta várias ferramentas e técnicas que, ao longo do tempo, tornaram-se de uso universal, servindo ao apoio da gestão de diversas áreas de conhecimento da administração (DAYCHOUM, 2013). Informações adicionais também podem ser encontradas em sítios especializados da Internet<a href="javascript:void(0);" rel="popover" data-content="<p>Para saber mais: <a href='https://www.portal-gestao.com/item/2414-a-an%25C3%25A1lise-swot-e-o-processo-de-planeamento-estrat%25C3%25A9gico.html' target='_blank' title=''>Portal Gestão</a>; <a href='http://www.portal-administracao.com/2014/01/analise-swot-conceito-e-aplicacao.html' target='_blank' title=''>Portal Administração</a>; <a href='http://www.portalgerenciais.com.br/brainstorm.php' target='_blank' title=''>Portal Gerenciais</a>. Acessos em 24/02/2015. </p>" data-toggle="popover" data-size="popover-small"><sup>3</sup></a>.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina9.php', 'aula4pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


