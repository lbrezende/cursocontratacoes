<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referências Bibliográficas', 'exibir', '4','20', '20', 'aula4pagina19.php', 'aula4pagina19.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">SUGESTÕES DE LEITURA COMPLEMENTAR</h3>
                <p>Associação Brasileira de Normas Técnicas. ABNT NBR ISO 31000.</p>
                <p>Committee of Sponsoring Organizations of the Treadway Commission, COSO ERM - COSO II, <i>Enterprise Risk Management – Integrated Framework</i> (Gerenciamento de Riscos Corporativos – Estrutura Integrada).</p>
                <p>Livro: DAYCHOUM, Merhi. <i>40 + 10 Ferramentas e técnicas de gerenciamento</i>. Brasport. 2013.</p>
                <h3 class="titulo">REFERÊNCIAS BIBLIOGRÁFICAS</h3>
                <p>Associação Brasileira de Normas Técnicas. ABNT NBR ISO 31000:2009, <i>Risk management, norma orientadora e reguladora que introduz o padrão mundial em Gestão de Riscos, aplicável a entidades de todos os tamanhos e tipos</i>. Disponível em: <a href="http://www.iso.org/iso/home/standards/iso31000.htm" target="_blank" title="">http://www.iso.org/iso/home/standards/iso31000.htm</a></p>
                <p>Associação Brasileira de Normas Técnicas. ABNT ISO GUIA 73:2009</p>
                <p>Committee of Sponsoring Organizations of the Treadway Commission, COSO ERM - COSO II, <i>Enterprise Risk Management – Integrated Framework</i> (Gerenciamento de Riscos Corporativos – Estrutura Integrada).</p>
                <p>BRASIL. Conselho Nacional de Justiça. Resolução nº171/2013.</p>
                <p>BRASIL. Tribunal de Contas da União. <i>Guia de boas práticas em contratação de soluções de Tecnologia da Informação: riscos e controles para o planejamento da contratação</i> – Versão 1.0. – Brasília: TCU, 2012.</p>
                <p>BRASIL. Tribunal de Contas da União. Instrução Normativa nº 63/2010.</p>
                <p>BRASIL. Tribunal de Contas da União. <i>Critérios Gerais de Controle Interno na Administração Pública. Um estudo dos modelos e das normas disciplinadoras em diversos países</i>. 2009.</p>
                <p>Dicionários Houaiss e Michaelis.</p>
                <p>Guia de orientação para o gerenciamento de riscos corporativos / Instituto Brasileiro de Governança Corporativa; coordenação: Eduarda La Rocque. São Paulo, SP: IBGC, 2007 (Série de Cadernos de Governança Corporativa, 3). 48p.</p>
                <p>Guia Project Management Body of Knowledge (PMBOK) – 4. ed.</p>
                <p>INTOSAI - GOV 9130/2007</p>
                <p>IPPF – Guia Prático: Coordenando o Gerenciamento e a Avaliação de Riscos.</p>
                <p>NETO, Antonio Alves de Carvalho; LIMA, Maria Lúcia de Oliveira F.; NORONHA, Maridel Piloto; PALUMBO, Salvatore. <i>Curso GESTÃO DE RISCOS Princípios e Diretrizes</i>. Acordo de Cooperação Técnica TCU/CNJ nº 087/2010. Brasília-DF, 2014.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina19.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


