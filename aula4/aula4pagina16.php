<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Tratamento de Riscos', 'exibir', '4','16', '20', 'aula4pagina15.php', 'aula4pagina17.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			     <h4 class="subTitulo">5.2.  RESPOSTA A RISCOS</h4>
               <p>O tratamento dos riscos objetiva definir uma postura da organização em relação aos riscos identificados. De acordo com a gestão organizacional, a instituição decidirá entre:</p>
               <ul>
                <li>evitar Riscos;</li>
                <li>transferir Riscos;</li>
                <li>aceitar ou reter Riscos;</li>
                <li>mitigar Riscos.</li>
               </ul>
               <p>A figura abaixo destaca as opções:</p>
               <p class="textAlignCenter"><img src="../include/img/aulas/Figura12.png" alt="Opções de reposta da organização em relação aos riscos identificados: evitar, transferir, aceitar e mitigar" /></p>
               <p class="textAlignCenter fonteMenor">Figura 12 – Tratamento de riscos (NETO, et al. 2014).</p>
               <h4 class="subTitulo">5.2.1.  EVITANDO RISCOS</h4>
               <p>Evitar um risco consiste em adotar ação (ou ações) para evitar totalmente o risco identificado. Pode ser efetuada eliminando ou descontinuando as atividades geradoras do risco.</p>
               <p>É a decisão de não se envolver ou agir de forma a se retirar de uma situação de risco. Exemplo: uma organização decide se desfazer de uma unidade de negócios, pois considera que aquela atividade não agrega tanto valor em relação aos custos e riscos envolvidos (IBGC, 2007, p.23). </p>
               <p>Outro exemplo é o caso de uma autarquia que decide descontinuar um tipo específico de atividade, legalmente atribuída a ela, tendo em vista que essa atividade envolve riscos consideráveis e não é mais necessária. Nesse caso, o risco poderá ser evitado pela atuação de sua assessoria parlamentar, que avaliará a possibilidade de sugerir alteração da referida lei, para liberar a autarquia da atividade desnecessária.</p>
               <p>Para o setor público, essa opção, de modo geral, é difícil em várias situações, tendo em vista que é da natureza desse assumir riscos que os cidadãos não podem assumir individualmente (NETO, et al. 2014).</p>


              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina15.php', 'aula4pagina17.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


