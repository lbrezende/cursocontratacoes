<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Considerações Finais', 'exibir', '4','19', '20', 'aula4pagina18.php', 'aula4pagina20.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">

                <div class="col-lg-6 col-md-6 col-sm-6">
                  <h3 class="titulo">RESUMO DA AULA</h3>

                  <p>Nesta aula, apresentamos os principais conceitos associados aos riscos, bem como tivemos uma visão global sobre gestão de riscos corporativos. Verificamos que se trata de tema novo e de muita importância, pois possibilita que a alta administração e demais gestores da organização lidem eficientemente com a incerteza, buscando um balanceamento ótimo entre desempenho e riscos associados.</p>
                  <p>Na próxima aula, serão apresentadas noções sobre o significado dos resultados para a Administração Pública, além de boas práticas associadas às contratações em geral.</p>
                  <p>Até lá... </p>
                  <p>Abraços e bom estudo. </p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <h3 class="textAlignCenter titulo">MENSAGEM</h3>
                  <p class="textAlignCenter"><img src="../include/img/aulas/Mensagem_Aula_4.jpg" style="" alt="Palavra do Autor" /></p>
                </div>
                <div class="clear"></div>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina18.php', 'aula4pagina20.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


