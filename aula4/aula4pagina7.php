<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Gestão e Gerenciamento de Riscos', 'exibir', '4','7', '20', 'aula4pagina6.php', 'aula4pagina8.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">2.  GESTÃO E GERENCIAMENTO DE RISCOS</h3>
                <p>A Gestão de Risco é um conjunto de atividades coordenadas para dirigir e controlar uma organização, no que se refere aos riscos. Essas atividades normalmente incluem a avaliação do risco, seu tratamento, aceitação e comunicação às partes interessadas (ABNT ISO/IEC - Guia 73:2005).</p>
                <p>De acordo com a ISO 31000, ambas as expressões gestão de risco e gerenciamento de riscos são utilizadas. Ocorre que, em termos gerais, a gestão de risco está relacionada à arquitetura existente (princípios, estrutura e processo) para gerenciar riscos eficazmente, ao passo que o gerenciamento de riscos refere-se à aplicação dessa arquitetura para riscos específicos.</p>
                <p>Assim, as duas expressões não são sinônimas. A gestão de risco possui maior amplitude, abrangência e está situada no nível organizacional, mais direcionada à alta administração. Já o gerenciamento de riscos está ligado primordialmente às técnicas a serem utilizadas no processo, em nível operacional.</p>
                <p>Desse modo, é recomendável que as organizações administrem seus riscos por meio de um processo sistemático e lógico de gestão de riscos (ISO 31000), de modo a <span class="semi-bold">comunicar e consultar as partes interessadas, estabelecer o contexto</span> em que o processo de gestão de riscos será <span class="semi-bold">aplicado, identificar, analisar, avaliar, tratar</span> e <span class="semi-bold">monitorar</span> continuamente os riscos relevantes. Tudo isso será realizado de acordo com os critérios de riscos estabelecidos por cada instituição. A figura a seguir exemplifica esse processo: </p>
                <p class="textAlignCenter"><img src="../include/img/aulas/Figura8-06.png" alt="etapas do processo: identificar, analisar, avaliar, tratar, monitorar, comunicar e consultar, estabelecer o contexto." /></p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina6.php', 'aula4pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


