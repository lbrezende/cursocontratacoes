<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Tratamento de Riscos', 'exibir', '4','17', '20', 'aula4pagina16.php', 'aula4pagina18.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h4 class="subTitulo">5.2.2.  TRANSFERINDO RISCOS</h4>
                <p>Trata-se de compartilhar ou transferir uma parte do risco a terceiros. Prudente lembrar que o relacionamento com o terceiro para o qual o risco será transferido deve ser bem gerenciado/acompanhado para assegurar a efetiva transferência do risco.</p>
                <p>Alguns exemplos desse tipo de situação são os seguros, contratos com cláusulas específicas ou com garantias de níveis de serviço (SLA), terceirização de atividades, etc. Abaixo, um exemplo:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Uma concessionária de energia elétrica identificou e avaliou os riscos de falhas naturais com danos elétricos em seus equipamentos turbo-geradores e de potência de grandes usinas. Após analisar a melhor estratégia a ser adotada no que tange às despesas possíveis com franquia vis-à-vis os prêmios de risco a serem contratados, constitui-se um seguro destes equipamentos junto ao mercado, transferindo este risco operacional categorizado como de alto impacto e baixa freqüência, inerente ao processo de operação e manutenção (IBGC, 2007, p.23).</p>
                </div>
                <div class="clear"></div>
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <p class="textAlignCenter subTitulo">SLA</p>
                    <p>Um acordo de nível de serviço (Service Level Agreement -- SLA) é um contrato entre um fornecedor de serviços de TI e um cliente especificando, em geral em termos mensuráveis, quais serviços o fornecedor vai prestar. Níveis de serviço são definidos no início de qualquer relação de outsourcing e usados para mensurar e monitorar o desempenho de um fornecedor. Fonte: <a href="http://goo.gl/JmjNGl" target="_blank" title="acesse definições do SLA">http://goo.gl/JmjNGl</a>. Acesso em 30/01/2015.</p>
                  </div>
                </div>
                <div class="clear"></div>
                <p>Devem ser transferidos por meio de seguro os riscos tidos como catastróficos (riscos de baixa frequência e alta severidade), os riscos de alta frequência que provoquem cumulativamente perdas relevantes e todos aqueles cujo custo de transferência seja inferior ao custo de retenção (IBGC, 2007, p.23).</p>
                <p>Um exemplo é o caso de uma autarquia X, que decide passar para outra Y um tipo específico de operação legalmente atribuída a ela. O risco poderá ser afastado sugerindo-se que a assessoria parlamentar da autarquia X avalie a possibilidade de indicar a alteração da referida lei, de modo a transferir a operação, e os riscos a ela associados, para a autarquia Y, que passará a ser a nova responsável pela execução dessa operação.</p>
                <p>Alguns riscos não podem ser totalmente transferidos, por exemplo, o de reputação e imagem (NETO, et al. 2014).</p>
                <h4 class="subTitulo">5.2.3.  ACEITANDO OU RETENDO RISCOS</h4>
                <p>Outro tratamento relacionado ao risco consiste em aceitar ou tolerar esse, sem que nenhuma ação específica seja tomada. É a decisão de aceitar um risco (ABNT ISO/IEC - Guia 73:2005. Glossário BCB.)</p>
                <p>Essa opção pode decorrer de:</p>
                <ul>
                  <li>o nível de o risco ser considerado baixo;</li>
                  <li>a capacidade limitada da organização para fazer alguma coisa;</li>
                  <li>o custo do tratamento pode ser desproporcional ao benefício.</li>
                </ul>
                <p>Já a <span class="semi-bold">retenção</span> do risco ocorre quando, entre as opções de tratamento, nenhuma resposta é considerada eficaz para reduzir a probabilidade ou o impacto do risco a um custo aceitável.</p>
                <p>Geralmente, o tratamento de alguns riscos pode ficar tão caro que, mesmo não sendo toleráveis, vale mais a pena retê-los e ter um plano “B”, caso ocorram (plano de contingência). Nesses casos, nada é feito além de aceitar e monitorar o risco (NETO, et al. 2014).</p>
                <h4 class="subTitulo">5.2.4.  MITIGANDO RISCOS </h4>
                <p>Frequentemente ouvimos a expressão mitigar riscos. Ela é comum, haja vista que um grande número de riscos tratados recebe esse tipo de resposta.</p>
                <p>Mitigar riscos é uma opção da Administração pela adoção de políticas para conviver de modo “harmonioso” com a chance de o risco ocorrer, mantendo essa possibilidade sob “vigília”. As organizações buscam ações que possam reduzir a probabilidade, o impacto do risco ou ambos. </p>
                <p>Essas ações são chamadas atividades de controle, conhecidas entre nós simplesmente como “controles internos”, que consistem em políticas e procedimentos adotados pela gestão para manter os riscos dentro dos níveis aceitáveis (NETO, et al. 2014).</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina16.php', 'aula4pagina18.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


