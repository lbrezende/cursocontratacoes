<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Análise e Avaliação de Riscos', 'exibir', '4','13', '20', 'aula4pagina12.php', 'aula4pagina14.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p>Para definir o nível de risco, devemos identificar qual o Impacto e a Probabilidade de cada risco. Após, devemos multiplicar um parâmetro pelo outro para obtermos o nível associado ao risco.</p>
                <p>Vamos exemplificar. Para tanto, utilizaremos o objetivo já citado nesta aula, conforme abaixo.</p>
                <p class="textAlignCenter fonteMenor">Tabela 03: Classificação de Riscos</p>
                <p class="textAlignCenter"><a href="../include/img/aulas/Tabela3.png" target="_blank" title=""><img src="../include/img/aulas/Tabela3.png" alt="" style="max-width:500px;" /></a></p>
                <p>Com os dados obtidos da relação Probabilidade x Impacto, podemos verificar qual o enquadramento do nível de risco, conforme matriz abaixo (NETO, et al. 2014):</p>
                <p class="textAlignCenter subTitulo">MATRIZ DE NÍVEL DE RISCO</p>
                <p class="textAlignCenter"><a href="../include/img/aulas/Figura10_MatrizDeNivelDeRisco.png" target="_blank" title=""><img src="../include/img/aulas/Figura10_MatrizDeNivelDeRisco.png" alt="" style="max-width:500px;" /></a></p>
                <p class="textAlignCenter fonteMenor">Figura 10 - Aferição do Nível de Risco.</p>
                <p>Conforme será abordado adiante, em regra, as organizações podem buscar medidas e controles para modificar o nível de risco, de modo a tentar posicioná-lo em um patamar que seja aceitável para a organização.</p>
                <p>Lidar com a avaliação de risco não é tarefa fácil para os órgãos. A título de exemplo, um incêndio pode ter sido classificado como um evento de alta magnitude para uma organização do setor florestal. Contudo, se a organização possui forte capacitação interna para prevenir e controlar um incêndio, o evento, inicialmente classificado como de alto impacto, pode ser reclassificado para médio ou baixo impacto (IBGC, 2007, P.24).</p>
                

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina12.php', 'aula4pagina14.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


