<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

  $acessibilidadeTxt = null;
  if ($acessibilidade == "sim") { 
    $acessibilidadeTxt = "?ac=sim";
  }; 
configHeader('Bem-vindo', 'exibir', '4','1', '20', 'index.php', 'aula4pagina2.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

    <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <p><span class="semi-bold">Caros colegas,</span></p>
                <p>Bom dia, boa tarde, boa noite!</p>
                <p>Sejam bem-vindos.</p>
                <p>Dando sequência a nosso treinamento, na aula de hoje, abordaremos outro assunto contemporâneo e bem interessante: a gestão de riscos.</p>
                <p>Vamos iniciar?</p>
                <h3 class="titulo">Objetivos desta aula</h3>
                <div class="col-lg-6 col-md-6 col-sm-6 ">
                  <p>Prezado participante,</p>
                                <p>Normalmente dizem que não é muito didático iniciar uma aula falando que o tema é complexo. Realmente não é, porém, quando falamos em Gestão de riscos corporativos, não há como negar que o assunto foge à esfera da simplicidade. Trata-se de questão abrangente, que está sendo difundida no âmbito das organizações públicas na esteira das ações de governança. Nosso objetivo, neste momento, é compreender o conceito de risco e seus elementos essenciais, de modo a adquirir uma visão geral sobre o tema.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
                  <img src="../include/img/aulas/mapaMentalAula4Tela1.png" alt="" class="imgAulas" />
                </div>
                <div class="clear espacamentoLista"></div>   						
              </div>
            </div>   
           </div>
        </div>
    </article>    

    <footer>  
      <nav role="navigation" class="">
        <div class="textAlignCenter ">
          <a href=<?php echo '"aula4pagina2.php'.$acessibilidadeTxt.'"';?> class="btn btn-lg btn-success" title="Ir para próxima página">Avan&ccedil;ar <i class="fa fa-arrow-circle-o-right">  </i></a>
        </div>
      </nav>
    </footer>  

<!-- <?php  //configNavegacaoRodape('exibir', 'index.php', 'aula4pagina3.php'); //Rodapé automático aqui  ?>-->         
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


