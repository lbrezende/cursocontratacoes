<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Conceitos', 'exibir', '4','4', '20', 'aula4pagina3.php', 'aula4pagina5.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h4 class="subTitulo">1.2. CONCEITOS: O QUE É UM RISCO CORPORATIVO? </h4>
                <p>Conforme visto anteriormente, a Governança nasceu para tornar a gestão mais sólida, transparente e efetiva. Na linha das boas práticas afetas à Governança, surgiu a gestão de riscos como meio para que os agentes gerenciassem adequadamente as oportunidades e as ameaças ao desempenho de seu negócio.</p>
                <p>A crise financeira dos últimos anos deixou clara a necessidade de se abordar a questão dos riscos corporativos de modo mais assertivo. Isso porque as organizações passaram a entender que era importante aprender a lidar com possíveis situações que poderiam prejudicar ou inviabilizar seus objetivos.</p>
                <p>A necessidade de se disseminar uma cultura de gestão contínua de riscos nas instituições foi impulsionada pelos efeitos desastrosos que impactaram negativamente a imagem e as contas das instituições afetadas por escândalos e pela crise mundial.</p>
                <p>Mas o que é um risco corporativo, organizacional ou simplesmente risco (denominação que utilizaremos a partir de agora)?</p>
                <p>Vejamos algumas definições de risco:</p>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="col-lg-6 col-md-6 col-sm-6 "><img src="../include/img/icons/aspas_esquerda.png" style="float:left;" /><p class="textAlignCenter bordaConceito">Risco é o efeito da <span class="semi-bold">incerteza</span> nos <span class="semi-bold">objetivos</span>. (ISO 31000)</p></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="clear"></div>
                <p>Para a ISO 31000, o efeito é um desvio em relação ao esperado. Esse desvio pode ser positivo e/ou negativo.</p>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="col-lg-6 col-md-6 col-sm-6 "><img src="../include/img/icons/aspas_direita.png" style="float:right;" /><p class="textAlignCenter bordaConceito">O risco é representado pela possibilidade de que um <span class="semi-bold">evento</span> venha a ocorrer e afete negativamente a realização dos <span class="semi-bold">objetivos</span>. (COSO II)</p></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="clear"></div>
                <p>De acordo com o COSO II, a realização de objetivos está sujeita à ação de eventos internos e externos, nem sempre sob o controle da organização:</p>
                <div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
                <div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
                  <p class="fonteMenor">Os eventos podem gerar impacto tanto negativo quanto positivo ou ambos. Os que geram impacto negativo representam riscos que podem impedir a criação de valor ou mesmo destruir o valor existente. Os de impacto positivo podem contrabalançar os de impacto negativo ou podem representar oportunidades, que por sua vez representam a possibilidade de um evento ocorrer e influenciar favoravelmente a realização dos objetivos, apoiando a criação ou a preservação de valor. A direção da organização canaliza as oportunidades para seus processos de elaboração de estratégias ou objetivos, formulando planos que visam ao aproveitamento destes.</p>
                </div>
                <div class="clear"></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="col-lg-6 col-md-6 col-sm-6 "><img src="../include/img/icons/aspas_esquerda.png" style="float:left;" /><p class="textAlignCenter bordaConceito">A INTOSAI GOV 9130/2007 (p. 12, item 2.3.2) descreve que “Um <span class="semi-bold">evento</span> é um incidente ou ocorrência proveniente de <span class="semi-bold">fontes internas ou externas</span> que <span class="semi-bold">afeta</span> a implementação da <span class="semi-bold">estratégia</span> ou a realização de <span class="semi-bold">objetivos”</span>.</p></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="clear"></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="col-lg-6 col-md-6 col-sm-6 "><img src="../include/img/icons/aspas_direita.png" style="float:right;" /><p class="textAlignCenter bordaConceito">Risco é um evento ou uma <span class="semi-bold">condição incerta</span> que, se ocorrer, tem um efeito em, pelo menos, um <span class="semi-bold">objetivo</span> do projeto. Os objetivos podem incluir escopo, cronograma, custo e qualidade. Um risco pode ter uma ou mais causas e, se ocorrer, pode ter um ou mais impactos. (PMBOK)</p></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="clear"></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="col-lg-6 col-md-6 col-sm-6 "><img src="../include/img/icons/aspas_esquerda.png" style="float:left;" /><p class="textAlignCenter bordaConceito">Risco é a possibilidade de algo acontecer e ter impacto nos <span class="semi-bold">objetivos</span>, sendo medido em termos de <span class="semi-bold">consequências</span> e <span class="semi-bold">probabilidades</span>. (TCU IN 63/2010, Art. 1º, V. e CNJ Res.171/2013, Art. 12, IV).</p></div>
                <div class="col-lg-3 col-md-3 col-sm-3 "></div>
                <div class="clear"></div>
                <p>Conforme será abordado adiante, só há sentido falar em riscos quando especificamos objetivos a serem atingidos e controles a serem implementados para tratar os riscos relevantes.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina3.php', 'aula4pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


