<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Riscos', 'exibir', '4','2', '20', 'index.php', 'aula4pagina3.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">1. RISCOS</h3>
                <h4 class="subTitulo"><p>Você sabe o que é um risco?</p></h4>
                <p>No senso comum, risco é a probabilidade de perigo, geralmente com ameaça física para o homem e/ou para o meio ambiente, normalmente associada a risco de vida, infecção, contaminação, etc. Por extensão de sentido, é a possibilidade de insucesso de determinado empreendimento, em função de acontecimento eventual, incerto, mas previsível, cuja ocorrência não depende exclusivamente da vontade dos interessados (Dicionários Houaiss e Michaelis).</p>
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <p>A etimologia da palavra “Risco” indica que o termo possui origem no vocábulo risicu ou riscu em latim, que significa ousar (to dare, em inglês) - IBGC, 2007.</p>
                  </div>
                </div>
                <div class="clear"></div>
                <p>O risco é inerente a toda atividade humana, em qualquer esfera, seja na vida pessoal, no esporte, no lazer, no trabalho, na empresa, etc.</p>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <p>Observe o vídeo a seguir, que, embora trate de riscos ambientais, apresenta algumas características do risco. Observe que o risco está associado a eventos ou condições incertos que, se ocorrerem, afetam objetivos das pessoas ou organizações. Não se preocupe, pois esses conceitos serão detalhados no decorrer desta aula.</p>    
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="video-container">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/DaRrgKdg4Lo" frameborder="0" allowfullscreen></iframe>
                  </div><br><br>
                </div>
                <div class="clear"></div>
                <p>Os riscos estão por toda parte...</p>
                <p>Nossa abordagem em relação ao risco será concentrada nos riscos corporativos, ou seja, aqueles que afetam as empresas, órgãos e projetos.</p>

              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'index.php', 'aula4pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


