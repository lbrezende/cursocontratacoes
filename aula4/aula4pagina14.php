<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Apetite e Tolerância a Risco', 'exibir', '4','14', '20', 'aula4pagina13.php', 'aula4pagina15.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">4.  APETITE E TOLERÂNCIA A RISCO</h3>
                <p>O apetite pelo risco refere-se à quantidade e ao tipo de risco que uma organização está preparada para buscar, manter ou assumir, ou seja, quais riscos a organização deseja correr (ABNT ISO Guia 73:2009 - definição 3.7.1.2).</p>
                <p>Cada organização possui um apetite a risco diferente, de acordo com sua área de atuação, cultura, administração, etc. Também há a aversão ao risco, que é a atitude de afastar-se de riscos.</p>
                <p>A tolerância a risco consiste nos “Níveis aceitáveis de variação em relação à realização dos objetivos”. (INTOSAI GOV 9130/2007, 2.2.6). Também é a “disposição da organização ou parte interessada em suportar o risco após o tratamento, a fim de atingir seus objetivos” (ISO GUIA 73:2009, 3.7.1.3).</p>
                <p>Abaixo, destacamos as diversas posições em relação ao risco (NETO, et al. 2014).</p>
                <p class="textAlignCenter"><img src="../include/img/aulas/Figura11.png" alt="escala mostra em um polo governo e indústrias como avessos a risco e no outro polo investidores e empreendedores com alto apetite a risco" /></p>
                <p class="textAlignCenter fonteMenor">Figura 11 – Escala de predisposição ao risco </p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina13.php', 'aula4pagina15.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


