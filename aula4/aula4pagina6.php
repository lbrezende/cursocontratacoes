<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Conceitos', 'exibir', '4','6', '20', 'aula4pagina5.php', 'aula4pagina7.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
    			     <p>O modelo COSO II, Enterprise Risk Management - Integrated Framework (Gerenciamento de Riscos Corporativos - Estrutura Integrada, p.19) classifica os objetivos organizacionais em quatro categorias, clique nas imagens para maiores informações:</p>
                
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"><!-- inicio de tabela de conceitos com accordion -->
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingOne">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/Estrategicos_COSO_Categorias-01.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Estratégicos</h4>
                    <p class="textAlignCenter">Metas gerais, alinhadas com mecanismos de suporte à sua missão.</p>
                    </div>
                  </div>
                  </div>
                  </div><!-- divisao em cols-->
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/Confiabilidade_COSO_Categorias-03.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Comunicação</h4>
                    <p class="textAlignCenter">Confiabilidade de relatórios.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingThree">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/Operacionais_COSO_Categorias-02.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Operacionais </h4>
                    <p class="textAlignCenter">Utilização eficaz e eficiente dos recursos.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="headingFour">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/Conformidade_COSO_Categorias-04.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Conformidade</h4>
                    <p class="textAlignCenter">Cumprimento de leis e regulamentos aplicáveis.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div class="clear"></div><!-- fim de tabela de conceitos com accordion -->
                  <p>O Guia PMBOK (4° ed) aborda os objetivos de projetos pela perspectiva de suas dimensões, que podem possuir a seguinte classificação (clique nas imagens para maiores informações): </p>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading1">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/PMBOK_ClassificaçãoCusto-02.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Custo</h4>
                    <p class="textAlignCenter">Valores associados ao objetivo.</p>
                    </div>
                  </div>
                  </div>
                  </div><!-- divisao em cols-->
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading2">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/PMBOK_ClassificaçãoQualidade-05.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Qualidade</h4>
                    <p class="textAlignCenter">Padrão de aceitação.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading3">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/PMBOK_ClassificaçãoPrazo-03.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Prazo</h4>
                    <p class="textAlignCenter">(cronograma) Tempo necessário.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"><!-- divisao em cols-->
                  <div class="panel panel-default">
                  <div class="" role="tab" id="heading4">
                    <h4 class="panel-title">
                    <a class="textAlignCenter" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapseOne">
                      <img src="../include/img/icons/PMBOK_ClassificaçãoEscopo-04.png" alt="" border="0" class="" />
                    </a>
                    </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body">
                    <h4 class="subTitulo textAlignCenter">Escopo</h4>
                    <p class="textAlignCenter">Área de abrangência.</p>
                    </div>
                  </div>
                  </div>
                  </div>
                  </div>
                  <div class="clear"></div>
                  <p>Os riscos podem afetar qualquer categoria ou dimensão dos objetivos organizacionais.</p>
                  <p>Não há como falar em risco sem visualizar o objetivo, que poderá ser afetado caso o evento de risco ocorra. Isso tendo em vista que o risco só pode ser identificado se mantivermos o foco no objetivo a ser atingido pela organização.</p>
                  <p>Assim, o risco sempre está associado a um objetivo, expondo-o à inviabilização ou ao desvio de seu rumo.</p>
                  <p>A ISO 31000 destaca que “Organizações de todos os tipos e tamanhos enfrentam influências e fatores internos e externos que tornam incerto se e quando elas atingirão seus objetivos”. Assim, qualquer atividade que a organização desenvolva envolverá direta ou indiretamente riscos.</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina5.php', 'aula4pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


