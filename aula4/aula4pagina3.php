<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Normativos', 'exibir', '4','3', '20', 'aula4pagina2.php', 'aula4pagina4.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h4 class="subTitulo">1.1.  NORMATIVOS</h4>
                <p>No nosso estudo, utilizaremos como referencial a <a href="http://www.iso.org/iso/home/standards/iso31000.htm" target="_blank" title="acesse a iso 31000:2009">ISO 31000:2009, Risk management</a>, norma orientadora e reguladora que introduz o padrão mundial em Gestão de Riscos, aplicável a entidades de todos os tamanhos e tipos. A ISO 31000 é a principal norma em Gestão de Risco, seja esse ambiental, financeiro, operacional, etc. No Brasil, a norma foi traduzida e publicada pela ABNT em 30 de novembro de 2009, como a norma brasileira <a href="http://www.abntcatalogo.com.br/" target="_blank" title="link para abnt iso 31000">ABNT NBR ISO 31000</a>.                 
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
                    <h4 class="subTitulo">ABNT - ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS</h4>
                    <p>A ABNT é uma entidade privada e sem fins lucrativos, fundada em 28 de setembro de 1940. É membro fundador da International Organization for Standardization (Organização Internacional de Normalização - ISO), da Comisión Panamericana de Normas Técnicas (Comissão Pan-Americana de Normas Técnicas - Copant) e da Asociación Mercosur de Normalización (Associação Mercosul de Normalização - AMN). Desde a sua fundação, é também membro da International Electrotechnical Commission (Comissão Eletrotécnica Internacional - IEC). A ABNT é responsável pela publicação das Normas Brasileiras (ABNT NBR), elaboradas por seus Comitês Brasileiros (ABNT/CB), Organismos de Normalização Setorial (ABNT/ONS) e Comissões de Estudo Especiais (ABNT/CEE). As normas da ABNT podem ser compradas diretamente no sítio da Entidade.</p>
                    <p>Fonte: <a href="http://www.abnt.org.br/abnt/conheca-a-abnt" target="_blank" title="conheça a abnt">http://www.abnt.org.br/abnt/conheca-a-abnt</a></p>
                    <p>Acesso em 23/02/2015.</p>
                  </div>
                </div>
                <div class="clear"></div>
                <p>Também serão utilizados conceitos do modelo proposto pelo <i>Committee of Sponsoring Organizations of the Treadway Commission</i>, <a href="http://www.coso.org/documents/COSO_ERM_ExecutiveSummary_Portuguese.pdf" target="_blank" title="">COSO II</a>, do <i>Project Management Body of Knowledge</i>, <a href="http://www.pmi.org/PMBOK-Guide-and-Standards.aspx" target="_blank" title="acesse o pmbok">PMBOK</a>, da <i>International Organization of Supreme Audit Institutions</i>, <a href="http://www.intosai.org/issai-executive-summaries/view/article/intosai-gov-9130-guidelines-for-internal-control-standards-for-the-public-sector-further-inf.html" target="_blank" title="link para intosai">INTOSAI</a>, da norma GOV 9130/ 2007, entre outras referências.</p>
                <div class="espacamentoBottom">
                  <div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="cerebro" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-1">
                    <h4 class="subTitulo">COSO</h4>
                    <p>COSO - Committee of Sponsoring Organizations of the Treadway Commission é uma organização norte-americana voluntária do setor privado, dedicada à melhoria da qualidade dos relatórios financeiros de forma que reflitam ética no negócio, controles internos eficazes e governança corporativa. O COSO foi composto originalmente em 1985 para assessorar a “National Commission on Fraudulent Financial Reporting”, através de uma iniciativa independente do setor privado que deve estudar os motivos que estimulam a fraude nos relatórios financeiros e produzir recomendações para as empresas públicas, suas respectivas auditorias independentes, para a SEC (Securities and Exchange Commission), outros órgãos reguladores e para instituições educacionais.</p>
                    <p>Fonte: <a href="http://www.coso.org" target="_blank" title="conheça o coso">www.coso.org</a> e Glossário de Riscos do Banco central do Brasil.</p>
                    <h4 class="subTitulo">PMBOK</h4>
                    <p>O Guia Project Management Body of Knowledge (PMBOK) é um conjunto de práticas na gestão de projetos organizado pelo instituto PMI e é considerado a base do conhecimento sobre gestão de projetos por profissionais da área.</p>
                    <p>Fonte: <a href="http://pt.wikipedia.org/wiki/Project_Management_Body_of_Knowledge" target="_blank" title="conheça o pmbok">http://pt.wikipedia.org/wiki/Project_Management_Body_of_Knowledge</a></p>                    
                    <p>Acesso em 25/01/2015.</p>
                    <h4 class="subTitulo">INTOSAI</h4>
                    <p>A INTOSAI foi fundada em 1953 por 34 países, entre eles o Brasil. A organização trabalha no sentido de promover o intercâmbio de informações e de experiências sobre os principais desafios enfrentados pelas EFS (Entidades Superiores de Fiscalização p. ex. TCU no Brasil) no desempenho de suas funções. O Tribunal de Contas da União é membro do Comitê de Normas Profissionais, do Grupo de Trabalho de Tecnologia da Informação, do Grupo de Trabalho de Auditoria Ambiental e do Grupo de Trabalho de Privatização, Regulação Econômica e Parceria Público Privada. Além disso, o Tribunal preside o Subcomitê de Auditoria de Desempenho, pertencente ao Comitê de Normas Profissionais.</p>
                    <p>Fonte: <a href="http://portal3.tcu.gov.br/portal/page/portal/TCU/relacoes_institucionais/relacoes_internacionais/organizacoes_internacionais/Not%C3%ADcia%20-%20Conhe%C3%A7a%20a%20Intosai.pdf" target="_blank" title="conheça o intosai">http://portal3.tcu.gov.br/portal/page/portal/TCU/relacoes_institucionais/relacoes_internacionais/organizacoes_internacionais/Not%C3%ADcia%20-%20Conhe%C3%A7a%20a%20Intosai.pdf</a></p>                    
                    <p>Acesso em 25/01/2015.</p>
                  </div>
                </div>
                <div class="clear"></div>
                <p>A seguir, alguns exemplos de modelos referenciais afetos à teoria dos riscos:</p>
                 <div class="col-lg-6 col-md-6 col-sm-6">
                  <p class="textAlignCenter"><img src="../include/img/aulas/Imagem_ModeloCOSO.png" alt="" style="max-width:320px;padding:0px;" /></p>
                  <p class="textAlignCenter fonteMenor">Figura 2: Modelo <span class="semi-bold">COSO ERM</span></p>
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-6">
                  <p class="textAlignCenter"><img src="../include/img/aulas/Imagem_ModeloCOBIT.png" alt="" style="max-width:320px;padding:0px;" /></p>
                  <p class="textAlignCenter fonteMenor">Figura 3: Modelo <span class="semi-bold">COBIT</span></p>
                 </div>
                 <div class="clear"><br /></div>
                 <div class="col-lg-6 col-md-6 col-sm-6">
                  <p class="textAlignCenter"><a href="../include/img/aulas/Imagem_ModeloISO31000.png" target="_blank" title=""><img src="../include/img/aulas/Imagem_ModeloISO31000.png" alt="" style="max-width:320px;padding:0px;" /></a></p>
                  <p class="textAlignCenter fonteMenor">Figura 4: Processo de Gestão de Riscos - Modelo ISO 31.000</p>
                 </div>
                 <div class="col-lg-6 col-md-6 col-sm-6">
                  <p class="textAlignCenter"><a href="../include/img/aulas/Imagem_ModeloPMBOK.png" target="_blank" title=""><img src="../include/img/aulas/Imagem_ModeloPMBOK.png" alt="" style="max-width:320px;padding:0px;" /></a></p>
                  <p class="textAlignCenter fonteMenor">Figura 5: Resumo do gerenciamento dos riscos do projeto - Modelo <span class="semi-bold">PMBOK</span></p>
                 </div>
                 <div class="clear"></div>

                
                <p>O COSO desenvolveu dois modelos (TCU, 2009, p. 12). O primeiro foi o COSO I, referência para auxiliar empresas e outras organizações a avaliar e aperfeiçoar seus <span class="semi-bold">sistemas de controle interno</span>. Essa estrutura foi incorporada em políticas, normas e regulamentos adotados por milhares de organizações para controlar melhor suas atividades visando ao cumprimento dos objetivos estabelecidos.</p>
                <p>O segundo modelo, o COSO ERM ou COSO II, <i>Enterprise Risk Management – Integrated Framework</i> (Gerenciamento de Riscos Corporativos – Estrutura Integrada), nasceu para servir como modelo de estratégia de fácil utilização pelas organizações para avaliar e melhorar o próprio <span class="semi-bold">gerenciamento de riscos</span>.</p>
                <p>Abaixo, a estrutura integrada dos modelos COSO:</p>
                <p class="textAlignCenter"><a href="../include/img/aulas/Imagem_1.1_EstruturaIntegradaCOSO.jpg" target="_blank" title=""><img src="../include/img/aulas/Imagem_1.1_EstruturaIntegradaCOSO.jpg" alt="" style="max-width:400px;padding:0px;" /></a></p>
                <p class="textAlignCenter fonteMenor">Figura 6 - Modelos de Referência - COSO e COSO ERM (Secretaria de Controle Interno STF. Apresentação – Gestão de Riscos)</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina2.php', 'aula4pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


