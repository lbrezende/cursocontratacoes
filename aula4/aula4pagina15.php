<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Tratamento de Riscos', 'exibir', '4','15', '20', 'aula4pagina14.php', 'aula4pagina16.php', '<h4 style="font-weight:bold">Riscos Corporativos</h4>');
?> 

 <article id="aula" accesskey="2">
       <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="grid simple espaçamento">
              <div class="grid-body no-border pt-20">
                <h3 class="titulo">5.  TRATAMENTO DE RISCOS</h3>
                <p>De acordo com a ISO 31000, o tratamento de riscos “é o processo de modificar um risco”. Mais precisamente, por meio desse tratamento, selecionam-se opções e medidas para serem implementadas a fim de modificar os níveis de risco.</p>
                <p class="subTitulo">Mas como modificar um risco tendo em vista a incerteza atribuída a eles?</p>
                <p>O grau de incerteza dos riscos não inviabiliza o seu tratamento. O que se busca é estabelecer uma resposta que seja adequada para modificar a probabilidade ou a consequência de um risco. Esse tratamento pode ser implementado de diversas maneiras, entre as quais (Neto, et al. 2014):</p>
                <ul>
                  <li>Remoção da fonte de risco - tornar a fonte de risco isolada ou nula.</li>
                  <li>Alteração da probabilidade – criar mecanismo que possa diminuir a chance de o risco ocorrer.</li>
                  <li>Alteração das consequências – buscar meios que resguardem o objetivo ou diminuam o impacto da ocorrência do risco.</li>
                  <li>Compartilhamento do risco com outra parte – fazer, por exemplo, um seguro.</li>
                  <li>Evitar o risco pela decisão de não iniciar ou descontinuar a atividade.</li>
                </ul>
                <p>As medidas a serem implantadas no tratamento a riscos deverão ser analisadas no contexto de cada organização, de modo a avaliar a pertinência e adequação de cada medida em relação aos objetivos a serem protegidos. Assim, a Administração deve avaliar:</p>
                <div class="paddingBottom300">
                  <p class="abreCaixa" id="0"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                  <div id="caixa-0">
                    <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Se o resultado do tratamento proposto será efetivo.</p>
                    <p class="abreCaixa" id="1"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                    <div id="caixa-1">
                      <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Se os custos envolvidos no tratamento compensam em relação aos benefícios a serem auferidos, ou seja, se o tratamento é economicamente viável.</p>
                      <p class="abreCaixa" id="2"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                      <div id="caixa-2">
                        <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Se o efeito de cada resposta realmente irá reduzir a probabilidade e o impacto do risco.</p>
                        <p class="abreCaixa" id="3"><a href="javascript:void(0);" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
                        <div id="caixa-3">
                          <p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Se há riscos secundários que podem ser gerados pelo tratamento.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <p>É altamente conveniente designar um responsável pelo acompanhamento e pela(s) resposta(s) ao tratamento. É o chamado proprietário do risco.</p>
                <p>No tratamento de riscos, novos controles podem ser criados ou controles já existentes poderão ser remodelados para atender ao processo de gestão de riscos impulsionado pela administração do órgão.</p>
                <h4 class="subTitulo">5.1.  RISCO INERENTE OU RISCO BRUTO E RISCO RESIDUAL</h4>
                <p>O <span class="semi-bold">risco inerente</span> é o risco do negócio, processo ou atividade antes de qualquer tratamento ou controle. É o risco natural, aquele em que nenhuma ação foi efetuada para alterar a probabilidade de ocorrência ou de impacto. Também é denominado “<span class="semi-bold">risco bruto</span>". De acordo com o nível de risco, ele poderá estar acima do apetite a risco da organização (NETO, et al. 2014).</p>
                <p>O <span class="semi-bold">risco residual</span> é o risco remanescente, após a adoção de medidas de proteção (ABNT ISO/IEC - Guia 73:2005). É aquele resultante do processo de tomada de ações e aplicação das melhores práticas de controles internos ou da resposta da organização ao risco (IBGC, 2007, p.24). </p>
                <p>Para que a avaliação do risco esteja completa, o efeito dos controles sobre os riscos inerentes deverão ser detalhados. Um bom controle reduz o risco residual, em regra. Assim:</p>
                <p class="textAlignCenter subTitulo">Risco Inerente - Efeito do Controle = Risco Residual</p>
              </div>
            </div>   
           </div>
        </div>
    </article>    

<?php  configNavegacaoRodape('exibir', 'aula4pagina14.php', 'aula4pagina16.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


