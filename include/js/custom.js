$(document).ready(function() {		

	$("[id|='destaque']").hide();
	$('.cerebro').click(function(){
		$('#esconder').hide("slow");
		$('#destaque-1').show('slideDown');

	});
	
	$('.saibaMais').click(function(){
		$('#esconder-SaibaMais').hide("slow");
		$('#destaque-SaibaMais-1').show('slideDown');

	});

	$( "[name|='conteudo']" ).hide();
	$('.itemmenu').click(function(){
		var name = $(this).attr('name'); //pega o name para completar no conteúdo
		$( "[name|='conteudo']" ).attr('style','display:none');
		//$( "[name|='conteudo']" ).hide(); //oculta todos os outros conteúdos para exibir apenas o próximo
		$('#conteudo-'+name).attr('style','display:block'); //exibe o conteúdo clicado baseado no name do elemnto de onde partiu o clique
		//$('#conteudo-'+name).show(); //exibe o conteúdo clicado baseado no name do elemnto de onde partiu o clique
		$('.itemmenu').removeClass('selected'); //remove todas as classes selected dos itens de menu
		$('#'+name).addClass('selected'); //inclui a classe de seleção somente no item selecionado
	});
	
	$("[id|='caixa']").hide();
	$('.abreCaixa').click(function(){
		var name = $(this).attr('id');
		//alert(name);
		$('#'+name).hide();
		$('#caixa-'+name).toggle('slideDown');	
	});
	
	$('.abreMais').click(function(){
		var name = $(this).attr('id');
		$('#esconder-'+name).hide();
		$('#caixa-'+name).toggle('slideDown');	
	});
	
    $("#ir-menu").click(function(){
      $("#menu").focus();
     });
    $("#ir-aula").click(function(){
      $("#aula").focus();
     });     	

    $("[rel='popover']").popover({
		html:true, 
		trigger: 'focus',
		placement: 'auto right',
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content referencia"></div></div>'
	});

	$(".diminuirFonte").click(function(){
		$('article').css('font-size','16px');
		$('.fonteMenor').css('font-size','14px');
		$('.referencia p').css('font-size','16px !important');
		$("[rel=popover]").attr("data-size","popover-small");
		$('.titulo').css('font-size','20px !important');
		$('.subTitulo').css('font-size','16px');
	})

	$(".aumentarFonte").click(function(){
		$('article').css('font-size','30px');
		$('.fonteMenor').css('font-size','24px');
		$('.referencia p').attr('css','font-size: 30px !important');
		$('.referencia p').addClass('referenciaGrande');
		$("[rel=popover]").attr("data-size","popover-big");
		$('.titulo').css('font-size','34px');
		$('.subTitulo').css('font-size','30px');
		$('.btn-primary').css('font-size','30px');
		$('.btn-success').css('font-size','30px');
		$('.modal').css('font-size','30px');
		$('i').css('font-size','30px');
		$('li').css('line-height','35px');

	})

	if ($("#acessibilidade").html() == "sim") {
		$(".aumentarFonte").click();
	};
	
 });