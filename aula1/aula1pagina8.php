<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Identificando Problemas nas Contratações Públicas', 'exibir', '1','8', '12', 'aula1pagina7.php', 'aula1pagina9.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
							<div class="paddingTop20">
								<div class="col-lg-6 col-md-6 col-sm-6" id="esconder-SaibaMais" ><p class="textAlignRight">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p></div>
								<div class="clear"></div>
								<div class="col-lg-12 col-md-12 col-sm-12" id="destaque-SaibaMais-1">
									<div class="col-lg-8 col-md-8 col-sm-8" ><p class="textAlignCenter tituloSaibaMais" style="padding:0px;margin:0px;">Por que há tantos problemas nas contratações públicas? Todos nós 
concordamos que essas são as questões de maior destaque?
</p><hr class="linhaInterrogacao" /></div>
									<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
									<div class="clear"></div>
									<p>Essas perguntas, certamente, não possuem resposta única, tendo em vista que os problemas detectados na complexidade das contratações públicas decorrem também de uma variedade de deficiências em várias etapas dos procedimentos condutores das demandas.</p>
									<p>Nesse contexto de problemas, destaca-se a frequente atribuição da burocracia como grande vilã da eficiência do setor público.</p>
									<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
									<div  class="bordaPalavraAutor">
										<p>A meu sentir, a burocracia excessiva e desarrazoada, por si só, realmente mina a celeridade e a eficiência da Administração Pública. Essa é a burocracia que atrapalha. </p>
										<p>De outra sorte, a burocracia legal e sistematicamente necessária é inerente à Área Pública, tendo em vista que objetiva definir linhas hierárquicas e procedimentais, formalizando registros que são necessários e importantes para todo o processo administrativo. Esse tipo de burocracia é salutar na Administração Pública, pois possibilita o registro formal das atividades, o que, em última instância, dá garantia aos gestores acerca das decisões e das ações tomadas nos casos concretos. Para o gestor que age com correção e critério, o registro burocrático e necessário dos atos, ações e decisões só beneficia o seu trabalho.</p>
									</div>
									<p>A pergunta do momento seria:</p>
									<div class="col-lg-6 col-md-6 col-sm-6" id="esconder" ><p class="textAlignRight">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="cerebro" id="iconeSaibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p></div>
									<div class="clear"></div>
									<div class="col-lg-12 col-md-12 col-sm-12" id="destaque-1">
										<div class="col-lg-8 col-md-8 col-sm-8" ><p class="textAlignCenter tituloSaibaMais" style="padding:0px;margin:0px;">Sim, entendi tudo, mas quais são os obstáculos que atrapalham o 
desempenho do gestor público e como fazer a Administração Pública ser mais eficiente?
	</p><hr class="linhaInterrogacao" /></div>
										<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
										<div class="clear"></div>
										<p>Vamos construir essa ideia. Pensando...</p>
										<p>As causas/fontes de risco, se não trabalhadas, podem se transformar em problemas. São exemplos de causas nesse contexto: a ausência de planejamento detalhado; projeto básico/termo de referência mal elaborado; falha na publicação legal do edital; e a desclassificação indevida de um licitante durante o certame. Causas como essas podem gerar eventos e consequências danosas ao bom andamento dos processos licitatórios e à imagem do órgão.</p>
										<p>Conforme será visto durante este treinamento, a aplicação de algumas ações já pacificadas na área de TI minimizará a ocorrência desses problemas e, consequentemente, favorecerá muito a melhoria da qualidade das contratações do setor público.</p>
									</div>
								</div>
								
							</div>
						</div>
					</div>   
				 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina7.php', 'aula1pagina9.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


