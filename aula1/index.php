<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
 if(isset($_GET["ac"])){
        $acessibilidade = $_GET["ac"];
    }

  $acessibilidadeTxt = null;
  if ($acessibilidade == "sim") { 
    $acessibilidadeTxt = "?ac=sim";
  }; 

configHeader('Bem-vindo', 'exibir', '1','1', '12', 'index.php', 'aula1pagina2.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<p class="semi-bold">Caros colegas,</p>
						
						
                            <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12">
								<p>Bom dia, boa tarde, boa noite. Sejam bem-vindos ao curso de “Planejamento das Contratações: buscando a gestão efetiva dos gastos públicos”. Para começar, que tal assistir ao vídeo “Estratégia, Jogo Incrível!”, que mostra uma interessante partida de Damas.</p>
								<p>No vídeo abaixo, poderemos observar como a estratégia é importante para alcançar objetivos. Observem, também, que um planejamento eficiente dá segurança e calma na fase de execução do projeto.</p>
								<p>Ao longo deste treinamento, você perceberá a ligação desse vídeo com as noções que serão trabalhadas.</p>
								<p>Vamos começar?</p>
							  </div>
                              <div class="col-lg-6 col-md-6 col-sm-6 col-lg-12">
                                <div class="video-container">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/v_XjJzYeDZg" frameborder="0" allowfullscreen></iframe>
                                </div><br><br>
                              </div>
                            </div>
                    </div>
                  </div>   
                </div>
            </article>    

            <footer>  
              <nav role="navigation">
                <div class="textAlignCenter">
                  <a href=<?php echo '"aula1pagina2.php'.$acessibilidadeTxt.'"';?> class="btn btn-lg btn-success" title="Ir para próxima página">Avan&ccedil;ar <i class="fa fa-arrow-circle-o-right">  </i></a>
                </div>
              </nav>
            </footer>  
          <!-- <?php  //configNavegacaoRodape('exibir', 'index.php', 'aula1pagina3.php'); //Rodapé automático aqui  ?>-->         
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


