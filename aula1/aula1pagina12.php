<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Referências Bibliográficas', 'exibir', '1','12', '12', 'aula1pagina11.php', '', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">SUGESTÕES DE LEITURA COMPLEMENTAR</h3>
						<p><span class="semi-bold">Artigo:</span></p>
						<p><i>Gestores gastam mais da metade de seu tempo em atividades de apagar fogo</i>. Consultoria empresarial ProGeps. Arquivo disponível na plataforma de ensino.
						<p><span class="semi-bold">Livros:</span></p>
						<p>CURY, Augusto. <i>O Código da Inteligência: Bons Profissionais e Excelentes Profissionais</i>. - Edição de Bolso. Pocket Ouro, 2008.</p>
						<p>CAVALCANTI, Augusto Sherman. <i>O novo modelo de contratação de soluções de TI pela Administração Pública</i>. Editora Fórum.</p>
						<h3 class="titulo">REFERÊNCIAS BIBLIOGRÁFICAS</h3>
						<p>AMATO, P. M. <i>Introdução à Administração pública</i>. Rio de Janeiro: Fundação Getúlio Vargas, 1958.</p>
						<p>BOMBIG, Alberto; OSAKABE, Marcelo; com GORCZESKI Vinicius. Artigo “<i>Modelo de gestão privada aumenta público de museus de São Paulo - As Organizações Sociais introduziram métodos de gestão privada na administração cultural. Viraram um caso de sucesso de público e crítica</i>”, abril, 2013. Disponível em <a href="http://revistaepoca.globo.com/ideias/noticia/2013/04/modelo-de-gestao-privada-aumenta-publico-de-museus-de-sao-paulo.html" target="_blank">http://revistaepoca.globo.com/ideias/noticia/2013/04/modelo-de-gestao-privada-aumenta-publico-de-museus-de-sao-paulo.html</a>. Acesso em 2/9/2014.</p>
						<p>BRASIL. Tribunal de Contas da União. <i>Guia de boas práticas em contratação de soluções de tecnologia da informação: riscos e controles para o planejamento da contratação / Tribunal de Contas da União</i>. – Versão 1.0. – Brasília: TCU, 2012.</p>
						<p>BRASIL. Tribunal de Contas da União. <i>Licitações e contratos: orientações e jurisprudência do TCU / Tribunal de Contas da União</i>. – 4. ed. rev., atual. e ampl. – Brasília: TCU, Secretaria Geral da Presidência: Senado Federal, Secretaria Especial de Editoração e Publicações, 2010. 910 p.</p>
						<p>BRASIL. Tribunal de Contas da União – TCU. Relatório de Auditoria TC 038.687/2012-7 GRUPO I – CLASSE V – PLENÁRIO. Disponível em <a href="http://www.tcu.gov.br/Consultas/Juris/Docs/judoc/Acord/20130322/AC_0605_09_13_P.doc" target="_blank">http://www.tcu.gov.br/Consultas/Juris/Docs/judoc/Acord/20130322/AC_0605_09_13_P.doc</a></p>
						<p>CAVALCANTI, Augusto Sherman. <i>O novo modelo de contratação de soluções de TI pela Administração Pública</i>. Belo Horizonte: Fórum, 2013.</p>
						<p>Constituição Federal Brasileira.</p>
						<p>CURY, Augusto. <i>O código da inteligência: bons profissionais e excelentes profissionais</i>. Edição de Bolso. Pocket Ouro, 2008.</p>
						<p>Chiavenato, Idalberto. <i>Introdução à teoria geral da Administração: uma visão abrangente da moderna administração das organizações</i>. Rio de Janeiro: Elsevier, 2003.</p>
						<p>EVENTO EXAME FÓRUM 2013 - Como aumentar nossa produtividade, promovido pela revista Exame em setembro de 2013, que contou com a participação do Ministro da Fazenda Guido Mantega, Ministro Joaquim Barbosa na condição de Presidente do- Supremo Tribunal Federal, Ministro Augusto Nardes como Presidente do Tribunal de Contas da União e Jorge Gerdau Johannpeter, Presidente do Conselho de Administração do grupo GERDAU. Também participaram do Fórum como debatedores empresários, executivos e economistas brasileiros e estrangeiros.</p>
						<p>FURTADO, Lucas Rocha. <i>Curso de licitações e contratos administrativos</i>. 4. ed. Belo Horizonte: Fórum, 2012.</p>
						<p>IN MP/SLTI Nº4/2010, art. 2º, inciso III. Disponível em: <a href="http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/instrucao-normativa-mp-slti-no04" target="_blank">http://www.governoeletronico.gov.br/sisp-conteudo/nucleo-de-contratacoes-de-ti/modelo-de-contratacoes-normativos-e-documentos-de-referencia/instrucao-normativa-mp-slti-no04</a>.</p>
						<p>MENEGOLLA, Maximiliano; Sant´Anna  Ilza. <i>Por que planejar? Como planejar?. - Currículo, área, aula</i>. Rio de Janeiro: Vozes, 2010.</p>
						<p>MOTTA, Paulo Roberto de Mendonça. Artigo – “<i>O estado da arte da gestão pública</i>”, fev. 2013. Disponível em <a href="http://www.scielo.br/scielo.php?pid=S0034-75902013000100008&script=sci_arttext" target="_blank">http://www.scielo.br/scielo.php?pid=S0034-75902013000100008&script=sci_arttext.</a> Acesso em 22/07/2015.</p>
						<p>NETO, Antonio Alves de Carvalho; LIMA, Maria Lúcia de Oliveira F.; NORONHA, Maridel Piloto; PALUMBO, Salvatore. Curso <i>GESTÃO DE RISCOS Princípios e Diretrizes</i>. Brasília-DF, 2014.</p>
						<p>Relatório de Auditoria de Gestão, Supremo Tribunal Federal. Prestação de Contas Anual, Exercício 2012.</p>
						<p>Relatório de Auditoria de Gestão, Supremo Tribunal Federal. Prestação de Contas Anual, Exercício 2013.</p>
						<p>REVISTA EXAME. <i>Brasil já tem bons exemplos de eficiência pública e privada</i>, out. 2013. Disponível em: <a href="http://exame.abril.com.br/revista-exame/edicoes/1051/noticias/ja-temos-os-bons-exemplos" target="_blank">http://exame.abril.com.br/revista-exame/edicoes/1051/noticias/ja-temos-os-bons-exemplos</a>. Acesso em 10/09/2014.</p>
						<p>TEIXEIRA, Alexandre. Artigo <i>Gestão pública à moda privada</i>, jun/2010. Disponível em <a href="http://epocanegocios.globo.com/Revista/Common/0,,EMI145255-16642,00-GESTAO+PUBLICA+A+MODA+PRIVADA.html" target="_blank">http://epocanegocios.globo.com/Revista/Common/0,,EMI145255-16642,00-GESTAO+PUBLICA+A+MODA+PRIVADA.html</a>. Acesso em 28/05/2015.</p>
						<p>VELASCO, Clara. <i>Entre 100 maiores cidades do país, 34 não têm plano de saneamento básico</i>, maio, 2014. Disponível em: <a href="http://g1.globo.com/brasil/noticia/2014/05/entre-100-maiores-cidades-do-pais-34-nao-tem-plano-de-saneamento-basico.html" target="_blank">http://g1.globo.com/brasil/noticia/2014/05/entre-100-maiores-cidades-do-pais-34-nao-tem-plano-de-saneamento-basico.html</a> Acesso em 12/08/2014.</p>                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina11.php', 'fim'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


