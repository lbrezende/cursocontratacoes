<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Administração Pública e Setor Privado', 'exibir', '1','3', '12', 'aula1pagina2.php', 'aula1pagina4.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">1.  ADMINISTRAÇÃO PÚBLICA E SETOR PRIVADO</h3>
						<p>Todos nós aprendemos no dia a dia um pouco de administração. Seja no lar, no trabalho, na família, ou até mesmo na organização de uma viagem.</p>
						<div class="espacamentoBottom">
								<div class="col-lg-6 col-md-6 col-sm-6" id="esconder-SaibaMais" ><p class="textAlignRight">Clique na imagem ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p></div>
								<div class="clear"></div>
								<div class="col-lg-12 col-md-12 col-sm-12" id="destaque-SaibaMais-1">
									<div class="paddingBottom20">
										<div class="col-lg-8 col-md-8 col-sm-8"><p class="textAlignCenter tituloSaibaMais"><br />Mas você conhece o conceito de administração?</p><hr class="linhaInterrogacao" /></div>
										<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
										<div class="clear"></div>
									</div>
									
									<p>Conceitualmente, administração “é a maneira de governar organizações ou parte delas. É o processo de <span class="semi-bold">planejar, organizar, dirigir e controlar</span> o uso de <span class="semi-bold">recursos</span> organizacionais para <span class="semi-bold">alcançar</span> determinados <span class="semi-bold">objetivos</span> de maneira <span class="semi-bold">eficiente e eficaz</span>.” (Chiavenato, 2003).</p>
									<p>O conceito de administrar é amplo e geral, perfeitamente aplicável às necessidades tanto do setor privado quanto do público. Em relação ao nosso tema, muito interessa a busca por práticas que desenvolvam a produtividade. </p>
									<p>Apesar de várias distinções entre os dois setores, também há muitas semelhanças. O que se compra por lá, também compramos por aqui...e aí por diante.</p>
									<p>De acordo com o Quinto EXAME Fórum 2013 - Como aumentar nossa produtividade - , o Brasil possui boas e más experiências relacionadas à eficiência no setor público e no setor privado. Nesse contexto, é necessária uma aproximação entre os dois setores para aumentar a produtividade no país, por meio da adoção de experiências bem sucedidas de cada setor.</p>
									<p>Os debatedores do Fórum 2013 concluíram que, para se tornar mais produtivo, o país precisa se modernizar e reduzir suas disparidades, a começar pelo setor público (REVISTA EXAME, 2013).</p>
									<p>Foi destacado, também, que “na média, um trabalhador americano produz o mesmo que cinco brasileiros. Nos últimos 30 anos, a produtividade brasileira cresceu à taxa média inferior a 2% ao ano, menos do que outros emergentes”. Apesar da comparação, vale lembrar que as características de infraestrutura de cada país são distintas e essa questão gera impactos na produtividade.</p>

								</div>
						</div>
						<div class="clear"></div>
						
						
                    </div>
                  </div>   
                </div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina2.php', 'aula1pagina4.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


