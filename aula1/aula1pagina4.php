<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Administração Pública e Setor Privado', 'exibir', '1','4', '12', 'aula1pagina3.php', 'aula1pagina5.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>Durante os diversos anos em que trabalhei no setor privado e, posteriormente, com as atividades desenvolvidas no setor público, observei várias práticas que auxiliam na melhoria da gestão. Entre as características da área privada que potencializam a sua administração, podemos citar:</p>
								<ul class="palavraAutor">
									<li><span class="semi-bold">Objetivos claros e definidos - </span>as estratégias são traçadas com base em metas a serem alcançadas.</li>
									<li><span class="semi-bold">Demandas bem delineadas, prioritariamente organizadas – </span>há grande preocupação em classificar as demandas segundo critérios de importância e urgência.</li>
									<li><span class="semi-bold">Agilidade nas decisões - </span>a estrutura hierárquica simplificada buscar acelerar a comunicação. A autonomia dos dirigentes também contribui nesse ponto.</li>
									<li><span class="semi-bold">Reconhecimento meritocrático – </span>a política de incentivos e de premiação, além de um plano de cargos e salários que possibilite o crescimento profissional a todos os empregados contribui para valorizar a eficiência.</li>
									<li><span class="semi-bold">Planejamento organizacional bem definido – </span>há grande preocupação em tornar os objetivos organizacionais claramente delimitados e difundidos.</li>
									<li><span class="semi-bold">Recursos bem dimensionados, normalmente alocados a projetos estratégicos – </span>tendo em vista a concorrência acirrada do setor privado, a correta alocação de recursos pode definir a sobrevivência da empresa.</li>
									<li><span class="semi-bold">Treinamento constante para as equipes – </span>o investimento frequente em treinamento busca tornar as empresas mais competitivas.</li>
									<li><span class="semi-bold">Pouca tolerância a erros pessoais – </span>alguns erros não são tolerados e podem levar à demissão do empregado, principalmente quando geram prejuízos financeiros ou à imagem da empresa.</li>
									<li><span class="semi-bold">Busca constante pela eficiência, que, normalmente, representa dinheiro para a empresa - </span>a eficiência para a empresa privada representa sobrevivência e consolidação de ganhos no mercado em que atua.</li>
									<li><span class="semi-bold">Clara noção de quem é o cliente – </span>há uma busca rotineira pela aproximação e identificação com o cliente.</li>
									<li><span class="semi-bold">Possibilidade de fazer, em regra, tudo o que a lei não proíbe – </span>o ordenamento jurídico aplicável às empresas privadas possui menor amplitude e isso facilita a atuação delas em relação à Administração Pública. As necessidades geradas por pressões de mercado e revoluções tecnológicas obrigam o setor privado a agir com rapidez e inovar.</li>
									
								</ul>
						</div>
						
                    </div>
                  </div>   
                </div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina3.php', 'aula1pagina5.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


