<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Objetivos desta aula', 'exibir', '1','2', '12', 'index.php', 'aula1pagina3.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">Objetivos desta aula</h3>
						<div class="col-lg-6 col-md-6 col-sm-6 ">
							<p>Prezados participantes,</p>
							<p>Nesta primeira aula, será apresentado a vocês o foco principal deste treinamento, com algumas informações sobre o caminho a ser trilhado.</p>
							<p>De início, desenvolveremos, de modo contributivo, noções gerais acerca da iniciativa privada e do setor público, destacando que, apesar de distintas, essas áreas podem possuir simetria entre operações que busquem a melhoria em seus processos de trabalho.</p>						
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 semPaddingLeft">
							<img src="../include/img/aulas/mapaMentalAula1Tela2.jpg" alt="" class="imgAulas" />
						</div>
						<div class="clear espacamentoLista"></div>
						<div class="espacamentoBottom">
								<div class="col-md-6 textAlignRight" ><a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img alt="" src="../include/img/icons/saibaMais.png" /></a></div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
									<p>Quer conhecer mais sobre mapas mentais? Acesse os links a seguir:</p>
									<ul> <li><a href="http://pt.wikipedia.org/wiki/Mapa_mental" title="conheça mais sobre o mapas mentais" target="_blank">http://pt.wikipedia.org/wiki/Mapa_mental</a></li>
										 <li><a href="http://freemind.sourceforge.net/wiki/index.php/Main_Page" title="conheça mais sobre o Freemind para mapas mentais" target="_blank">http://freemind.sourceforge.net/wiki/index.php/Main_Page</a></li>
									</ul>
								</div>
						</div>
						<div class="clear"></div>
						<p>Ao final desta aula, você deverá ser capaz de:</p>
						<div class="paddingBottom300">
							<p class="abreCaixa" id="0"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
							<div id="caixa-0">
								<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" />compreender que a busca pelo melhor uso dos recursos existentes, físicos, humanos e tecnológicos em prol da eficiência é, hoje em dia, um ideal a ser atingido tanto pelo setor privado quanto pela Administração Pública;</p>
								<p class="abreCaixa" id="1"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
								<div id="caixa-1">
									<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> identificar e listar os problemas que normalmente ocorrem nas contratações públicas;</p>
									<p class="abreCaixa" id="2"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
									<div id="caixa-2">
										<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> compreender e entender a relevância da estrutura burocrática no processo de contratação.</p>
									</div>
								</div>
							</div>
						</div>						
                    </div>
                  </div>   
                </div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'index.php', 'aula1pagina3.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


