<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Identificando Problemas nas Contratações Públicas', 'exibir', '1','7', '12', 'aula1pagina6.php', 'aula1pagina8.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">3. IDENTIFICANDO PROBLEMAS NAS CONTRATAÇÕES PÚBLICAS</h3>
						<p>Apesar de a Administração Pública estar prioritariamente regida por leis, existem diversas ações dos gestores de contratos, que se mal justificadas ou mal planejadas, podem levar a sérios problemas nas contratações e no acompanhamento dos contratos. Diante disso, devemos identificar os principais pontos que podem gerar dificuldades nas contratações públicas para tentar evitá-los e, com isso, ganhar celeridade nos processos.</p>
						<div class="espacamentoBottom">
								<div class="col-lg-6 col-md-6 col-sm-6" id="esconder-SaibaMais" ><p class="textAlignRight">Clique no ícone ao lado&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" class="saibaMais" id="iconeSaibaMais" title="clique aqui para saber mais"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></a></p></div>
								<div class="clear"></div>
								<div class="col-lg-12 col-md-12 col-sm-12" id="destaque-SaibaMais-1">
									<div class="col-lg-8 col-md-8 col-sm-8" ><p class="textAlignCenter tituloSaibaMais"><br />Vamos então ao primeiro passo? Como podemos realmente saber o que é um problema?</p><hr class="linhaInterrogacao" /></div>
									<div class="col-lg-4 col-md-4 col-sm-4"><img width="76px" height="68px" alt="" src="../include/img/icons/interrogacao.png" /></div>
									<div class="clear"></div>
									<p>Para auxiliar, utilizaremos uma abordagem contemporânea relacionada ao estudo de riscos, e que será apresentada nos módulos seguintes.</p>
									<div  style="border-color:red;border-width:2px;border-style:dashed;padding:5px;margin-top:20px;margin-bottom:20px;">
										<p><span style="color:red" class="semi-bold">Problema</span> é a situação ou condição existente que afeta no momento presente os objetivos de um indivíduo, grupo de pessoas, projeto ou organização e que requer uma solução. É a diferença entre situação desejada e situação real.</p>
										<p>Um risco, quanto se concretiza (isto é, quando o evento de risco ocorre), torna-se um problema (NETO, et al. 2014).</p>
									</div>
									<p>É comum ouvirmos falar em problemas nas contratações públicas. A todo momento, observamos na mídia notícias envolvendo fraudes, má aplicação ou desperdício de recursos dos cofres do Estado. </p>
									<p>Recentemente, após divulgação pela imprensa de que boa parte das cidades brasileiras apresenta problemas de saneamento básico (VELASCO, 2014), o Governo Federal registrou: “o problema não é a falta de dinheiro e sim a falta de projetos e de planejamento das obras necessárias”. </p>
									<p>Em outra situação, o Tribunal de Contas da União – TCU – constatou, em seu Relatório de Auditoria TC 038.687/2012-7 Grupo I – Classe V – Plenário, que mais de 700 quilômetros da ferrovia Norte-Sul apresentam problemas e, entre outros defeitos, não suportariam o peso necessário para o transporte de cargas. O valor do contrato de construção para o lote auditado é de mais de 290 milhões de reais. Para minimizar o problema, os trens terão que trafegar com volume menor de carga e em velocidades inferiores ao normal. </p>
									<p>A ineficiência na execução de obras públicas gera prejuízos de bilhões para o Brasil. De acordo com estudos do Tribunal de Contas da União, mais da metade das obras não possuem projetos básicos ou executivos, etapas que antecedem a execução de um empreendimento. </p>
									<p>Para o TCU, os danos decorrem de uma absoluta falta de planejamento<a href="javascript:void(0);" rel="popover" data-content="<p>EVENTO EXAME FÓRUM 2013 - Como aumentar nossa produtividade, promovido pela revista Exame em  setembro de 2013, que contou com a participação do Ministro da Fazenda Guido Mantega, Ministro Joaquim Barbosa na condição de Presidente do Supremo Tribunal Federal, Ministro Augusto Nardes como  Presidente do Tribunal de Contas da União e Jorge Gerdau Johannpeter, Presidente do Conselho de Administração do grupo GERDAU. Também participaram do Fórum como debatedores empresários, executivos e economistas brasileiros e estrangeiros.</p>" data-toggle="popover" data-size="popover-small"><sup>3</sup></a>. Os problemas enfrentados nas obras também se estendem para as compras e as contratações de serviços, tendo em vista que decorrem de deficiências nas fases iniciais de planejamento das contratações.</p>
									<p>Exemplos de que as coisas não andam como deveriam... Entre os entraves mais notórios nas questões relativas às contratações do setor público, podemos citar:</p>
									<div class="paddingBottom300">
										<p class="abreCaixa" id="0"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
										<div id="caixa-0">
											<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Licitações que não vingam.</p>
											<p class="abreCaixa" id="1"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
											<div id="caixa-1">
												<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Gastos excessivos.</p>
												<p class="abreCaixa" id="2"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
												<div id="caixa-2">
													<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Problemas na execução de contratos.</p>
													<p class="abreCaixa" id="3"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
													<div id="caixa-3">
														<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Falta/ineficiência no planejamento das despesas.</p>
														<p class="abreCaixa" id="4"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
														<div id="caixa-4">
															<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Baixo conhecimento técnico acerca do objeto da demanda.</p>
															<p class="abreCaixa" id="5"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
															<div id="caixa-5">
																<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Critério inadequado para alocação de orçamento.</p>
																<p class="abreCaixa" id="6"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
																<div id="caixa-6">
																	<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Deficiências na estimativa de preços.</p>
																	<p class="abreCaixa" id="7"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
																	<div id="caixa-7">
																		<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Falta de intercâmbio com outros órgãos.</p>
																		<p class="abreCaixa" id="8"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
																		<div id="caixa-8">
																			<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Baixa motivação dos servidores.</p>
																			<p class="abreCaixa" id="9"><a href="javascript:void(0)" alt=""><img src="../include/img/icons/cliqueAqui.png" alt="clique aqui" border="0" /></a>Clique aqui</p>
																			<div id="caixa-9">
																				<p><img src="../include/img/icons/checked.png" alt="Texto lido" border="0" class="marginRight10px" /> Burocracia excessiva.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>						
									

								</div>
						</div>
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina6.php', 'aula1pagina8.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


