<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Administração Pública e Setor Privado', 'exibir', '1','5', '12', 'aula1pagina4.php', 'aula1pagina6.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 
			<article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<p style="margin-top:10px;">Muitas das práticas do setor privado são executadas atualmente na Administração Pública. Mas essa postura não é recente. De acordo com Amato (1958):</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">As ideias sobre uma Administração Pública eficiente avançaram com a Revolução Industrial e com o enfraquecimento dos poderes aristocratas e absolutistas. No século XVIII, por causa de uma incipiente burocracia pública, já havia, na Prússia, preocupações de gestão centradas no controle, nas finanças públicas e na comunicação das ordens públicas. Nesse mesmo período, a experiência prussiana levou não só à criação dos primeiros cursos de Administração Pública como também à proposta de uma nova ciência do cameralismo ou da Administração.</p>
						</div>
						<div class="clear"></div>
						<p>Para o professor Motta (2013), “Cabe conciliar lições aprendidas no meio privado com o meio público, respeitando, porém, características inerentes à Administração Pública”.</p>
						<p>Nesse sentido, o empresário gaúcho Jorge Gerdau Johannpeter criou o Movimento Brasil Competitivo (MBC)<a href="javascript:void(0);" rel="popover" data-content="<p>Conheça o Movimento Brasil Competitivo em: <a href='http://www.mbc.org.br/mbc/novo/index.php' target='_blank' title='acesse o site do MBC clicando aqui'>http://www.mbc.org.br/mbc/novo/index.php</a></p>" data-toggle="popover" data-size="popover-small"><sup>1</sup></a>   para transplantar práticas da gestão privada no universo da gestão pública. Uma das preocupações do MBC é evitar que avanços na gestão se percam com a troca de poder (TEIXEIRA, 2010).</p>
						<p>Nas palavras do Presidente do Conselho Superior do MBC em 2014, Elcio Anibal de Lucca:</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">Ter um setor público mais produtivo e empresas qualificadas é tornar o país competitivo e, portanto, sendo o país mais competitivo, mais e mais empresas serão competitivas e vice-versa. É um processo bidirecional que, como resultado final, leva a uma melhoria da qualidade de vida dos cidadãos e que, no final, também é um fator de competitividade.</p>
						</div>
						<div class="clear"></div>
						<p>Nos últimos anos, a Administração Pública vem evoluindo sua gestão por meio de ações de Governança<a href="javascript:void(0);" rel="popover" data-content="<p>De acordo com o Referencial Básico de Governança do TCU, 2013, Governança no setor público “compreende essencialmente os mecanismos de liderança, estratégia e controle postos em prática para avaliar, direcionar e monitorar a atuação da gestão, com vistas à condução de políticas públicas e à prestação de serviços de interesse da sociedade”. Disponível no portal do TCU e na biblioteca do curso.</p>" data-toggle="popover" data-size="popover-small"><sup>2</sup></a>. Felizmente essa evolução está sendo premiada e reconhecida por meio de práticas que valorizam a atuação dos órgãos e dos servidores públicos.</p>
						<p>Um exemplo disso é o Observatório da Despesa Pública - ODP - da Controladoria-Geral da União, que já conquistou diversos prêmios, entre eles, o United Nations Public Service Awards 2011, categoria Avançando na Gestão do Conhecimento Governamental. O United Nations Public Service Awards é o mais prestigioso reconhecimento internacional de excelência no serviço público dado pela Organização das Nações Unidas - ONU.</p>
						<div class="espacamentoBottom">
							<div class="col-md-6 textAlignRight" ><button type="button" class="saibaMais" id="iconeSaibaMais"><img alt="" src="../include/img/icons/saibaMais.png" /></button></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique no ícone ao lado</p></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
								<p class="titulo">Observatório da Despesa Pública - ODP</p>
								<p>Criado em dezembro de 2008 com o objetivo de aplicar metodologia científica, apoiada em tecnologia da informação, na produção de conhecimentos capazes de subsidiar e acelerar a tomada de decisões estratégicas por parte dos gestores públicos.</p>
								<p>Saiba mais <a href="http://www.cgu.gov.br/assuntos/informacoes-estrategicas/observatorio-da-despesa-publica/o-que-e/o-que-e" target="_blank">aqui</a>.</p>
							</div>
						</div>
						<div class="clear"></div>
						<p>Bons gestores também estão sendo premiados com base em sistemas de reconhecimento e capacitação, tendo em vista o cumprimento de metas e a análise de indicadores de desempenho e avaliações de 360 graus. Por meio de avaliações dessa natureza, em 2008, a Prefeitura de Porto Alegre premiou os 15 funcionários com melhores resultados no Sistema de Reconhecimento e Capacitação. Esse grupo foi enviado a Washington para um curso de gestão pública com duas semanas de duração na Universidade George Washington. Na ocasião, os merecedores puderam conhecer o Banco Mundial e o Banco Interamericano de Desenvolvimento – BID (TEIXEIRA, 2010).</p>
						<p>Outro exemplo de boa gestão é a Orquestra Sinfônica do Estado de São Paulo (Osesp), que é hoje uma referência, tendo em vista que passou a ser gerida como uma empresa privada. Atualmente, do orçamento anual da Orquestra (R$ 87 milhões), 64% vem do governo do Estado de São Paulo e os 36% restantes vêm de patrocínios, doações e projetos incentivados, além das verbas obtidas com vendas de ingresso e locação de espaços (BOMBIG, et al. 2013). </p>
						<p>A mudança adveio da implantação de um novo modelo de gestão, adotado com sucesso também em outros países, como Holanda e Áustria, com referência na iniciativa privada e mais distante da burocracia estatal. Esse modelo hoje está em vigor em importantes órgãos culturais paulistas – quase todos, sucesso de crítica e, principalmente, de público.</p>
						<p>Práticas dessa natureza favorecem o <span class="semi-bold">fortalecimento da gestão</span>, pois incentivam a criatividade e a eficiência das pessoas e das organizações.</p>
						<p>Dessa forma, observamos que tanto a iniciativa privada quanto o setor público buscam alocar recursos de modo a executar projetos que revertam em benefícios para a empresa, órgão, entidade ou sociedade. Essa é a ideia: por meio do intercâmbio, minimizar rapidamente as diferenças e copiar bons exemplos já existentes em cada setor.</p>
						
                    </div>
                  </div>   
                </div>
            </article>    <?php  configNavegacaoRodape('exibir', 'aula1pagina4.php', 'aula1pagina6.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


