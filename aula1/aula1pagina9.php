<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Justificar os Investimentos: Grande Questão para os Gestores', 'exibir', '1','9', '12', 'aula1pagina8.php', 'aula1pagina10.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">4.  JUSTIFICAR OS INVESTIMENTOS: GRANDE QUESTÃO PARA OS GESTORES</h3>
						<p>No paradigma brasileiro de desenvolvimento, o poder público exerce o papel de uma locomotiva responsável por impulsionar e mover boa parte da economia brasileira. Nesse contexto, os investimentos de responsabilidade do setor público tornam-se essenciais para o desenvolvimento de nosso país.</p>
						<p>Assim, é desejável que cada centavo de dinheiro público desembolsado sempre esteja associado à contrapartida de retorno para a sociedade. Entretanto, a tarefa de justificar os valores aplicados em cada projeto nem sempre é fácil para os gestores públicos. Nesse ponto, registre-se que os órgãos de controle sempre estão atentos às justificativas apresentadas. </p>
						<p>Falando especificamente dos investimentos de TI, crescentes significativamente a cada ano, o cenário é bem delicado. Além dos aspectos técnicos individuais da demanda, também o contexto organizacional deve sempre ser considerado com atenção, pois uma solução de TI que foi implantada com sucesso em um órgão poderá não ser bem sucedida em outro. </p>
						<p>Por exemplo, o Órgão A, que possui um bom nível de maturidade na governança corporativa de TI<a href="javascript:void(0);" rel="popover" data-content="<p>Governança de TI são Mecanismos para assegurar que a TI agregue valor ao negócio com riscos aceitáveis (Acórdão 2.585/2012-TCU-Plenário). É também o Sistema pelo qual o uso atual e futuro da TI é dirigido e controlado (NBR ISO/IEC 38.500).</p>" data-toggle="popover" data-size="popover-small"><sup>4</sup></a>, terá mais chances de êxito na implantação de um sistema do que o Órgão B que não tenha a sua área de TI bem estruturada.</p>
						<p>Nesse contexto, vejamos o trecho abaixo constante do Guia de Boas Práticas em Contratação de Soluções de Tecnologia da Informação: riscos e controles para o planejamento da contratação, do Tribunal de Contas da União (2012, p.44):</p>
						<div class="col-lg-2 col-md-2 col-sm-2 semPaddingRight"></div>
						<div class="col-lg-10 col-md-10 col-sm-10 semPaddingLeft">
							<p class="fonteMenor">Frequentemente, o <span class="semi-bold">retorno dos investimentos</span> em TI não é obtido na totalidade e a principal causa observada para esse resultado negativo é a ênfase em aspectos puramente técnicos, financeiros ou de programação das atividades de TI, em detrimento da atenção ao uso da TI no contexto geral do negócio. (Grifamos).</p>
						</div>
						<div class="clear"></div>
						<p>Assim como aumenta a atenção dada pelo TCU aos investimentos de TI, de modo geral, a Corte Federal de Contas vem incrementando ano a ano sua atuação junto aos órgãos e gestores públicos, pugnando pela melhoria na instrução dos processos que envolvam gastos públicos de modo geral. </p>
						<p>Não só as contratações de TI, mas todas as outras em geral sempre devem ser planejadas de modo a avaliar os custos e os benefícios para o projeto em questão e também os impactos da demanda no contexto geral do órgão. Deverá ser analisada a adequação da demanda ao ambiente organizacional, principalmente no que se refere ao planejamento estratégico do órgão.</p>
						<p>Por exemplo, um tribunal que possui plano de logística sustentável, com foco na preservação do meio ambiente e adequada gestão dos resíduos gerados deverá observar a promoção das contratações sustentáveis no planejamento e execução de suas demandas.</p>
						<div class="espacamentoBottom">
							<div class="col-md-6 textAlignRight" ><button type="button" class="saibaMais" id="iconeSaibaMais"><img alt="" src="../include/img/icons/saibaMais.png" /></button></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder-SaibaMais" ><p class="tituloSaibaMais">Saiba Mais</p><p>Clique na imagem ao lado</p></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-SaibaMais-1">
								<p class="titulo">Plano de logística sustentável</p>
								<p>O Plenário do Conselho Nacional de Justiça (CNJ) aprovou, em 3/3/2015, durante a 203ª Sessão Ordinária, resolução que determina aos órgãos e conselhos do Judiciário a criação de unidades ou núcleos socioambientais. O texto também prevê a implantação, nesses órgãos, de planos de logística sustentável. Com foco na preservação do meio ambiente, a norma tem o objetivo de estimular a reflexão e a mudança dos padrões de compra, consumo e gestão documental no Judiciário, bem como do corpo funcional e da força de trabalho auxiliar de cada instituição.</p>
								<p>Fonte: <a href="http://cpsustentaveis.planejamento.gov.br/noticias/aprovada-resolucao-que-cria-nucleos-socioambientais-em-orgaos-do-judiciario-e-implanta-pls" target="_blank">Ministério do Planejamento</a>.</p>
							</div>
						</div>
						<div class="clear"></div>
						<p>Dessa forma, o gestor deverá analisar com atenção o contexto geral do negócio do seu órgão e estimar corretamente o <span class="semi-bold">investimento</span> a ser realizado, bem como os possíveis <span class="semi-bold">retornos</span> a serem <span class="semi-bold">alcançados</span> com o gasto. Para isso, o <span class="semi-bold">incentivo à qualificação técnica</span>, além da <span class="semi-bold">atualização frequente dos servidores</span> constituem <span class="semi-bold">boas práticas</span> a serem trabalhadas pela Administração.</p>
						<div class="espacamentoBottom">
							<div class="col-md-6 textAlignRight" ><button type="button" class="cerebro" id="iconeSaibaMais"><img alt="" src="../include/img/icons/dica.png"/></button></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="esconder" ><p>Clique na imagem ao lado</p></div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-lg-12" id="destaque-1">
								<p>Caro gestor e aluno, sempre esteja atento à sua demanda. O ideal é analisar e parametrizar todos os pontos relevantes já no início do planejamento da aquisição/contratação.</p>
								<p>Um ponto que pode auxiliar muito é a definição de uma equipe de planejamento da contratação. De acordo com a IN MP/SLTI Nº4/2014, art. 2º, inciso IV, com adaptações, essa equipe poderá ser composta por:</p>
								<p>a) Integrante Técnico: servidor com amplos conhecimentos sobre o objeto da demanda, indicado pela autoridade competente da área demandante ou da área técnica;</p>
								<p>b) Integrante Administrativo: servidor representante da Área Administrativa, indicado pela autoridade competente dessa área;</p>
								<p>c) Integrante Requisitante: servidor representante da Área Requisitante, indicado pela autoridade competente dessa área.</p>
							</div>
						</div>
						<div class="clear"></div>
                    </div>
                  </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina8.php', 'aula1pagina10.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


