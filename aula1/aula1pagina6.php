<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('A Questão da Legalidade', 'exibir', '1','6', '12', 'aula1pagina5.php', 'aula1pagina7.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
					  <h3 class="titulo">2.  A QUESTÃO DA LEGALIDADE</h3>
						<p>Certamente você já ouviu aquela máxima que diz: “no setor privado pode-se fazer tudo que a lei não proíbe. Na Administração Pública, só se pode fazer o que a lei permite”. <span class="semi-bold" style="color:#38518B">Isso é mesmo verdade, sempre?</span></p>
						<p>A flexibilidade na iniciativa privada é maior do que na Administração Pública, isso é fato. Mas estaria o setor público “amarrado” aos termos da lei, sempre?</p>
						<p>Em que pese a Administração Pública estar sujeita ao regime jurídico-administrativo, devendo observar os diversos dispositivos legais e princípios constitucionais, em especial os princípios da legalidade e da impessoalidade, que estão associados de forma direta à existência do processo licitatório (FURTADO, 2012), não podemos entender essa questão como travadora do trabalho do agente público. </p>
						<p>Ocorre que a mesma lei que disciplina a atuação dos agentes públicos também estabelece, quando cabível, certa discricionariedade para que o gestor público possa administrar e decidir, de acordo com critérios de conveniência e oportunidade.</p>
						<p>Certa vez, em uma palestra proferida em 2013, numa renomada instituição de ensino superior de Brasília, o Ministro aposentado do STF <a id="modal" title="informações do Min Carlos Ayres Britto" href="javascript:void(0);" style="padding-left:0px;" class="btn btn-primary btn-small" data-toggle="modal" data-target="#myModal">Carlos Ayres Britto <sup><img src="../include/img/icons/plus.jpg" width="10px" height="10px" alt="clique aqui para saber mais sobre o Ministro Ayres Britto" style="margin-left:5px;" /></sup></a> disse que a lei para a Administração Pública não é um trilho, em que o caminho delimitado deve ser sempre seguido sem exceções. Para o Ministro, a lei aproxima-se mais de uma trilha. Assim, haverá possibilidade de ajustes e adequações na rota seguida, sempre que a situação fática exigir e desde que plenamente justificado, obedecendo-se, ainda, ao Princípio da razoabilidade.</p>
						<p>Desse modo, a Administração Pública não está totalmente inerte. Frequentemente haverá opções, desde que os atores públicos sejam razoáveis, claros e registrem (justifiquem), no processo administrativo da aquisição/contratação, todas as suas ações e decisões.</p>
						
						
                    </div>
                  </div>   
                </div>
            </article> 
      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <br>
              <i class="icon-credit-card icon-7x"></i>
              <h4 id="myModalLabel" class="semi-bold">Carlos Ayres Britto</h4>
             
            </div>
            <div class="modal-body">
              <div class="row form-row" style="text-align:center;">
                <div class="row">

                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 "><img src="../include/img/aulas/minAyresBritto.jpg" alt="" style="width:133px"><br><br></div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> <p class="no-margin">Doutor em Direito Constitucional, foi Ministro do STF (2003/2012), do TSE (2006/2010) e Presidente do CNJ (2011/2012).</p>
              <br></div>
                </div>
                  
              </div>
            </div>
            <div class="modal-footer">
              <a class="btn btn-default" data-dismiss="modal">Voltar para a aula</a>
              <a href="http://www.stf.jus.br/portal/ministro/verMinistro.asp?periodo=stf&id=38"  target="_blank" class="btn btn-primary">Ver currículo de Carlos Ayres Britto</a>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->			
<?php  configNavegacaoRodape('exibir', 'aula1pagina5.php', 'aula1pagina7.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


