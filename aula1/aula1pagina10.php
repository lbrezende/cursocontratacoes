<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Contratações do STF', 'exibir', '1','10', '12', 'aula1pagina9.php', 'aula1pagina11.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">5.  CONTRATAÇÕES DO STF</h3>
						<p>Em relação aos extensos problemas nas contratações públicas, verificamos que os órgãos que estão atentos às orientações do TCU na busca pela maximização do uso do dinheiro público não vivenciam tantas dificuldades (REVISTA EXAME, 2013).</p>
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div class="bordaPalavraAutor">
							<p>Em vários treinamentos de que participei, recebi dos instrutores muitos elogios ao modo como o STF conduz suas demandas. Por diversas vezes, tive o prazer de ouvir que somos referência em áreas específicas de contratações públicas.</p>
							<p>Em que pese esse bom padrão, é certo que temos, ainda, muito a melhorar. A ênfase no planejamento e na definição meticulosa das demandas e dos resultados será abordada neste treinamento, com vistas à melhoria dos processos de nossas contratações, <i>lato sensu</i>.</p>
						</div>
						<p>Em 2012, conforme Relatório de Auditoria de Gestão da Prestação de Contas Anual, o Supremo Tribunal Federal realizou 189 (cento e oitenta e nove) procedimentos licitatórios, nas modalidades abaixo:</p>
						<p class="textAlignCenter"><img src="../include/img/aulas/grafico1.png" style="" alt="Palavra do Autor" /></p>
						<p class="textAlignCenter fonteMenor">Figura 1 – Licitações do STF em 2012</p>
						<p>Desse total, apenas 4,23% dos procedimentos licitatórios não obtiveram êxito, conforme gráfico a seguir:</p>
						<p class="textAlignCenter"><img src="../include/img/aulas/grafico2.png" style="" alt="Palavra do Autor" /></p>
						<p class="textAlignCenter fonteMenor">Figura 2 – Licitações sem êxito em 2012</p>
						<p>Em relação aos valores, foram estimados gastos de mais de 61 milhões de reais.</p>
						<p>Já em 2013, conforme Relatório de Auditoria de Gestão da Prestação de Contas Anual, o STF realizou 170 (cento e setenta) procedimentos licitatórios nas seguintes modalidades:</p>
						<p class="textAlignCenter"><img src="../include/img/aulas/grafico3.png" style="" alt="Palavra do Autor" /></p>
						<p class="textAlignCenter fonteMenor">Figura 3 – Licitações do STF em 2013</p>
						<p>Do total de licitações realizadas em 2013, somente 9,41% não obtiveram sucesso.</p>
						<p class="textAlignCenter"><img src="../include/img/aulas/grafico4.png" style="" alt="Palavra do Autor" /></p>
						<p class="textAlignCenter fonteMenor">Figura 4 – Licitações sem êxito em 2013</p>
						<p>Apesar do acréscimo no número de licitações que não vingaram em relação ao ano anterior, o percentual de êxito nas contratações em 2013 ainda ultrapassa os 90%. Em termos de valores, foram licitados e contratados R$ 87.480.473,68. </p>
						<p>Conforme dados da execução orçamentária registrados no SIAFI, os processos de contratações diretas realizados pelo STF resultaram em despesas no valor de R$ 6.680.901,40 em 2013.</p>
						<p>Os números demonstram sucesso nas contratações. Faltam, contudo, estudos mais detalhados que revelem as principais causas de insucesso em alguns procedimentos, os desdobramentos da execução contratual, os riscos envolvidos nos processos e os benefícios alcançados.</p>
						<p class="textAlignCenter"><img src="../include/img/icons/palavraAutor.png" style="max-height:98px" alt="Palavra do Autor" /></p>
						<div  class="bordaPalavraAutor">
							<p>Uma boa prática que corrobora a gestão efetiva dos gastos públicos consiste em <span class="semi-bold">monitorar os resultados dos procedimentos licitatórios</span>, por meio de <span class="semi-bold">indicadores de desempenho<a href="javascript:void(0);" rel="popover" data-content="<p>A construção e implantação de indicadores de desempenho tem por objetivo medir, da forma mais próxima ao real, o resultado almejado. Fonte:  <a href='http://consadnacional.org.br/wp-content/uploads/2013/05/012-ACORDO-DE-NÍVEL-DE-SERVIÇO-E-EFICIÊNCIA-NA-GESTÃO-CONTRATUAL-O-CASO-DA-CIDADE-ADMINISTRATIVA.pdf' target='_blank' title='fonte do texto'>VI Congresso CONSAD de Gestão Pública</a></p>" data-toggle="popover" data-size="popover-small"><sup>5</sup></a></span>.</p>
						</div>
						
					  </div>
                    </div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina9.php', 'aula1pagina11.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


