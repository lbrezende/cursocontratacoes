<?php 
//Inclusão de funções de configHeader e configFooter
require_once('../util/util.php'); 
configHeader('Considerações finais', 'exibir', '1','11', '12', 'aula1pagina10.php', 'aula1pagina12.php', '<h4 style="font-weight:bold"> Administração Pública e Setor Privado:</h4><h5>Identificando Problemas nas Contratações Públicas</h5>');
?> 

            <article id="aula" accesskey="2">
               <div class="row">
				<div class="col-md-12">
                    <div class="grid simple">
                      <div class="grid-body no-border">
						<h3 class="titulo">RESUMO DA AULA</h3>
						<p>Nesta primeira aula, verificamos sinteticamente que tanto o setor privado quanto a Administração Pública buscam a eficiência como fator impulsionador da melhoria na qualidade da gestão. Entendemos que o Princípio da Legalidade não engessa a atuação do gestor público, uma vez que normalmente há margem de discricionariedade para o gestor atuar. Verificamos que a estrutura burocrática no processo de contratação garante segurança aos gestores em relação ao registro de seus atos e decisões. Vimos que é importante identificar os problemas nas contratações, bem como justificar com atenção os investimentos. Por fim, conhecemos alguns dados sobre as licitações do STF.</p>
						<p>Na próxima aula, trabalharemos pontos relacionados ao planejamento, de modo geral, abordando a importância da ação de planejar, para as contratações públicas. </p>
						<p>Até a próxima...</p>
						<p>Abraços e bom estudo.</p>
						<h3 class="titulo">MENSAGEM</h3>
						<p class="textAlignCenter"><img src="../include/img/aulas/mensagem.jpg" style="" alt="Palavra do Autor" /></p>

					  </div>
					</div>   
                 </div>
				</div>
            </article>    
<?php  configNavegacaoRodape('exibir', 'aula1pagina10.php', 'aula1pagina12.php'); ?>
<?php configFooter(); // inclusão de rodapé automático nas páginas ?> 


